package client;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Bonus;
import model.BusinessTile;
import model.Configuration;
import model.ConnectionProblemException;
import model.Constant;
import model.CouncilBalcony;
import model.Councillor;
import model.Emporium;
import model.GameActions;
import model.King;
import model.NobilityTrack;
import model.Player;
import model.Region;
import model.RemoteClient;
import model.RemoteServer;
import model.Supply;
import model.Town;

/**
 * Classe che estende l'interfaccia Connection per gestire i metodi di gioco con
 * una connessione RMI
 *
 */
public class ClientRmi extends UnicastRemoteObject implements RemoteClient, Connection {
	private final transient Logger log = Logger.getLogger(ClientRmi.class.getName());

	private transient RemoteServer stub;
	private transient GameActions stubmatch;

	private boolean needsconfig = false;

	private boolean statuschanged = false;
	private String status = Constant.NOPLAYING;
	private int idmatch;

	private String clear = "-------------------------------------------------------------------------------------";

	private int howlongdoineedtowait = 20;
	private boolean end = false;
	/*
	 * match variables
	 */
	private Configuration c;

	public ClientRmi() throws RemoteException {
		super();
		Registry registryServer = LocateRegistry.getRegistry(Constant.PORT);
		try {
			this.stub = (RemoteServer) registryServer.lookup("Server");
		} catch (NotBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			throw new NullPointerException();
		}
		if (obj == this) {
			return true;
		}
		if (this.getClass() == obj.getClass()) {
			return true;
		}

		ClientRmi fobj = (ClientRmi) obj;
		if (idmatch == fobj.getIdmatch()) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return idmatch;

	}

	/**
	 * Metodo lanciato dopo un login con successo, che mette sul registry il
	 * client
	 * 
	 * @param playername
	 *            nome del giocatore
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	private void createResponseConnection(String playername) throws RemoteException {
		Registry registryClient;
		try {
			registryClient = LocateRegistry.getRegistry(Constant.PORT);
		} catch (RemoteException e) {
			log.log(Level.FINEST, e.toString(), e);
			registryClient = LocateRegistry.createRegistry(Constant.PORT);
		}
		try {
			registryClient.bind(playername, this);
			this.stub.lookForStub(playername);
		} catch (AlreadyBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}

	}

	@Override
	public String createAccount(String username, String password) throws ConnectionProblemException {
		try {
			String outcome = this.stub.remoteCreateAccount(username, password);
			if (outcome.equals(Constant.CONNECTSUCCESS)) {
				this.createResponseConnection(username);
			}
			return outcome;
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String login(String username, String password) throws ConnectionProblemException {
		try {
			String outcome = this.stub.remoteLogin(username, password);
			if (outcome.equals(Constant.CONNECTSUCCESS)) {
				this.createResponseConnection(username);
			}
			return outcome;
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String joinGame(String mynameplayer) throws ConnectionProblemException {
		try {
			return this.stub.remoteJoinGame(mynameplayer);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}

	}

	@Override
	public String setConfig(Configuration c) throws ConnectionProblemException {
		try {
			this.stub.remoteSetConfig(c);
			return Constant.SUCCESS;
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String electCouncillorWithEarn(String playername, int counc, String region)
			throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteElectCouncillorWithEarn(playername, counc, region);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String acquireBusinessTile(String playername, String region, int pos, int[] indexpoliticcarddropped)
			throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteAcquireBusinessTile(playername, region, pos, indexpoliticcarddropped);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String buildEmporiumWithTile(String playername, int busindex, String town)
			throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteBuildEmporiumWithTile(playername, town, busindex);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String buildEmporiumWithKing(String myplayername, String town, int[] ind) throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteBuildEmporiumWithKing(myplayername, town, ind);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String engageAssistant(String nameplayer) throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteEngageAssistant(nameplayer);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String changeBusinessTiles(String playername, String region) throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteChangeBusinessTile(playername, region);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String electCouncillorPaying(String playername, int scelta, String regione)
			throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteElectCouncillorWithAssistant(playername, scelta, regione);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String additionalMainAction(String nameplayer) throws ConnectionProblemException {
		try {
			return this.stubmatch.remoteAdditionalMainAction(nameplayer);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String endTurn(String nameplayer) throws ConnectionProblemException {
		try {
			return this.stubmatch.remotePassToNextRound(nameplayer);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String buySupply(String nameplayer, int supplyindex) throws ConnectionProblemException {
		try {
			return this.stubmatch.buySupply(nameplayer, supplyindex);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public void createSupply(String nameplayer, int assistants, int[] businesstile, int[] politiccard, int price)
			throws ConnectionProblemException {
		try {
			this.stubmatch.addMarketSupply(nameplayer, assistants, businesstile, politiccard, price);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public void reconnect(String nameplayer) throws ConnectionProblemException {
		try {
			this.stubmatch.reconnect(nameplayer);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public void updateConfig(Configuration c, int idmatch) throws RemoteException {
		this.setConfigFinal(c);
		this.setStatus(Constant.WAITINGFORPLAYERS);
		this.setIdmatch(idmatch);
		this.setStatusChanged(true);
		this.takeRegistryMatch();
	}

	/**
	 * Metodo per trovare nel registry l'oggetto remoto del match in cui si è
	 * entrati
	 * 
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	private void takeRegistryMatch() throws RemoteException {
		Registry registryMatch = LocateRegistry.getRegistry(Constant.PORT);
		try {
			this.stubmatch = (GameActions) registryMatch.lookup("match" + this.idmatch);
		} catch (NotBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
	}

	@Override
	public void updateMatch() throws RemoteException {
		this.setStatus(Constant.STARTED);
		this.setStatusChanged(true);
	}

	@Override
	public void giveMeConfig() throws RemoteException {
		this.setNeedsconfig(true);
		this.setStatusChanged(true);
	}

	@Override
	public void setTimer() {
		final MyTimer timer = new MyTimer();
		timer.scheduleAtFixedRate(() -> {
			if (getHowlongdoineedtowait() == 20) {
				timer.cancel();
			} else {
				increaseHowlongdoineedtowait();
				setStatusChanged(true);
			}
		}, 1000, 1000);
	}

	@Override
	public void endingMatch() throws RemoteException {
		this.setEnd(true);
	}

	@Override
	public boolean isStatusChanged() {
		return statuschanged;
	}

	@Override
	public void setStatusChanged(boolean statuschanged) {
		this.statuschanged = statuschanged;
	}

	public int getIdmatch() {
		return idmatch;
	}

	private void setIdmatch(int idmatch) {
		this.idmatch = idmatch;
	}

	@Override
	public boolean isNeedsconfig() {
		return needsconfig;
	}

	private void setNeedsconfig(boolean needsconfig) {
		this.needsconfig = needsconfig;
	}

	@Override
	public Configuration getConfig() {
		return c;
	}

	private void setConfigFinal(Configuration c) {
		this.c = c;
	}

	@Override
	public String getStatus() {
		return status;
	}

	private void setStatus(String status) {
		this.status = status;
	}

	public int getHowlongdoineedtowait() {
		return howlongdoineedtowait;
	}

	private void increaseHowlongdoineedtowait() {
		this.howlongdoineedtowait--;
	}

	@Override
	public boolean isEnd() {
		return end;
	}

	@Override
	public void setEnd(boolean end) {
		this.statuschanged = false;
		this.needsconfig = false;
		this.status = Constant.NOPLAYING;
		this.end = end;
	}

	// UI
	@Override
	public String whoIsNow(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteWhoIsNow();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public King getKing(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetKing();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public NobilityTrack getNobilityTrack(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetNobilityTrack();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<Player> getPlayers(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetPlayers();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Region getRegion(String me, String r) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetRegion(r);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<Emporium> getEmporiums(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetEmporium();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<String> getPlayersName(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetPlayersname();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<BusinessTile> getPlayersTile(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetPlayersTile();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Town[] getTowns(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetTowns();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public CouncilBalcony getCouncillorBalcony(String me, String r) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetCouncillorBalcony(r);
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<Councillor> getCouncillorPoolList(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetCouncillorPoolList();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusGold(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetBonusGoldTown();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusSilver(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetBonusSilverTown();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusBronze(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetBonusBronzeTown();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusIron(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteGetBonusIronTown();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String whoIsTheWinner(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteWhoIsTheWinner();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<Supply> getSupplies(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteShowSupplies();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public boolean isSellingPhase(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteIsSellingPhase();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public boolean isBuyingPhase(String me) throws ConnectionProblemException {
		try {
			this.stubmatch.noMoreAfk(me);
			return this.stubmatch.remoteIsBuyingPhase();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String disconnect(String myplayername) throws ConnectionProblemException {
		try {
			this.stub.remotePlayerDisconnected(myplayername);
			Registry registryClient = LocateRegistry.getRegistry(Constant.PORT);
			registryClient.unbind(myplayername);
			return Constant.SUCCESS;
		} catch (RemoteException | NotBoundException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public boolean giveMeTavolino() throws ConnectionProblemException {
		try {
			return this.stubmatch.giveMeTavolino();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM + " " + e.toString(), e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public void playerGoneAfk(String s) throws RemoteException {
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);
		pw.println(clear);
		pw.println("The player: " + s + " has gone afk.");
		pw.println(clear);
	}

	@Override
	public void playerDisconnect(String s) throws RemoteException {
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);
		pw.println(clear);
		pw.println("The player: " + s + " disconnected.");
		pw.println(clear);
	}

}
