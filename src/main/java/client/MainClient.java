package client;

public class MainClient {
	private ClientCli cc;

	private MainClient() {
		cc = new ClientCli();
	}

	private void start() {
		cc.start();
	}

	public static void main(String[] args) {
		MainClient mc = new MainClient();
		mc.start();
	}
}
