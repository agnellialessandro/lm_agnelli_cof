package client;

import java.util.List;

import model.Bonus;
import model.BusinessTile;
import model.Configuration;
import model.ConnectionProblemException;
import model.CouncilBalcony;
import model.Councillor;
import model.Emporium;
import model.King;
import model.NobilityTrack;
import model.Player;
import model.Region;
import model.Supply;
import model.Town;

public interface Connection {
	/**
	 * Metodo per l'azione principale eleggere un consigliere ricevendo un guadagno
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @param scelta
	 * indice del consigliere da eleggere
	 * @param regione
	 * nome della regione in cui eleggere il consigliere
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String electCouncillorWithEarn(String myplayername, int scelta, String regione)
			throws ConnectionProblemException;
	/**
	 * Metodo per l'azione principale per comprare una businesstile
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @param regione
	 * nome della regione in cui comprare la business tile
	 * @param theonetobuy
	 * indice della businesstile da comprare
	 * @param index
	 * indice carte politiche scartate per soddisfare il concilio
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String acquireBusinessTile(String myplayername, String regione, int theonetobuy, int[] index)
			throws ConnectionProblemException;
	/**
	 * Metodo per costruire un emporio usando una businesstile
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @param l
	 * indice della business tile da usare
	 * @param town
	 * nome della città in cui costruire
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String buildEmporiumWithTile(String myplayername, int l, String town) throws ConnectionProblemException;
	/**
	 * Metodo per costruire un emporio con l'aiuto del re.
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @param town
	 * nome città in cui si vuole costruire l'emporio
	 * @param ind
	 * indice carte politiche scartate per soddisfare il concilio
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String buildEmporiumWithKing(String myplayername, String town, int[] ind) throws ConnectionProblemException;
	
	/**
	 * Metodo per passare il turno
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String endTurn(String myplayername) throws ConnectionProblemException;
	
	/**
	 * Metodo per l'azione veloce ingaggio di un assistente
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String engageAssistant(String myplayername) throws ConnectionProblemException;
	
	/**
	 * Metodo per cambiare la businesstile di una regione
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @param region
	 * nome della regione in cui si vuole cambiare la businesstile
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String changeBusinessTiles(String myplayername, String region) throws ConnectionProblemException;
	
	/**
	 * Metodo per eleggere un consigliere pagando
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @param scelta
	 * indice del consigliere da eleggere
	 * @param regione
	 * nome della regione in cui eleggere
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String electCouncillorPaying(String myplayername, int scelta, String regione)
			throws ConnectionProblemException;
	
	/**
	 * Metodo per ottenere un'ulteriore azione principale
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String additionalMainAction(String myplayername) throws ConnectionProblemException;
	
	/**
	 * Metodo per poter creare un account nel server
	 * @param username
	 * nome del giocatore che compie l'azione
	 * @param password
	 * password da impostare all'account
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String createAccount(String username, String password) throws ConnectionProblemException;
	
	/**
	 * Metodo per accedere all'account nel server
	 * @param username
	 * nome del giocatore che compie l'azione
	 * @param password
	 * password dell'account
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String login(String username, String password) throws ConnectionProblemException;
	
	/**
	 * Metodo per poter entrare in un Match nel server
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String joinGame(String myplayername) throws ConnectionProblemException;
	
	/**
	 * Metodo per poter inviare una configurazione al server
	 * @param c2
	 * configurazione da inviare
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String setConfig(Configuration c2) throws ConnectionProblemException;
	
	/**
	 * Metodo per poter creare una supply da inviare al server nella fase di Market
	 * @param nameplayer
	 * nome del giocatore che compie l'azione
	 * @param assistants
	 * numero di assistenti da vendere
	 * @param businesstile
	 * indice delle businesstile da vendere
	 * @param politiccard
	 * indice delle carte politiche da vendere
	 * @param price
	 * prezzo della supply
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public void createSupply(String nameplayer, int assistants, int[] businesstile, int[] politiccard, int price)
			throws ConnectionProblemException;
	
	/**
	 * Metodo per poter comprare una supply durante la fase di market
	 * @param nameplayer
	 * nome del giocatore che compie l'azione
	 * @param supplyindex
	 * indice della supply da comprare
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String buySupply(String nameplayer, int supplyindex) throws ConnectionProblemException;
	
	/**
	 * Metodo per potersi riconnettere nel caso si sia andati offline
	 * @param nameplayer
	 * nome del giocatore che compie l'azione
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public void reconnect(String nameplayer) throws ConnectionProblemException;
	
	public Configuration getConfig();
	
	public boolean isNeedsconfig();

	public boolean isStatusChanged();

	public boolean isEnd();

	public void setEnd(boolean b);

	public void setStatusChanged(boolean b);

	public Object getStatus();
	
	/**
	 * Metodo per potersi disconnettere
	 * @param myplayername
	 * nome del giocatore che compie l'azione
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String disconnect(String myplayername) throws ConnectionProblemException;
	
	/**
	 * Metodo per conoscere di che giocatore è il turno
	 * @param me 
	 * nome del giocatore che compie l'azione
	 * @return
	 * Stringa da Constant che descrive se l'azione è andata a buon fine
	 * nome del giocatore che deve giocare
	 * @throws ConnectionProblemException
	 * Se ci sono problemi di connessione
	 */
	public String whoIsNow(String me) throws ConnectionProblemException;
	
	public King getKing(String me) throws ConnectionProblemException;

	public NobilityTrack getNobilityTrack(String me) throws ConnectionProblemException;

	public List<Player> getPlayers(String me) throws ConnectionProblemException;

	public Region getRegion(String me, String r) throws ConnectionProblemException;

	public List<Emporium> getEmporiums(String me) throws ConnectionProblemException;

	public List<String> getPlayersName(String me) throws ConnectionProblemException;

	public List<BusinessTile> getPlayersTile(String me) throws ConnectionProblemException;

	public Town[] getTowns(String me) throws ConnectionProblemException;

	public CouncilBalcony getCouncillorBalcony(String me, String r) throws ConnectionProblemException;

	public List<Councillor> getCouncillorPoolList(String me) throws ConnectionProblemException;

	public Bonus getBonusGold(String me) throws ConnectionProblemException;

	public Bonus getBonusSilver(String me) throws ConnectionProblemException;

	public Bonus getBonusBronze(String me) throws ConnectionProblemException;

	public Bonus getBonusIron(String me) throws ConnectionProblemException;

	public String whoIsTheWinner(String me) throws ConnectionProblemException;

	public List<Supply> getSupplies(String me) throws ConnectionProblemException;

	boolean isBuyingPhase(String me) throws ConnectionProblemException;

	boolean isSellingPhase(String me) throws ConnectionProblemException;

	boolean giveMeTavolino() throws ConnectionProblemException;
}
