package client;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;

import model.Bonus;
import model.BusinessTile;
import model.Configuration;
import model.ConnectionProblemException;
import model.Constant;
import model.CouncilBalcony;
import model.Councillor;
import model.Emporium;
import model.King;
import model.NobilityTrack;
import model.Player;
import model.PoliticCard;
import model.Region;
import model.Supply;
import model.Town;

public class ClientCli {

	private static final Logger log = Logger.getLogger(ClientCli.class.getName());

	private String pressdefault = "for default press 0";

	private PrintWriter pw;
	private Scanner s;
	private String myplayername;
	private String whoisnow;
	private boolean endmatch = false;
	private String connection = Constant.RMI;
	private boolean newmatch = false;

	private Connection connect;
	private boolean retried = false;

	private int countretryaction = 0;

	/*
	 * variable of the game
	 */

	ClientCli() {
		/*
		 * input output initializing
		 */
		pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);
		s = new Scanner(System.in);
	}

	public void start() {
		pw.println(Constant.TITLE);
		runConsole();
	}

	/**
	 * Inizializza la console, chiede che tipo di connessione si voglia usare e
	 * i dati del login
	 */
	private void runConsole() {
		pw.println("Write create in login to create a new account(care it's CasE SENsiTiVE)");
		pw.println("Connection type: " + connection + "(to change write RMI/socket in login");
		pw.println("Login");
		pw.print("Username(no space allowed): ");
		pw.flush();
		String user = getStringInput();
		switch (user) {
		case "create":
			createUI();
			break;
		case Constant.RMI:
			connection = Constant.RMI;
			clear();
			runConsole();
			break;
		case Constant.SOCKET:
			connection = Constant.SOCKET;
			clear();
			runConsole();
			break;
		default:
			pw.print("Password: ");
			pw.flush();
			String psw = getStringInput();
			login(user, psw);
			break;
		}
	}

	/**
	 * chiede i dati per la creazione dell'account e il tipo di connessione da
	 * utilizzare
	 */
	private void createUI() {
		clear();
		pw.println("Write back in login to return to previously menu(care it's CasE SENsiTiVE)");
		pw.println("Connection type: " + this.connection + "(to change write RMI/socket in login");
		pw.println("Login");
		pw.print("Username(no space allowed):");
		pw.flush();
		String username = getStringInput();
		switch (username) {
		case Constant.RMI:
			this.connection = Constant.RMI;
			createUI();
			break;
		case Constant.SOCKET:
			this.connection = Constant.SOCKET;
			createUI();
			break;
		case "back":
			clear();
			runConsole();
			break;
		default:
			pw.print("Password:");
			pw.flush();
			String password = getStringInput();
			create(username, password);
			break;
		}
	}

	private void create(String username, String password) {
		createConnection();
		createAccount(username, password);
	}

	/**
	 * effettura la chiamata per la creazione dell'account
	 * 
	 * @param username
	 * @param password
	 */

	private void createAccount(String username, String password) {
		clear();
		String outcome = null;
		try {
			outcome = connect.createAccount(username, password);
		} catch (ConnectionProblemException e) {
			clear();
			log.log(Level.FINE, Constant.CONNECTIONPROBLEM, e);
			createAccount(username, password);
			return;
		}
		switch (outcome) {
		case Constant.CONNECTSUCCESS:
			this.myplayername = username;
			clear();
			game();
			break;
		case Constant.NAMEEXISTS:
			clear();
			pw.println("Username already in use");
			createUI();
			break;
		case Constant.FILEREADPROBLEM:
			createAccount(username, password);
			break;
		default:
			pw.println("Something went wrong.");
			createUI();
			break;
		}
	}

	private void login(String username, String password) {
		createConnection();
		loginAccount(username, password);
	}

	/**
	 * seleziona il tipo di connessione e istanzia una classe diversa a seconda
	 * di quale connessione è stata scelta
	 */
	private void createConnection() {
		if (retried) {
			return;
		}
		if (this.connection.equals(Constant.RMI)) {
			try {
				this.connect = new ClientRmi();
				this.retried = true;
			} catch (RemoteException e) {
				log.log(Level.SEVERE, e.toString(), e);
				runConsole();
			}
		} else {
			try {
				this.connect = new ClientSocket();
				this.retried = true;
			} catch (IOException e) {
				log.log(Level.SEVERE, e.toString(), e);
				runConsole();
			}
		}
	}

	/**
	 * effettura il login dell'account con i parametri passati
	 * 
	 * @param username
	 * @param password
	 */
	private void loginAccount(String username, String password) {
		clear();
		String outcome = null;
		try {
			outcome = this.connect.login(username, password);
		} catch (ConnectionProblemException e) {
			clear();
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			login(username, password);
			return;
		}
		switch (outcome) {
		case Constant.CONNECTSUCCESS:
			this.myplayername = username;
			pw.println("Connected");
			game();
			break;
		case Constant.ALREADYCONNECTED:
			pw.println("This account is already in use");
			runConsole();
			break;
		case Constant.NOLOGINMATCH:
			pw.println("Mistyped username/password");
			runConsole();
			break;
		case Constant.FILEREADPROBLEM:
			loginAccount(username, password);
			break;
		default:
			pw.println("Something went wrong.");
			runConsole();
			break;
		}
	}

	/**
	 * primo menu del gioco e permette di entrare in una partita o uscire
	 */

	private void game() {
		this.newmatch = false;
		clear();
		pw.println("1. Join match");
		pw.println("0. Exit");
		int choice = getIntInput();
		switch (choice) {
		case 1:
			joinMatch();
			break;
		case 0:
			exit();
			System.exit(0);
			break;
		default:
			game();
		}
	}

	private void exit() {
		boolean x = false;
		while (!x) {
			try {
				this.connect.disconnect(myplayername);
				x = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
	}

	/**
	 * chiama la funzione su remoto per entrare in una partita e rimane in
	 * attesa, se viene impostato una richiesta di configurazione chiama il
	 * metodo apposito
	 */
	private void joinMatch() {
		/*
		 * entra in un match, se
		 */
		String outcome = null;
		try {
			outcome = this.connect.joinGame(this.myplayername);
		} catch (ConnectionProblemException e) {
			clear();
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			return;
		}
		if (outcome.equals(Constant.SUCCESS)) {
			pw.println("Joining match...");
			pw.println("Joined");
			clear();
			waitForPlayerScreen();
		}
		if (outcome.equals(Constant.WAITING)) {
			pw.println("Initializing match");
			final MyTimer timer = new MyTimer();
			timer.scheduleAtFixedRate(() -> {
				if (connect.isStatusChanged() && connect.isNeedsconfig()) {
					timer.cancel();
					connect.setStatusChanged(false);
					createConfig();
				} else {
					if (connect.isStatusChanged() && connect.getStatus().equals(Constant.WAITINGFORPLAYERS)) {
						timer.cancel();
						pw.println("Joining match...");
						pw.println("Joined");
						clear();
						connect.setStatusChanged(false);
						waitForPlayerScreen();
					}
				}
			}, Constant.CHECKTIMER, Constant.CHECKTIMER);
		}
	}

	/**
	 * visualizza le scelte di configurazione fatte per la partita che si sta
	 * aspettando di giocare
	 */
	private void waitForPlayerScreen() {
		clear();
		pw.println("Map Stats");
		pw.println("Max bonus: " + connect.getConfig().getMaxBonus() + ".");
		pw.println("Map: " + connect.getConfig().getMap() + ".");
		pw.println("Round: " + connect.getConfig().getTimeRound() + " seconds.");
		pw.println("Max number of players: " + connect.getConfig().getMaxNumberOfPlayers());
		final MyTimer timer = new MyTimer();
		timer.scheduleAtFixedRate(() -> {
			if (connect.isStatusChanged() && connect.getStatus().equals(Constant.STARTED)) {
				connect.setStatusChanged(false);
				clear();
				printWhoIsNow();
				newTimerRefresh(timer);
				menuUI();
			}
		}, 1000, 1000);
		/*
		 * timer for decreasing retryaction
		 */
		MyTimer tim = new MyTimer();
		tim.scheduleAtFixedRate(() -> {
			if (countretryaction > 0) {
				countretryaction--;
			}
			if (connect.isEnd()) {
				timer.cancel();
			}
		}, 1000, 1000);
	}

	/**
	 * controlla se lo stato della partita è cambiato e manda un messaggio
	 */
	private void newTimerRefresh(MyTimer t) {
		t.cancel();
		final MyTimer timer = new MyTimer();
		newTimerCheckEnd(timer);
		timer.scheduleAtFixedRate(() -> {
			if (connect.isStatusChanged()) {
				clear();
				pw.println("Someone made a move, something changed!");
				printWhoIsNow();
				clear();
				connect.setStatusChanged(false);
			}
		}, 500, 500);
	}

	private void printWhoIsNow() {
		boolean retry = true;
		while (retry) {
			try {
				whoisnow = connect.whoIsNow(myplayername);
				retry = false;
			} catch (ConnectionProblemException e) {
				reconnect(e);
				retry = true;
			}
		}
		if (whoisnow.equals(myplayername)) {
			pw.println("It's your turn! Kick their asses!");
		} else {
			pw.println("Is " + whoisnow + "'s turn");
		}
	}

	/**
	 * controlla se è finita la partita e interrompe il timer che controlla lo
	 * stato della partita
	 * 
	 * @param t
	 */
	private void newTimerCheckEnd(final MyTimer t) {
		final MyTimer timer = new MyTimer();
		timer.scheduleAtFixedRate(() -> {
			if (connect.isEnd()) {
				endUI();
				t.cancel();
				timer.cancel();
			}
		}, 500, 500);

	}

	private void sleep() {
		try {
			Thread.sleep(500);
		} catch (Exception e1) {
			log.log(Level.SEVERE, "Err", e1);
			sleep();
		}
	}

	/**
	 * schermata di fine partita
	 */
	private void endUI() {
		sleep();
		newmatch = true;
		clear();
		pw.println("Match ended");
		if (giveMeTavolino()) {
			pw.println("All your opponents abbandoned the match. YOU WON");
		} else {
			boolean ok = false;
			while (!ok) {
				try {
					pw.println("The winner is: " + this.connect.whoIsTheWinner(this.myplayername));
					ok = true;
				} catch (ConnectionProblemException e) {
					reconnect(e);
				}
			}
			pw.println();
			victoryTrack();
		}
		pw.println("Press any key to exit.");
	}

	private boolean giveMeTavolino() {
		boolean tavolino = false;
		boolean ok = false;
		while (!ok) {
			try {
				tavolino = this.connect.giveMeTavolino();
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		return tavolino;
	}

	/**
	 * schermata di scelta parametri di configurazione partita
	 */
	private void createConfig() {
		clear();
		Configuration c = new Configuration();
		pw.println("Select map stats");
		// max bonus setup
		pw.println("Max bonus: " + c.getMaxBonus() + " " + pressdefault);
		int mb = getIntInput();
		if (mb > 0) {
			c.setMaxBonus(mb);
		}
		// time round setup
		pw.println("Time round: " + c.getTimeRound() + " " + pressdefault);
		mb = getIntInput();
		if (mb > 0) {
			c.setTimeRound(mb);
		}
		// map setup
		pw.println("Map: " + c.getMap() + " " + pressdefault);
		List<String> maps = new ArrayList<>();
		printListOfMaps(maps);
		mb = getIntInput();
		/*
		 * input control size()+1 perchè vengono stampate con indice +1 rispetto
		 * alla lista e l'ultima mappa è la lunghezza stessa
		 */
		while (mb < 0 || mb > maps.size() + 1) {
			pw.println(Constant.CHOICENOTALLOWED);
			mb = getIntInput();
		} // end input control
		if (mb != 0) {
			c.setMap(maps.get(mb - 1));
		}
		// maxplayer selecting
		pw.println("Max number of players: " + c.getMaxNumberOfPlayers() + " " + pressdefault);
		mb = getIntInput();
		/*
		 * input control size()+1 perchè vengono stampate con indice +1 rispetto
		 * alla lista e l'ultima mappa è la lunghezza stessa
		 */
		while (mb < 0) {
			pw.println(Constant.CHOICENOTALLOWED);
			mb = getIntInput();
		} // end input control
		if (mb != 0) {
			c.setMaxNumberOfPlayers(mb);
		}
		sendConfig(c);
	}

	private void sendConfig(Configuration c) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.setConfig(c);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		if (outcome.equals(Constant.SUCCESS)) {
			waitForPlayerScreen();
		} else {
			createConfig();
		}
	}

	/**
	 * stampa la lista delle mappe disponibili e le salva nella lista passata
	 * come parametro
	 * 
	 * @param s
	 */
	private void printListOfMaps(List<String> s) {
		// reading available maps
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + "listofmaps.txt")) {
			Scanner input = new Scanner(new BufferedReader(fr));
			String temp;
			while (input.hasNext()) {
				temp = input.next();
				s.add(temp);
			}
			input.close();
		} catch (IOException e) {
			clear();
			log.log(Level.FINE, "No map avaible, press 0", e);
			return;
		}
		// printing maps
		int i = 1;
		for (String e : s) {
			pw.println(i + ". " + e);
			i++;
		}
	}

	/**
	 * menu delle mosse della partita
	 */
	private void menuUI() {
		while (!endmatch) {
			clear();
			pw.println("What do you want to do?");
			pw.println("1. Show map.");
			pw.println("2. Show player stats");
			pw.println("3. Show region's balcony and tiles");
			pw.println("4. Show emporiums");
			pw.println("5. Show businesstile not used to build");
			pw.println("6. Show my hand");
			pw.println("7. Show track");
			pw.println("8. Make action");
			pw.println("9. End my turn");
			int scelta = getIntInput();
			if (newmatch) {
				exit();
				return;
			}
			if (scelta < 6) {
				firstswitch(scelta);
			} else {
				secondswitch(scelta);
			}
		}
	}

	private void firstswitch(int scelta) {
		switch (scelta) {
		case 1:
			mapUI();
			break;
		case 2:
			playerUI();
			break;
		case 3:
			balconyUI();
			break;
		case 4:
			showEmporiums();
			break;
		case 5:
			showBusinessTilePlayers();
			break;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
		}
	}

	private void secondswitch(int scelta) {
		switch (scelta) {
		case 6:
			showMyHand();
			break;
		case 7:
			trackUI();
			break;
		case 8:
			actions();
			break;
		case 9:
			endMyTurn();
			break;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
		}
	}

	/**
	 * controlla che tipo di turno sia e reindirizza il giocatore che vuole fare
	 * la mossa
	 */
	private void actions() {
		boolean selling = false;
		boolean buying = false;
		boolean ok = false;
		while (!ok) {
			try {
				buying = this.connect.isBuyingPhase(this.myplayername);
				selling = this.connect.isSellingPhase(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		if (checkMyTurn() && !(buying || selling)) {
			makeAction();
		} else {
			if (checkMyTurn() && (buying || selling)) {
				market();
			} else {
				playNotPrepared();
			}
		}
	}

	/**
	 * audio che viene riprodotto se si tenta di fare una mossa troppe volte
	 * quando non è il proprio turno
	 */
	private void playNotPrepared() {
		this.countretryaction++;
		pw.println("You are not prepared!");
		if (this.countretryaction < 5) {
			return;
		}
		this.countretryaction = 0;
		final int buffersize = 128000;
		File soundFile = null;
		AudioFormat audioFormat;
		SourceDataLine sourceLine = null;
		String strFilename = "sound/youarenotprepared.wav";

		try {
			soundFile = new File(strFilename);
		} catch (Exception e) {
			log.log(Level.SEVERE, e.toString(), e);
		}

		try (AudioInputStream audioStream = AudioSystem.getAudioInputStream(soundFile)) {
			audioFormat = audioStream.getFormat();
			DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
			sourceLine = (SourceDataLine) AudioSystem.getLine(info);
			sourceLine.open(audioFormat);
			sourceLine.start();
			int nBytesRead = 0;
			byte[] abData = new byte[buffersize];
			while (nBytesRead != -1) {
				nBytesRead = audioStream.read(abData, 0, abData.length);
				if (nBytesRead >= 0) {
					sourceLine.write(abData, 0, nBytesRead);
				}
			}
			sourceLine.drain();
			sourceLine.close();
			audioStream.close();
		} catch (Exception e) {
			if (sourceLine != null) {
				sourceLine.close();
			}
			log.log(Level.SEVERE, e.toString(), e);
		}
	}

	/**
	 * stampa la mappa della partita
	 */
	private void mapUI() {
		clear();
		stampMapAndBonuses();
		Town[] arrayoftowns = giveMeTowns();
		int y = 0;
		for (Town t : arrayoftowns) {
			String color = giveMeColor(t.getType());
			pw.print("-" + t.getName() + " is " + color + " ");
			y++;
			if (y % 5 == 0) {
				pw.println();
			}
		}
		Bonus b;
		boolean ok = false;
		while (!ok) {
			try {
				pw.println("BONUS TOWN COLOR");
				pw.print("Gold =>");
				b = connect.getBonusGold(this.myplayername);
				this.stampBonus(b);
				pw.println();
				pw.print("Silver =>");
				b = connect.getBonusSilver(this.myplayername);
				this.stampBonus(b);
				pw.println();
				pw.print("Bronze =>");
				b = connect.getBonusBronze(this.myplayername);
				this.stampBonus(b);
				pw.println();
				pw.print("Iron =>");
				b = connect.getBonusIron(this.myplayername);
				this.stampBonus(b);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.println();
		ok = false;
		while (!ok) {
			try {
				pw.println("-BONUS REGION");
				pw.print("Coast ");
				b = connect.getRegion(this.myplayername, Constant.COASTNAME).getBonus();
				this.stampBonus(b);
				pw.println();
				pw.print("Hills ");
				b = connect.getRegion(this.myplayername, Constant.HILLSNAME).getBonus();
				this.stampBonus(b);
				pw.println();
				pw.print("Mountains ");
				b = connect.getRegion(this.myplayername, Constant.MOUNTAINSNAME).getBonus();
				this.stampBonus(b);
				pw.println();
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		King tempking = giveMeKing();
		pw.println("-King is in: " + tempking.getTown().getName());
	}

	private void stampMapAndBonuses() {
		Scanner in;
		int index = 0;
		pw.println("      MAP               TOWN'S BONUS");
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + this.connect.getConfig().getMap() + "CLI.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				pw.print(in.nextLine());
				pw.print("    ");
				stampBonusTown(index);
				index++;
				pw.print("    ");
				stampBonusTown(index);
				index++;
				pw.print("    ");
				stampBonusTown(index);
				index++;
				pw.flush();
				pw.println();
				;
			}
			in.close();
		} catch (IOException e) {
			log.log(Level.SEVERE, e.toString(), e);
			return;
		}
	}

	/**
	 * stmapa le statistiche dei player
	 */
	private void playerUI() {
		clear();
		boolean ok = false;
		while (!ok) {
			try {
				for (Player p : connect.getPlayers(this.myplayername)) {
					pw.println(p.getName() + " nob:" + p.getNobilityPosition() + " rich:" + p.getCoinsPosition()
							+ " assistants:" + p.getAssistant() + " vic:" + p.getVictoryPosition());
				}
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
	}

	/**
	 * stampa i balconi dei concili
	 */
	private void balconyUI() {
		clear();
		pw.println("BALCONIES AND TILES");
		// coast balcony and tiles
		model.Region r = getRegion(Constant.COASTNAME);
		if (r == null) {
			return;
		}
		pw.print("Coast's: ");
		pw.flush();
		printCouncillorBalcony(r.getCouncBalcony());
		printTilesOfRegion(r);
		pw.println();
		// hills balcony and tiles
		r = getRegion(Constant.HILLSNAME);
		if (r == null) {
			return;
		}
		pw.print("Hills': ");
		pw.flush();
		printCouncillorBalcony(r.getCouncBalcony());
		printTilesOfRegion(r);
		pw.println();
		// mountains balcony and tiles
		r = getRegion(Constant.MOUNTAINSNAME);
		if (r == null) {
			return;
		}
		pw.print("Mountains': ");
		pw.flush();
		printCouncillorBalcony(r.getCouncBalcony());
		printTilesOfRegion(r);
		pw.println();
		// king balcony
		pw.print("King'S: ");
		pw.flush();
		printCouncillorBalcony(giveMeKing().getBalcony());
		pw.println();
	}

	/**
	 * data na stringa restituisce l'oggeto regione
	 * 
	 * @param r
	 * @return
	 */
	private Region getRegion(String r) {
		model.Region region = null;
		boolean ok = false;
		while (!ok) {
			try {
				region = connect.getRegion(this.myplayername, r);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		return region;
	}

	private void printCouncillorBalcony(CouncilBalcony cb) {
		String color;
		int length = cb.showCouncillors().length;
		color = giveMeColor(cb.showCouncillors()[0].getColor());
		pw.print(color);
		for (int i = 1; i < length; i++) {
			color = giveMeColor(cb.showCouncillors()[i].getColor());
			pw.print("-" + color);
		}
		pw.println();
	}

	private void showEmporiums() {
		clear();
		emporiumsUI();
	}

	/**
	 * stampa gli empori dei giocatori
	 */
	private void emporiumsUI() {
		List<Emporium> emp = null;
		boolean ok = false;
		while (!ok) {
			try {
				emp = this.connect.getEmporiums(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		List<String> players = null;
		ok = false;
		while (!ok) {
			try {
				players = this.connect.getPlayersName(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.println("Emporiums built");
		for (String name : players) {
			pw.print(name + ": ");
			for (Emporium e : emp) {
				if (e.getOwner().getName().equals(name)) {
					pw.print(e.getTown().getName() + " ");
				}
			}
			pw.println();
		}
		pw.flush();
	}

	/**
	 * stampa le carte permesso dei giocatori
	 */
	private void showBusinessTilePlayers() {
		clear();
		List<String> playersname = null;
		boolean ok = false;
		while (!ok) {
			try {
				playersname = this.connect.getPlayersName(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		for (String name : playersname) {
			pw.println(name);
			hisBusinessTile(name);
		}
	}

	/**
	 * stampa le carte permesso del giocatore parametro
	 * 
	 * @param name
	 */
	private void hisBusinessTile(String name) {
		int index = 1;
		List<BusinessTile> listofbusinesstiles = null;
		boolean ok = false;
		while (!ok) {
			try {
				listofbusinesstiles = this.connect.getPlayersTile(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		for (BusinessTile temp : listofbusinesstiles) {
			if (temp.getPlayer().getName().equals(name)) {
				pw.print(index + ". ");
				for (Town t : temp.getTown()) {
					pw.print(t.getName() + " ");
				}
				pw.flush();
				pw.println();
			}
		}
	}

	private void showMyHand() {
		clear();
		myHand();
	}

	/**
	 * stampa le track della partita
	 */
	private void trackUI() {
		clear();
		pw.println("Select the track you want to show.");
		pw.println("1. Coins track");
		pw.println("2. Nobility track");
		pw.println("3. Victory track");
		int scelta = getIntInput();
		switch (scelta) {
		case 1:
			coinsTrack();
			break;
		case 2:
			nobilityTrack();
			break;
		case 3:
			victoryTrack();
			break;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
		}
	}

	private void coinsTrack() {
		clear();
		List<Player> players = giveMePlayers();
		players.sort((Player p1, Player p2) -> Integer.compare(p1.getCoinsPosition(), p2.getCoinsPosition()));
		int max = players.get(players.size() - 1).getCoinsPosition();
		for (int i = 0; i < max + 1; i++) {
			pw.print(i);
			for (int j = 0; j < players.size(); j++) {
				if (players.get(j).getCoinsPosition() == i) {
					pw.print(" <= " + players.get(j).getName());
				}
			}
			pw.println();
		}
	}

	private void nobilityTrack() {
		clear();
		// sorting players
		List<Player> players = giveMePlayers();
		players.sort((Player p1, Player p2) -> Integer.compare(p1.getNobilityPosition(), p2.getNobilityPosition()));
		// printing the nobility
		pw.println("-NOBILITY TRACK:");
		NobilityTrack tempnb = null;
		boolean ok = false;
		while (!ok) {
			try {
				tempnb = this.connect.getNobilityTrack(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		Map<Integer, Bonus> nb = tempnb.getTrack();
		for (Map.Entry<Integer, Bonus> entry : nb.entrySet()) {
			pw.print(entry.getKey() + " => ");
			stampBonus(entry.getValue());
			for (int j = 0; j < players.size(); j++) {
				if (entry.getKey() == players.get(j).getNobilityPosition()) {
					pw.print(" <= " + players.get(j).getName());
				}
			}
			pw.println();
		}
	}

	private void victoryTrack() {
		clear();
		List<Player> players = giveMePlayers();
		players.sort((Player p1, Player p2) -> Integer.compare(p1.getVictoryPosition(), p2.getVictoryPosition()));
		int max = players.get(players.size() - 1).getVictoryPosition();
		for (int i = 0; i < max + 1; i++) {
			pw.print(i);
			for (int j = 0; j < players.size(); j++) {
				if (players.get(j).getVictoryPosition() == i) {
					pw.print(" <= " + players.get(j).getName());
				}
			}
			pw.println();
		}
	}

	/**
	 * stampa le carte del giocatore con cui è stato fatto il login
	 */
	private void myHand() {
		pw.print("My hand: ");
		List<Player> listofplayers = giveMePlayers();
		for (Player p : listofplayers) {
			if (p.getName().equals(this.myplayername)) {
				List<PoliticCard> pc = p.getHand().showCard();
				int length = pc.size();
				String color = giveMeColor(pc.get(0).getColor());
				pw.print(color);
				for (int i = 1; i < length; i++) {
					color = giveMeColor(pc.get(i).getColor());
					pw.print("-" + color);
					pw.flush();
				}
				pw.println();
			}
		}
	}

	/**
	 * stampa le carte permesso che sono a disposizione in ogni regione
	 * 
	 * @param r
	 */
	private void printTilesOfRegion(Region r) {
		BusinessTile[] bt = r.showTiles();
		pw.println("1 BUSINESS TILE");
		pw.print("city: ");
		pw.flush();
		for (Town t : bt[0].getTown()) {
			pw.print(t.getName() + " ");
		}
		pw.println();
		stampBonus(bt[0].getBonus());
		pw.flush();
		pw.println();
		// second tiles
		pw.println("2 BUSINESS TILE");
		pw.print("city: ");
		for (Town t : bt[1].getTown()) {
			pw.print(t.getName() + " ");
		}
		pw.println();
		stampBonus(bt[1].getBonus());
		pw.println();
		pw.flush();
	}

	/**
	 * stampa i bonus delle città
	 * 
	 * @param index
	 */
	private void stampBonusTown(int index) {
		if (index > 14) {
			return;
		}
		Town[] t = null;
		boolean ok = false;
		while (!ok) {
			try {
				t = this.connect.getTowns(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.print((index + 1) + ") " + t[index].getName() + " => ");
		stampBonus(t[index].getBonus());
	}

	/**
	 * stampa un bonus generico passato come parametro
	 * 
	 * @param b
	 */
	private void stampBonus(Bonus b) {
		if (b.getAssistants() != 0) {
			pw.print("Assistants: +" + b.getAssistants() + " ");
		}
		if (b.getCoins() != 0) {
			pw.print("Coins: +" + b.getCoins() + " ");
		}
		if (b.getCards() != 0) {
			pw.print("Cards: +" + b.getCards() + " ");
		}
		if (b.getNobilityMoves() != 0) {
			pw.print("Nobility points: +" + b.getNobilityMoves() + " ");
		}
		if (b.getVictoryPoints() != 0) {
			pw.print("Victory points: +" + b.getVictoryPoints() + " ");
		}
	}

	/**
	 * gestisce le azioni del market del player
	 */
	private void market() {
		clear();
		boolean buying = false;
		boolean selling = false;
		boolean ok = false;
		while (!ok) {
			try {
				selling = this.connect.isSellingPhase(this.myplayername);
				buying = this.connect.isBuyingPhase(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		if (selling) {
			pw.println("Do you want to sell something? [Y/N]");
		}
		if (buying) {
			pw.println("Do you want to buy something? [Y/N]");
		}
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			marketCall(selling, buying);
			return;
		case "n":
			return;
		default:
			market();
		}
	}

	private void marketCall(boolean selling, boolean buying) {
		if (selling) {
			setSupply();
		}
		if (buying) {
			buySupplies();
		}
	}

	/**
	 * permette di mettere in vendità sul market
	 */
	private void setSupply() {
		clear();
		pw.println("What do you want to sell?");
		pw.println("1. Assistant");
		pw.println("2. BusinessTile");
		pw.println("3. PoliticCards");
		pw.println("0. Back");
		int scelta = getIntInput();
		// input control
		while (scelta < 0 || scelta > 3) {
			pw.println(Constant.CHOICENOTALLOWED);
			scelta = getIntInput();
		}
		switch (scelta) {
		case 1:
			assistantSupply();
			break;
		case 2:
			businessSupply();
			break;
		case 3:
			politicSupply();
			break;
		default:
			return;
		}
	}

	private void assistantSupply() {
		Player x = null;
		List<Player> listofplayers = giveMePlayers();
		for (Player p : listofplayers) {
			if (p.getName().equals(myplayername)) {
				x = p;
			}
		}
		if (x == null) {
			clear();
			pw.println("Player not found!");
			return;
		}
		pw.println("You have " + x.getAssistant() + " assistants.");
		pw.println("How many assistants you want to sell?");
		int scelta = getIntInput();
		// input control
		while (scelta < 1 || scelta > x.getAssistant()) {
			pw.println(Constant.CHOICENOTALLOWED);
			scelta = getIntInput();
		} // end control
		int price = getPrice();
		pw.println("Do you want to sell " + scelta + " assistants for " + price + " coins? [Y/N]");
		finalizeAssistantSupply(scelta, price);
		return;
	}

	private void finalizeAssistantSupply(int scelta, int price) {
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			createSupplyAssCall(scelta, price);
			return;
		case "n":
			return;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
			return;
		}
	}

	private void createSupplyAssCall(int scelta, int price) {
		boolean ok = false;
		int[] emptyarray = {};
		while (!ok) {
			try {
				this.connect.createSupply(this.myplayername, scelta, emptyarray, emptyarray, price);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
	}

	private void businessSupply() {
		hisBusinessTile(this.myplayername);
		/*
		 * select which business tiles wants to sell
		 */
		pw.println("Select the business tiles you want to sell, to end the choice write 0.");
		List<BusinessTile> listofbusinesstiles = null;
		boolean ok = false;
		while (!ok) {
			try {
				listofbusinesstiles = this.connect.getPlayersTile(myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		int[] index = getIndexOfBusinessTileInArray(listofbusinesstiles);
		if (index.length == 0) {
			pw.println("At least one tiles is required!");
		}
		/*
		 * select price
		 */
		int price = getPrice();
		pw.println(Constant.AREYOUSURE);
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			createSupplyBusCall(index, price);
			return;
		case "n":
			return;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
			return;
		}
	}

	private int[] getIndexOfBusinessTileInArray(List<BusinessTile> listofbusinesstiles) {
		List<Integer> indici = new ArrayList<>();
		boolean end = false;
		int temp;
		int length = indici.size();
		int max = 0;
		for (BusinessTile bt : listofbusinesstiles) {
			if (bt.getPlayer().getName().equals(myplayername)) {
				max++;
			}
		}
		while (!end && length > max) {
			temp = getIntInput();
			// input control
			while (temp < 0 || temp > max) {
				pw.println(Constant.CHOICENOTALLOWED);
				temp = getIntInput();
			}
			// end control
			if (temp == 0) {
				end = true;
			} else {
				indici.add(temp);
			}
			length = indici.size();
		}
		// control if player choose at least 1 tiles
		if (indici.isEmpty()) {
			return new int[0];
		}
		// transform the list in array in order to adapt to parameter
		return transformList(indici, listofbusinesstiles);
	}

	private int[] transformList(List<Integer> indici, List<BusinessTile> listofbusinesstiles) {
		int lookfor;
		boolean checked = false;
		// transform the list in array in order to adapt to parameter
		int[] index = new int[indici.size()];
		for (int l = 0; l < indici.size(); l++) {
			lookfor = 0;
			for (int j = 0; j < listofbusinesstiles.size() && checked; j++) {
				if (listofbusinesstiles.get(j).getPlayer().getName().equals(this.myplayername)) {
					lookfor++;
				}
				if (lookfor == indici.get(l)) {
					index[l] = j;
					checked = true;
				}
			}
		}
		return index;
	}

	private void createSupplyBusCall(int[] index, int price) {
		int[] emptyarray = {};
		boolean ok = false;
		while (!ok) {
			try {
				this.connect.createSupply(this.myplayername, 0, index, emptyarray, price);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
	}

	private void politicSupply() {
		showMyHand();
		pw.println("Select politic cards to sell.");
		int max = 0;
		List<Player> listofplayers = giveMePlayers();
		for (Player p : listofplayers) {
			if (p.getName().equals(myplayername)) {
				max = p.getHand().getNumberOfCards();
			}
		}
		int[] index = selectIndexOfSomething(max, -1);
		if (index.length == 0) {
			pw.println("At least one card is required!");
			return;
		}
		/*
		 * select price
		 */
		int price = getPrice();
		pw.println(Constant.AREYOUSURE);
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			createSupplyPolCall(index, price);
			return;
		case "n":
			return;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
			return;
		}
	}

	private void createSupplyPolCall(int[] index, int price) {
		int[] emptyarray = {};
		boolean ok = false;
		while (!ok) {
			try {
				this.connect.createSupply(this.myplayername, 0, emptyarray, index, price);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
	}

	/**
	 * prende da input un numero non finito di indici e li mette in un array
	 * 
	 * @param maxcontrol
	 *            permette di impostare un numero massimo preso da input
	 * @param numberofinputs
	 *            permette di impostare il numero massimo di indici presi in
	 *            input, -1 per non essere finito
	 * @return
	 */
	private int[] selectIndexOfSomething(int maxcontrol, int numberofinputs) {
		int numofin;
		if (numberofinputs == -1) {
			numofin = 1000000000;
		} else {
			numofin = numberofinputs;
		}
		List<Integer> indici = new ArrayList<>();
		int length = indici.size();
		boolean end = false;
		int temp;
		while (!end && length < numofin) {
			temp = getIntInput();
			// input control
			while (temp < 0 || temp > maxcontrol) {
				pw.println(Constant.CHOICENOTALLOWED);
				temp = getIntInput();
			}
			// end control
			if (temp == 0) {
				end = true;
			} else {
				indici.add(temp);
			}
			length = indici.size();
		}
		// control if it player discarded at least 1 card
		if (indici.isEmpty()) {
			return new int[0];
		}
		// transform the list in array in order to adapt to parameter
		int[] index = new int[indici.size()];
		for (int j = 0; j < indici.size(); j++) {
			// -1 is needed in order to adapt to the index
			index[j] = indici.get(j) - 1;
		}
		return index;
	}

	private int getPrice() {
		pw.println("Select price.");
		int price = getIntInput();
		// input control
		while (price < 1) {
			pw.println(Constant.CHOICENOTALLOWED);
			price = getIntInput();
		} // end control
		return price;
	}

	/**
	 * permette l'acquisto di un oggetto da market
	 */
	private void buySupplies() {
		clear();
		showSupplies();
		pw.println("Select which one you want to buy");
		List<Supply> supplies = null;
		Player me = null;
		List<Player> listofplayers = giveMePlayers();
		boolean ok = false;
		while (!ok) {
			try {
				supplies = this.connect.getSupplies(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		for (Player p : listofplayers) {
			if (p.getName().equals(this.myplayername)) {
				me = p;
			}
		}
		if (me == null) {
			clear();
			pw.println("Player not found!");
			return;
		}
		if (supplies.isEmpty()) {
			pw.println("Nothing to be bought");
			return;
		}
		finalizeBuySupply(me, supplies);
		return;
	}

	private void finalizeBuySupply(Player me, List<Supply> supplies) {
		int scelta = getIntInput();
		// input control
		while (scelta < 0 || scelta > supplies.size()) {
			pw.println(Constant.CHOICENOTALLOWED);
			scelta = getIntInput();
		} // end control
		if (me.getCoinsPosition() < supplies.get(scelta - 1).getPrice()) {
			finalizeBuySupplyErr();
		} else {
			finalizeBuySupplyOn(scelta);
		}
	}

	private void finalizeBuySupplyErr() {
		String ret;
		pw.println(Constant.NOTENOUGHCOINS + "Retry? [Y/N]");
		ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			buySupplies();
			return;
		case "n":
			return;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
			return;
		}
	}

	private void finalizeBuySupplyOn(int scelta) {
		String ret;
		pw.println(Constant.AREYOUSURE);
		ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			buySupplyCall(scelta);
			return;
		case "n":
			return;
		default:
			buySupplies();
		}
	}

	private void buySupplyCall(int scelta) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.buySupply(this.myplayername, scelta - 1);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.println(outcome);
	}

	/**
	 * visualizza le offerte presentis ul market
	 */
	private void showSupplies() {
		clear();
		List<Supply> supplies = null;
		boolean ok = false;
		while (!ok) {
			try {
				supplies = this.connect.getSupplies(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		int count = 1;
		for (Supply sup : supplies) {
			pw.println(count + ". " + sup.getSeller().getName() + " is selling:");
			if (sup.getAssistant() != 0) {
				pw.println(sup.getAssistant() + " assistants");
			}
			if (!sup.getBusiness().isEmpty()) {
				pw.println(sup.getBusiness().size() + " business tile with those cities:");
				stampTileTowns(sup);
			}
			if (!sup.getPolitics().isEmpty()) {
				pw.println(sup.getPolitics().size() + "politic cards of those colors:");
				for (PoliticCard pc : sup.getPolitics()) {
					pw.println(giveMeColor(pc.getColor()));
				}
			}
			pw.println("for " + sup.getPrice() + " coins");
			count++;
		}
	}

	private void stampTileTowns(Supply sup) {
		for (int i = 0; i < sup.getBusiness().size(); i++) {
			pw.print(i + 1 + ". ");
			for (Town t : sup.getBusiness().get(i).getTown()) {
				pw.print(t.getName() + " ");
			}
			pw.println();
		}
	}

	// actions
	/**
	 * finisce il turno del giocatore
	 */
	private void endMyTurn() {
		if (!checkMyTurn()) {
			playNotPrepared();
			return;
		}
		pw.println(Constant.AREYOUSURE);
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			endTurnCall();
			return;
		case "n":
			return;
		default:
			endMyTurn();
		}
	}

	private void endTurnCall() {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.endTurn(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.println(outcome);
	}

	/**
	 * gestisce le azioni della partita
	 */
	private void makeAction() {
		clear();
		pw.println("9. Back");
		pw.println("Main action:");
		pw.println("1. Elect councillor");
		pw.println("2. Acquire business tile");
		pw.println("3. Build emporium with tile");
		pw.println("4. Build emporium with king");
		pw.println("Quick action:");
		pw.println("5. Engage assistant");
		pw.println("6. Change business tile");
		pw.println("7. Elect councillor with assistants");
		pw.println("8. Additional main action");
		int scelta = getIntInput();
		switch (scelta) {
		case 1:
			electCouncillorWithEarn();
			break;
		case 2:
			acquireBusinessTile();
			break;
		case 3:
			buildEmporiumWithTile();
			break;
		case 4:
			buildEmporiumWithKing();
			break;
		case 5:
			engageAssistant();
			break;
		case 6:
			changeBusinessTile();
			break;
		case 7:
			electCouncillorPaying();
			break;
		case 8:
			additionalMainAction();
			break;
		case 9:
			break;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
			makeAction();
		}
	}

	/**
	 * main action elegge un consigliere
	 */
	private void electCouncillorWithEarn() {
		clear();
		// print regions' balconies
		pw.print("1. " + Constant.COASTNAME);
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.COASTNAME));
		pw.print("2. " + Constant.HILLSNAME);
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.HILLSNAME));
		pw.print("3. " + Constant.MOUNTAINSNAME);
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.MOUNTAINSNAME));
		// print available councillor to be elected
		int index = 1;
		pw.print("Councillor free: ");
		List<Councillor> listofcouncillorpool = null;
		boolean ok = false;
		while (!ok) {
			try {
				listofcouncillorpool = this.connect.getCouncillorPoolList(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.flush();
		for (Councillor c : listofcouncillorpool) {
			pw.print(index + "." + giveMeColor(c.getColor()) + " ");
			index++;
		}
		pw.println();
		pw.println("Choose the region in which you want to add the councillor");
		// region selection
		String regione = selectRegion();
		// councillor selection
		pw.println("Select one councillor to elect. Write the number.");
		int scelta = getIntInput();
		int max = 0;
		List<Player> listofplayers = giveMePlayers();
		for (Player p : listofplayers) {
			if (p.getName().equals(myplayername)) {
				max = p.getHand().getNumberOfCards();
			}
		}
		while (scelta < 1 || scelta > max) {
			pw.println(Constant.CHOICENOTALLOWED);
			scelta = getIntInput();
		} // end control
		scelta--;// it's needed to adapt the choice to the index for the list
		finalizeElectCouncillorWithEarn(scelta, regione);
		return;
	}

	private void finalizeElectCouncillorWithEarn(int scelta, String regione) {
		pw.println("You will put the councillor selected in the region chosen and you will earn "
				+ Constant.BONUSELECTCOUNCILLOR + " coins. " + Constant.AREYOUSURE);
		String choice = getStringInput();
		choice = choice.toLowerCase();
		switch (choice) {
		case "y":
			electCouncillorWithEarnCall(scelta, regione);
			return;
		case "n":
			return;
		default:
			electCouncillorWithEarn();
		}
	}

	private void electCouncillorWithEarnCall(int scelta, String regione) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.electCouncillorWithEarn(this.myplayername, scelta, regione);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			}
		}
		pw.println(outcome);
	}

	private String selectRegion() {
		int scelta = getIntInput();
		// input control
		while (scelta < 1 || scelta > 3) {
			pw.println(Constant.CHOICENOTALLOWED);
			scelta = getIntInput();
		} // end control
		String regione;
		switch (scelta) {
		case 1:
			regione = Constant.COASTNAME;
			break;
		case 2:
			regione = Constant.HILLSNAME;
			break;
		case 3:
			regione = Constant.MOUNTAINSNAME;
			break;
		default:
			regione = null;
			break;
		}
		return regione;
	}

	/**
	 * main action acquista una carta permesso
	 */
	private void acquireBusinessTile() {
		clear();
		// print coast tiles
		pw.println("1. " + Constant.COASTNAME);
		printTilesOfRegion(giveMeRegion(Constant.COASTNAME));
		pw.print("-" + Constant.COASTNAME + "'s council: ");
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.COASTNAME));
		pw.println("2. " + Constant.HILLSNAME);
		printTilesOfRegion(giveMeRegion(Constant.HILLSNAME));
		pw.print("-" + Constant.HILLSNAME + "' council: ");
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.HILLSNAME));
		pw.println("3. " + Constant.MOUNTAINSNAME);
		printTilesOfRegion(giveMeRegion(Constant.MOUNTAINSNAME));
		pw.print("-" + Constant.MOUNTAINSNAME + "' council: ");
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.MOUNTAINSNAME));
		// printing available cards of the player
		myHand();
		pw.println();
		pw.println("Choose the region from where you want to buy");
		// region selection
		String regione = selectRegion();
		// tiles selection
		pw.println("Select which one of the two cards you want by tapping the number of the card. [1/2]");
		int theonetobuy = getIntInput();
		// input control
		while (theonetobuy < 1 || theonetobuy > 2) {
			pw.println(Constant.CHOICENOTALLOWED);
			theonetobuy = getIntInput();
		}
		// end control
		theonetobuy--;
		// discarding cards
		pw.println("Select the politic cards to satisfy the Council, to end the choice write 0.");
		int max = 0;
		List<Player> listofplayers = giveMePlayers();
		for (Player p : listofplayers) {
			if (p.getName().equals(myplayername)) {
				max = p.getHand().getNumberOfCards();
			}
		}
		int[] index = selectIndexOfSomething(max, 5);
		if (index.length == 0) {
			pw.println("At least one card discarded is required!");
			return;
		}
		finalizeAcquireBusinessTile(regione, theonetobuy, index);
		return;
	}

	private void finalizeAcquireBusinessTile(String regione, int theonetobuy, int[] index) {
		pw.println(Constant.AREYOUSURE);
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			acquireBusinessTileCall(regione, theonetobuy, index);
			return;
		case "n":
			return;
		default:
			acquireBusinessTile();
			return;
		}
	}

	private void acquireBusinessTileCall(String regione, int theonetobuy, int[] index) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.acquireBusinessTile(this.myplayername, regione, theonetobuy, index);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			}
		}
		pw.println(outcome);
	}

	/**
	 * main action costruisce emporio scartando una carta permesso
	 */
	private void buildEmporiumWithTile() {
		List<BusinessTile> b = getPlayerTiles();
		if (b.isEmpty()) {
			pw.println("You need tile to build emporium!");
			return;
		}
		clear();
		emporiumsUI();
		hisBusinessTile(this.myplayername);
		pw.println("Select which business tile use to build the emporium.");
		int getwhich = getIntInput();
		int max = 0;
		for (BusinessTile bt : b) {
			if (bt.getPlayer().getName().equals(myplayername)) {
				max++;
			}
		}
		// input control
		while (getwhich < 1 || getwhich > max) {
			pw.println(Constant.CHOICENOTALLOWED);
			getwhich = getIntInput();
		} // end control
			// find which business tile wants to discard
		int temp = 0;
		int l;// index of the businesstile
		for (l = 0; l < b.size() && temp != getwhich; l++) {
			if (b.get(l).getPlayer().getName().equals(this.myplayername)) {
				temp++;
			}
		}
		l--;// because the for increase l at the end of the cycle
		String town;
		int t = getTheTownInBusinessTile(b, l);
		town = b.get(l).getTown().get(t).getName();
		finalizeBuildEmporiumWithTile(l, town);
		return;
	}

	private int getTheTownInBusinessTile(List<BusinessTile> b, int l) {
		int t;
		if (b.get(l).getTown().size() > 1) {
			pw.println("Select which town of the tile you want to build in");
			t = getIntInput();
			// input control
			while (t < 1 || t > b.get(l).getTown().size()) {
				pw.println(Constant.CHOICENOTALLOWED);
				t = getIntInput();
			} // end control
			t--;
		} else {
			t = 0;
		}
		return t;
	}

	private List<BusinessTile> getPlayerTiles() {
		List<BusinessTile> b = null;
		boolean ok = false;
		while (!ok) {
			try {
				b = this.connect.getPlayersTile(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		// check if player can build something
		boolean check = false;
		for (int i = 0; i < b.size() && !check; i++) {
			if (b.get(i).getPlayer().getName().equals(myplayername)) {
				check = true;
			}
		}
		if (!check) {
			return new ArrayList<>();
		}
		return b;
	}

	private void finalizeBuildEmporiumWithTile(int l, String town) {
		pw.println(Constant.AREYOUSURE);
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			buildEmporiumWithTileCall(l, town);
			return;
		case "n":
			return;
		default:
			buildEmporiumWithTile();
			return;
		}
	}

	private void buildEmporiumWithTileCall(int l, String town) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.buildEmporiumWithTile(this.myplayername, l, town);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			}
		}
		pw.println(outcome);
	}

	/**
	 * main action costruisce un emporio soddisfando il concilio del re
	 */
	private void buildEmporiumWithKing() {
		clear();
		pw.print("King'S: ");
		pw.flush();
		printCouncillorBalcony(giveMeKing().getBalcony());
		myHand();
		emporiumsUI();
		// map
		stampMapAndBonuses();
		pw.println("King is in: " + giveMeKing().getTown().getName());
		// end map
		pw.println("Write the town you want to build in");
		String town = getStringInput();
		Town[] t = giveMeTowns();
		boolean found = false;
		for (int i = 0; i < t.length && !found; i++) {
			if (t[i].getName().equals(town)) {
				found = true;
			}
		}
		// input control
		if (!found) {
			finalizeBuildEmporiumWithKingErr();
			return;
		} // end control
		pw.println("You have to satisfy the council. Select the card you want to discard. Write 0 to end the discard.");
		List<Player> listofplayers = giveMePlayers();
		int max = 0;
		for (Player p : listofplayers) {
			if (p.getName().equals(myplayername)) {
				max = p.getHand().getNumberOfCards();
			}
		}
		int[] index = selectIndexOfSomething(max, 4);
		if (index.length == 0) {
			pw.println("At least one card discarded is required!");
			return;
		}
		finalizeBuildEmporiumWithKing(town, index);
		return;
	}

	private void finalizeBuildEmporiumWithKingErr() {
		pw.println("Town doesn't exist. Do you want to retry the action? Y/N");
		String scelta = getStringInput();
		scelta = scelta.toLowerCase();
		switch (scelta) {
		case "y":
			buildEmporiumWithKing();
			return;
		case "n":
			return;
		default:
			pw.println(Constant.CHOICENOTALLOWED);
			return;
		}
	}

	private void finalizeBuildEmporiumWithKing(String town, int[] index) {
		pw.println(Constant.AREYOUSURE);
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			buildEmporiumWithKingCall(town, index);
			return;
		case "n":
			return;
		default:
			buildEmporiumWithKing();
		}
	}

	private void buildEmporiumWithKingCall(String town, int[] index) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.buildEmporiumWithKing(this.myplayername, town, index);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			}
		}
		pw.println(outcome);
	}

	/**
	 * quick action ingaggia un assistente pagando tre monete
	 */
	private void engageAssistant() {
		clear();
		pw.println("You will pay 3 coins to buy the assistant." + Constant.AREYOUSURE);
		String scelta = getStringInput();
		scelta = scelta.toLowerCase();
		switch (scelta) {
		case "y":
			engageAssistantCall();
			return;
		case "n":
			return;
		default:
			engageAssistant();
		}
	}

	private void engageAssistantCall() {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.engageAssistant(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			}
		}
		pw.println(outcome);
	}

	/**
	 * quick action scarta le due carte permesso della regione selezionata
	 */
	private void changeBusinessTile() {
		// information print
		pw.print("Coast's: ");
		pw.flush();
		model.Region r = getRegion(Constant.COASTNAME);
		if (r == null) {
			return;
		}
		printTilesOfRegion(r);
		pw.print("Hills': ");
		pw.flush();
		r = getRegion(Constant.HILLSNAME);
		if (r == null) {
			return;
		}
		printTilesOfRegion(r);
		pw.print("Mountains': ");
		pw.flush();
		r = getRegion(Constant.MOUNTAINSNAME);
		if (r == null) {
			return;
		}
		printTilesOfRegion(r);
		// region choice
		pw.print("Select the region");
		pw.flush();
		String regione = selectRegion();
		finalizeChangeBusinessTile(regione);
		return;
	}

	private void finalizeChangeBusinessTile(String regione) {
		pw.println(Constant.AREYOUSURE);
		String ret = getStringInput();
		ret = ret.toLowerCase();
		switch (ret) {
		case "y":
			changeBusinessTileCall(regione);
			return;
		case "n":
			return;
		default:
			changeBusinessTile();
		}
	}

	private void changeBusinessTileCall(String regione) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.changeBusinessTiles(this.myplayername, regione);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			}
		}
		pw.println(outcome);
	}

	/**
	 * quick action elegge un consigliere pagando un assistente
	 */
	private void electCouncillorPaying() {
		pw.print("1. " + Constant.COASTNAME);
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.COASTNAME));
		pw.print("2. " + Constant.HILLSNAME);
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.HILLSNAME));
		pw.print("3. " + Constant.MOUNTAINSNAME);
		printCouncillorBalcony(giveMeCouncillorBalcony(Constant.MOUNTAINSNAME));
		pw.println("Choose the region in which you want to add the councillor");
		String regione = selectRegion();
		pw.print("Select one ");
		int index = 1;
		boolean ok = false;
		while (!ok) {
			try {
				for (Councillor c : this.connect.getCouncillorPoolList(this.myplayername)) {
					pw.print(index + "." + giveMeColor(c.getColor()) + " ");
					index++;
				}
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.flush();
		int scelta = getIntInput();
		// input control
		while (scelta < 1 || scelta > 4) {
			pw.println(Constant.CHOICENOTALLOWED);
			scelta = getIntInput();
		} // end input control
		scelta--;// it's needed to adapt the choice to the index for the list
		pw.println("You will put the councillor selected in the region chosen." + Constant.AREYOUSURE);
		String choice = getStringInput();
		choice = choice.toLowerCase();
		switch (choice) {
		case "y":
			electCouncillorPayingCall(scelta, regione);
			return;
		case "n":
			return;
		default:
			electCouncillorPaying();
		}
	}

	private void electCouncillorPayingCall(int scelta, String regione) {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.electCouncillorPaying(this.myplayername, scelta, regione);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			}
		}
		pw.println(outcome);
	}

	/**
	 * quick action aggiunge una main action
	 */
	private void additionalMainAction() {
		clear();
		pw.println("You will consume your quick action to make another main action if it's already consumed."
				+ Constant.AREYOUSURE);
		String scelta = getStringInput();
		scelta = scelta.toLowerCase();
		switch (scelta) {
		case "y":
			additionalMainActionCall();
			return;
		case "n":
			return;
		default:
			engageAssistant();
		}
	}

	private void additionalMainActionCall() {
		String outcome = null;
		boolean ok = false;
		while (!ok) {
			try {
				outcome = this.connect.additionalMainAction(this.myplayername);
				ok = true;
			} catch (IllegalArgumentException e) {
				clear();
				log.log(Level.SEVERE, e.getMessage(), e);
				return;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		pw.println(outcome);
	}

	/**
	 * ritorna nome del colore dato per parametro
	 * 
	 * @param colorselected
	 * @return
	 */
	private String giveMeColor(Color colorselected) {
		Color[] colors = { Color.magenta, Color.blue, Color.white, Color.black, Color.orange, Color.pink, Color.cyan,
				Constant.GOLD, Constant.SILVER, Constant.BRONZE, Constant.IRON };
		String[] colorsname = { "PURPLE", "BLU", "WHITE", "BLACK", "ORANGE", "PINK", "MULTICOLOR", "GOLD", "SILVER",
				"BRONZE", "IRON" };
		boolean found = false;
		int i;
		for (i = 0; i < colors.length && !found; i++) {
			if (colors[i].equals(colorselected)) {
				found = true;
			}
		}
		i--;
		if (found) {
			return colorsname[i];
		} else {
			return "Color not found in the library";
		}
	}

	private boolean checkMyTurn() {
		return this.whoisnow.equals(myplayername);
	}

	private void clear() {
		pw.println("-------------------------------------------------------------------------------------");
	}

	/**
	 * gestisce l'input di un intero con i vari controlli dei caratteri non
	 * ammessi
	 * 
	 * @return
	 */
	private int getIntInput() {
		int input = -1;
		boolean berror = true;
		do {
			if (s.hasNextInt()) {
				input = s.nextInt();
				berror = false;
			} else {
				s.next();
				pw.println(Constant.CHOICENOTALLOWED);
			}
		} while (berror);
		return input;
	}

	/**
	 * gestisce l'input di una stringa
	 * 
	 * @return
	 */
	private String getStringInput() {
		String input = null;
		boolean berror = true;
		do {
			try {
				input = s.next();
				berror = false;
			} catch (Exception e) {
				log.log(Level.FINE, Constant.CHOICENOTALLOWED, e);
			}
		} while (berror);
		return input;
	}

	/**
	 * tenta di riconnettersi al server se avvenuta una
	 * ConnectionProblemException
	 * 
	 * @param e
	 */
	private void reconnect(ConnectionProblemException e) {
		clear();
		log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
		boolean done = false;
		while (!done) {
			try {
				this.connect.reconnect(this.myplayername);
				done = true;
			} catch (ConnectionProblemException e2) {
				log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e2);
				pw.println("Reconnecting...");
			}
		}
	}

	private List<Player> giveMePlayers() {
		boolean ok = false;
		List<Player> listofplayers = null;
		while (!ok) {
			try {
				listofplayers = this.connect.getPlayers(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		return listofplayers;
	}

	private King giveMeKing() {
		boolean ok = false;
		King king = null;
		while (!ok) {
			try {
				king = this.connect.getKing(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		return king;
	}

	private CouncilBalcony giveMeCouncillorBalcony(String where) {
		boolean ok = false;
		CouncilBalcony cb = null;
		while (!ok) {
			try {
				cb = this.connect.getCouncillorBalcony(this.myplayername, where);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		return cb;
	}

	private Region giveMeRegion(String where) {
		boolean ok = false;
		Region r = null;
		while (!ok) {
			try {
				r = this.connect.getRegion(this.myplayername, where);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		return r;
	}

	private Town[] giveMeTowns() {
		Town[] t = null;
		boolean ok = false;
		while (!ok) {
			try {
				t = this.connect.getTowns(this.myplayername);
				ok = true;
			} catch (ConnectionProblemException e) {
				reconnect(e);
			}
		}
		return t;
	}
}
