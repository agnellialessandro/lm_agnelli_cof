package client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Bonus;
import model.BusinessTile;
import model.Configuration;
import model.ConnectionProblemException;
import model.Constant;
import model.CouncilBalcony;
import model.Councillor;
import model.Emporium;
import model.King;
import model.NobilityTrack;
import model.Player;
import model.Region;
import model.Supply;
import model.Town;
import server.Match;

/**
 * Classe che estende l'interfaccia Connection per gestire i metodi di gioco con
 * una connessione Socket
 * 
 *
 */
public class ClientSocket implements Connection {
	private static final Logger log = Logger.getLogger(ClientCli.class.getName());

	private String nomoreafk = "nomoreafk ";

	private String serverip = System.getProperty("localhost");
	private int port = Constant.SOCKETPORT;
	private Socket socket;
	private Scanner socketIn;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private PrintWriter socketOut;
	private Match m;
	private Configuration c;
	private int idmatch;
	private String status = Constant.NOPLAYING;

	private String result;
	private boolean resultsetted = false;

	private boolean needsconfig = false;
	private boolean statuschanged = false;
	private boolean end = false;

	protected ClientSocket() throws IOException {
		socket = new Socket(this.serverip, port);
		socketIn = new Scanner(new BufferedReader(new InputStreamReader(socket.getInputStream())));
		socketOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
		ois = new ObjectInputStream(socket.getInputStream());
		oos = new ObjectOutputStream(socket.getOutputStream());
		SocketClientListener scl = new SocketClientListener(this, socketIn, oos, ois);
		Thread th = new Thread(scl);
		th.start();
	}

	protected void setMatch(Match m) {
		this.m = m;
		this.setStatusChanged(true);
	}

	private String waitForResult() {
		int i = 0;
		while (!this.isResultsetted()) {
			if (i > 30000) {
				log.log(Level.SEVERE, Constant.SERVERPROBLEMTIME);
				return Constant.SERVERPROBLEMTIME;
			} else {
				i++;
			}
			sleep();
		}
		return Constant.SUCCESS;
	}

	private void sleep() {
		try {
			Thread.sleep(1);
		} catch (Exception e) {
			log.log(Level.SEVERE, "Couldn't wait for a respons", e);
			sleep();
		}
	}

	private String giveMeResult() {
		String outcome = waitForResult();
		if (outcome.equals(Constant.SUCCESS)) {
			this.setResultsetted(false);
			return this.getResult();
		} else {
			return outcome;
		}
	}

	@Override
	public String electCouncillorWithEarn(String myplayername, int scelta, String regione)
			throws ConnectionProblemException {
		try {
			socketOut.print(
					"electcouncillorwithearn " + idmatch + " " + myplayername + " " + scelta + " " + regione + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String acquireBusinessTile(String myplayername, String regione, int theonetobuy, int[] index)
			throws ConnectionProblemException {
		try {
			socketOut.print("acquirebusinesstile " + idmatch + " " + myplayername + " " + regione + " " + theonetobuy
					+ " " + index.length);
			for (int i = 0; i < index.length; i++) {
				socketOut.print(" " + index[i]);
			}
			socketOut.print(" ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String buildEmporiumWithTile(String myplayername, int l, String town) throws ConnectionProblemException {
		try {
			socketOut.print("buildemporiumwithtile " + idmatch + " " + myplayername + " " + l + " " + town + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String buildEmporiumWithKing(String myplayername, String town, int[] ind) throws ConnectionProblemException {
		try {
			socketOut.print("buildemporiumwithking " + idmatch + " " + myplayername + " " + town + " " + ind.length);
			for (int i = 0; i < ind.length; i++) {
				socketOut.print(" " + ind[i]);
			}
			socketOut.print(" ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String endTurn(String myplayername) throws ConnectionProblemException {
		try {
			socketOut.print("endturn " + idmatch + " " + myplayername + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String engageAssistant(String myplayername) throws ConnectionProblemException {
		try {
			socketOut.print("engageassistant " + idmatch + " " + myplayername + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String changeBusinessTiles(String myplayername, String region) throws ConnectionProblemException {
		try {
			socketOut.print("changebusinesstiles " + idmatch + " " + myplayername + " " + region + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String electCouncillorPaying(String myplayername, int scelta, String regione)
			throws ConnectionProblemException {
		try {
			socketOut.print(
					"electcouncillorpaying " + idmatch + " " + myplayername + " " + scelta + " " + regione + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String additionalMainAction(String myplayername) throws ConnectionProblemException {
		try {
			socketOut.print("additionalmainaction " + idmatch + " " + myplayername + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String createAccount(String username, String password) throws ConnectionProblemException {
		try {
			socketOut.print("create " + username + " " + password + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String login(String username, String password) throws ConnectionProblemException {
		try {
			socketOut.print("login " + username + " " + password + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String joinGame(String myplayername) throws ConnectionProblemException {
		try {
			socketOut.print("joingame " + myplayername + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String setConfig(Configuration c2) throws ConnectionProblemException {
		try {
			oos.writeUnshared(c2);
			oos.flush();
			return giveMeResult();
		} catch (NoSuchElementException | IOException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public void createSupply(String nameplayer, int assistants, int[] businesstile, int[] politiccard, int price)
			throws ConnectionProblemException {
		try {
			socketOut.print("createsupply " + idmatch + " " + assistants);
			for (int i = 0; i < businesstile.length; i++) {
				socketOut.print(" " + businesstile[i]);
			}
			for (int i = 0; i < politiccard.length; i++) {
				socketOut.print(" " + politiccard[i]);
			}
			socketOut.print(" " + price + " ");
			socketOut.flush();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String buySupply(String nameplayer, int supplyindex) throws ConnectionProblemException {
		try {
			socketOut.print("buysupply " + idmatch + " " + nameplayer + " " + supplyindex + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public void reconnect(String nameplayer) throws ConnectionProblemException {
		try {
			socketOut.print("reconnecting " + idmatch + " " + nameplayer + " ");
			socketOut.flush();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Configuration getConfig() {
		return this.c;
	}

	@Override
	public boolean isNeedsconfig() {
		return this.needsconfig;
	}

	@Override
	public boolean isStatusChanged() {
		return this.statuschanged;
	}

	@Override
	public boolean isEnd() {
		return this.end;
	}

	@Override
	public void setEnd(boolean b) {
		this.end = b;
	}

	@Override
	public void setStatusChanged(boolean b) {
		this.statuschanged = b;
	}

	@Override
	public String getStatus() {
		return this.status;
	}

	@Override
	public String whoIsNow(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.whoIsNow();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public King getKing(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getKing();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public NobilityTrack getNobilityTrack(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getNobilityTrack();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<Player> getPlayers(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getPlayers();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Region getRegion(String me, String r) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getRegion(r);
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<Emporium> getEmporiums(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getEmporiums();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<String> getPlayersName(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getPlayersName();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<BusinessTile> getPlayersTile(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getPlayersTile();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Town[] getTowns(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getTowns();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public CouncilBalcony getCouncillorBalcony(String me, String r) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getRegion(r).getCouncBalcony();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public List<Councillor> getCouncillorPoolList(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getCouncillorPoolList();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusGold(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getBonusGoldTown();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusSilver(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getBonusSilverTown();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusBronze(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getBonusBronzeTown();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public Bonus getBonusIron(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.getBonusIronTown();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public String whoIsTheWinner(String me) throws ConnectionProblemException {
		return m.whoIsTheWinner();
	}

	@Override
	public List<Supply> getSupplies(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.showSupplies();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public boolean isBuyingPhase(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.isBuyingPhase();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	@Override
	public boolean isSellingPhase(String me) throws ConnectionProblemException {
		try {
			socketOut.print(nomoreafk + idmatch + " " + me + " ");
			socketOut.flush();
			return m.isSellingPhase();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	protected void setNeedsconfig(boolean needsconfig) {
		this.needsconfig = needsconfig;
	}

	private String getResult() {
		return result;
	}

	protected void setResult(String result) {
		this.result = result;
	}

	private boolean isResultsetted() {
		return resultsetted;
	}

	protected void setResultsetted(boolean resultsetted) {
		this.resultsetted = resultsetted;
	}

	protected int getIdmatch() {
		return idmatch;
	}

	protected void setIdmatch(int idmatch) {
		this.idmatch = idmatch;
	}

	protected void setConfigFinal(Configuration c) {
		this.c = c;
	}

	protected void setStatus(String status) {
		this.status = status;
	}

	protected void goDown() {
		socketIn.close();
		socketOut.close();
		try {
			ois.close();
			oos.close();
		} catch (IOException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
	}

	@Override
	public String disconnect(String myplayername) throws ConnectionProblemException {
		try {
			socketOut.print("exit " + myplayername + " ");
			socketOut.flush();
			return giveMeResult();
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

	public Socket giveMeSocket() {
		return this.socket;
	}

	@Override
	public boolean giveMeTavolino() throws ConnectionProblemException {
		try {
			socketOut.print("tavolino ");
			socketOut.flush();
			return "true".equals(giveMeResult());
		} catch (NoSuchElementException e) {
			log.log(Level.SEVERE, Constant.CONNECTIONPROBLEM, e);
			throw new ConnectionProblemException(Constant.CONNECTIONPROBLEM);
		}
	}

}
