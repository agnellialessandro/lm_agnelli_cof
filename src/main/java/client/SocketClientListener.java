package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Configuration;
import model.Constant;
import server.Match;
/**
 * 
 * Classe ausiliaria per la gestione dei socket, rimarrà in ascolto dei messaggi dal server
 *
 */
public class SocketClientListener implements Runnable {
	private static final Logger log = Logger.getLogger(ClientCli.class.getName());

	private ClientSocket cs;
	private Scanner socketIn;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	boolean end = false;

	SocketClientListener(ClientSocket cs, Scanner socketIn, ObjectOutputStream oos, ObjectInputStream ois)
			throws IOException {
		this.cs = cs;
		this.socketIn = socketIn;
		this.oos = oos;
		this.ois = ois;
	}

	@Override
	public void run() {
		String what;
		while (!end) {
			what = socketIn.nextLine();
			switch (what) {
			case Constant.GIVECONFIG:
				this.cs.setNeedsconfig(true);
				this.cs.setStatusChanged(true);
				break;
			case "end":
				this.cs.setEnd(true);
				break;
			case "updateconfig":
				updateConfig();
				break;
			case "updatematch":
				updateMatch();
				break;
			case "shutdown":
				this.cs.goDown();
				end = true;
				this.cs.setResult(Constant.SUCCESS);
				this.cs.setResultsetted(true);
				break;
			default:
				this.cs.setResult(what);
				this.cs.setResultsetted(true);
				break;
			}
		}
	}

	private void updateMatch() {
		try {
			Match m = (Match) ois.readUnshared();		
			
			
			oos.reset();	
	
			if (m != null) {
				this.cs.setMatch(m);
			}
		} catch (ClassNotFoundException | IOException e) {
			log.log(Level.SEVERE, "Error updating match", e);
		}
		this.cs.setStatus(Constant.STARTED);
		this.cs.setStatusChanged(true);
	}

	private void updateConfig() {
		try {

			Configuration c;

			
			c = (Configuration) ois.readObject();
			
			this.cs.setConfigFinal(c);
		} catch (ClassNotFoundException | IOException e) {
			log.log(Level.SEVERE, "Error updating match stats", e);
		}
		this.cs.setIdmatch(socketIn.nextInt());
		this.cs.setStatus(Constant.WAITINGFORPLAYERS);
		this.cs.setStatusChanged(true);
	}

}
