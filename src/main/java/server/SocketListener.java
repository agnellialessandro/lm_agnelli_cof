package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.ClientCli;
import model.Configuration;
import model.Constant;

public class SocketListener implements Runnable {
	private static final Logger log = Logger.getLogger(ClientCli.class.getName());
	private boolean stop = false;
	private boolean connected = false;
	private Socket sock;
	private Scanner inSocket;
	private PrintWriter outSocket;
	private Server server;
	private String playername;
	private boolean configurator = false;
	ObjectOutputStream outToClient;
	ObjectInputStream inFromClient;

	SocketListener(Server server, Socket sock) throws IOException {
		this.sock = sock;
		this.server = server;
		this.outToClient = new ObjectOutputStream(sock.getOutputStream());
		this.inFromClient = new ObjectInputStream(sock.getInputStream());
	}

	@Override
	public void run() {
		try {
			this.inSocket = new Scanner(new BufferedReader(new InputStreamReader(sock.getInputStream())));
			this.outSocket = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sock.getOutputStream())), true);
		} catch (IOException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
		connectionPhaseOne();
		while (connected) {
			connectionPhaseTwo();
			while (!stop && connected) {
				gamePhaseOne();
			}
		}

	}

	private void connectionPhaseOne() {
		String message;
		boolean cont = false;
		while (!cont) {
			message = inSocket.next();
			switch (message) {
			case "login":
				cont = this.login();
				break;
			case "create":
				cont = this.createAccount();
				break;
			default:
				cont = false;
				break;

			}
		}
		server.addPlayer(this.getName());
		server.addSocketListener(this);
		this.connected = true;

	}

	public boolean login() {
		String result;
		String clientname;
		String password;
		boolean cont = false;

		clientname = inSocket.next();
		password = inSocket.next();
		result = server.login(clientname, password);
		outSocket.println(result);
		outSocket.flush();
		if (result.equals(Constant.CONNECTSUCCESS)) {
			this.playername = clientname;
			cont = true;
		}
		return cont;
	}

	public boolean createAccount() {
		String result;
		String clientname;
		String password;
		boolean cont = false;
		clientname = inSocket.next();
		password = inSocket.next();
		result = server.createAccount(clientname, password);
		outSocket.println(result);
		outSocket.flush();
		if (result.equals(Constant.SUCCESS)) {
			this.playername = clientname;
			cont = true;
		}
		return cont;
	}

	private void connectionPhaseTwo() {
		String message;
		boolean cont = false;
		while (!cont) {
			message = inSocket.next();
			switch (message) {
			case "joingame":
				cont = joinMatch();
				break;
			case "options":

				break;
			case "exit":
				exit();
				break;
			default:
				break;

			}
		}
	}

	public boolean joinMatch() {
		String message = inSocket.next();
		if (message.equals(this.playername)) {
			String result = server.joinGame(this.playername);
			this.outSocket.println(result);
			if (this.configurator) {
				this.receiveConfig();
			}
			return true;
		}
		this.outSocket.println(Constant.CHOICENOTALLOWED);
		this.outSocket.flush();
		return false;
	}

	public void giveMeConfig() {
		this.configurator = true;
		outSocket.println(Constant.GIVECONFIG);
		outSocket.flush();

	}

	public void receiveConfig() {
		try {
			Configuration config = (Configuration) this.inFromClient.readObject();
			server.setConfig(config);
		} catch (ClassNotFoundException | IOException e) {
			server.setConfig(new Configuration());
			log.log(Level.SEVERE, e.toString(), e);
		}
	}

	public void updateConfig(Configuration c, int matchid) throws IOException {
		this.outSocket.println("updateconfig");
		this.outToClient.writeUnshared(c);
		this.outToClient.flush();
		this.outSocket.println(matchid);
		this.outSocket.println(Constant.SUCCESS);
	}

	public void updateMatch(Match m) throws IOException {
		this.outSocket.println("updatematch");		
		this.outToClient.reset();
		this.outToClient.writeUnshared(m);
		this.outToClient.flush();
	}

	private void exit() {
		if (this.inSocket.next().equals(playername)) {
			server.disconnectSocketPlayer(this);
		}
		this.outSocket.println("shutdown");
		this.connected = false;
	}

	public void gamePhaseOne() {
		String message = this.inSocket.next();
		switch (message) {

		case "aquirebusinesstile":
			acquireBusinessTile();
			break;
		case "buildemporiumwithtile":
			buildEmporiumWithTile();
			break;
		case "buildemporiumwithking":
			buildEmporiumWithTile();
			break;
		case "electcouncillorwithearn":
			electCouncillorWithEarn();
			break;
		case "endturn":
			endTurn();
			break;
		case "engageassistant":
			engageAssistant();
			break;
		case "changebusinesstile":
			changeBusinessTile();
			break;
		case "electcouncillorpaying":
			electCouncillorPaying();
			break;
		case "additionalmainaction":
			additionalMainAction();
			break;
		default:
			break;
		}

	}

	public void acquireBusinessTile() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		String regione = this.inSocket.next();
		int theonetobuy = this.inSocket.nextInt();
		int sizeofarray = this.inSocket.nextInt();
		int[] discardedcards = new int[sizeofarray];
		for (int i = 0; i < sizeofarray; i++) {
			discardedcards[i] = this.inSocket.nextInt();
		}
		this.outSocket.println(server.getMatches().get(idmatch).acquireBusinessTileCall(myplayername, regione,
				theonetobuy, discardedcards));

	}

	public void buildEmporiumWithTile() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		int businesstile = this.inSocket.nextInt();
		String town = this.inSocket.next();
		this.outSocket
				.println(server.getMatches().get(idmatch).buildEmporiumWithTileCall(myplayername, town, businesstile));
	}

	public void buildEmporiumwWithKing() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		String town = this.inSocket.next();
		int sizeofarray = this.inSocket.nextInt();
		int[] discardedcards = new int[sizeofarray];
		for (int i = 0; i < sizeofarray; i++) {
			discardedcards[i] = this.inSocket.nextInt();
		}
		this.outSocket.println(
				server.getMatches().get(idmatch).buildEmporiumWithKingCall(myplayername, town, discardedcards));
	}

	public void electCouncillorWithEarn() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		int scelta = this.inSocket.nextInt();
		String regione = this.inSocket.next();
		this.outSocket
				.println(server.getMatches().get(idmatch).electCouncillorWithEarnCall(myplayername, scelta, regione));
	}

	public void engageAssistant() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		this.outSocket.println(server.getMatches().get(idmatch).engageAssistantCall(myplayername));
	}

	public void changeBusinessTile() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		String regione = this.inSocket.next();
		this.outSocket.println(server.getMatches().get(idmatch).changeBusinessTileCall(myplayername, regione));
	}

	public void electCouncillorPaying() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		int scelta = this.inSocket.nextInt();
		String regione = this.inSocket.next();
		this.outSocket.println(
				server.getMatches().get(idmatch).electCouncillorWithAssistantCall(myplayername, scelta, regione));
	}

	public void additionalMainAction() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		this.outSocket.println(server.getMatches().get(idmatch).additionalMainActionCall(myplayername));
	}

	public void endTurn() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		this.outSocket.println(server.getMatches().get(idmatch).nextTurnCall(myplayername));
	}

	public void noMoreAfk() {
		int idmatch = this.inSocket.nextInt();
		String myplayername = this.inSocket.next();
		server.getMatches().get(idmatch).noMoreAfkCall(myplayername);
	}

	public void setEnd() {
		this.stop = true;
		this.outSocket.println("end");
	}

	public String getName() {
		return this.playername;
	}

}
