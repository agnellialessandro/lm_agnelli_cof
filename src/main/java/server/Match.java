package server;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import client.MyTimer;
import model.Bonus;
import model.BusinessTile;
import model.Configuration;
import model.Constant;
import model.CouncilBalcony;
import model.Councillor;
import model.CouncillorPool;
import model.Deck;
import model.Emporium;
import model.GameActions;
import model.IllegalActionException;
import model.King;
import model.Market;
import model.NobilityTrack;
import model.Player;
import model.PoliticCard;
import model.Region;
import model.RemoteClient;
import model.RemoteServer;
import model.Street;
import model.Supply;
import model.Town;
import model.VictoryTrack;
import model.Way;

/**
 * Classe che gestisce ogni partita, è una classe remota che verrà messa online
 * tramite RMI
 *
 */
public class Match extends UnicastRemoteObject implements GameActions, Serializable {

	private static final Logger log = Logger.getLogger(Match.class.getName());

	private transient MyTimer timer;

	private transient List<MyTimer> offlinetimers;
	private transient List<String> offliners;

	private transient RemoteServer serverstub;
	private transient Server server;

	private Region coast;
	private Region hills;
	private Region mountains;
	private Town[] towns;
	private List<Street> streets;
	private List<Player> players;
	private List<Player> temporaryplayers;
	private King king;
	private CouncillorPool cp;
	private VictoryTrack vt;
	private NobilityTrack nt;
	private Configuration config;
	private Deck deck;
	private List<Emporium> emporiums;
	private transient Market market;

	private Player theonewhomakeactions;
	private boolean mainactionready;
	private boolean quickactionready;
	private List<BusinessTile> tilesbought;

	private boolean started = false;
	private Player theonewhoended = null;
	private boolean hasnonext = false;
	private boolean dontdoanythingelse = false;
	private boolean tavolino = false;

	/* bonus */
	private Bonus bonusgold;
	private Bonus bonussilver;
	private Bonus bonusbronze;
	private Bonus bonusiron;
	private int kingbonus = 0;

	private int bonusgoldnumber = 0;
	private int bonussilvernumber = 0;
	private int bonusbronzenumber = 0;
	private int bonusironnumber = 0;

	/* market variables */
	private int marketcounter = 0;
	private boolean marketbuyingphase = false;
	private boolean marketsellingphase = false;

	/**
	 * Costruttore, inizializza tutte le variabili del gioco. Inizializzando
	 * Match non inizia la partita. La partita avrà inizio dopo un tempo
	 * specificato da un timer
	 * 
	 * @param c
	 *            Configurazione scelta dal primo giocatore che entra nella
	 *            partita
	 * @param server
	 *            indirizzo del server, serve per poter ottenere informazioni
	 *            sui giocatori connessi
	 * @throws IOException
	 *             se ci sono problemi con la lettura dei files per le città e
	 *             mappa
	 */
	public Match(Configuration c, Server server) throws IOException {
		this.server = server;
		init(c);
	}

	private synchronized void init(Configuration c) throws IOException {
		try {
			Registry registry = LocateRegistry.getRegistry(Constant.PORT);
			serverstub = (RemoteServer) registry.lookup("Server");
		} catch (NotBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
		this.tilesbought = new ArrayList<>();
		this.config = c;
		this.deck = new Deck(this.config);
		this.players = new ArrayList<>();
		this.offliners = new ArrayList<>();
		this.offlinetimers = new ArrayList<>();
		this.cp = new CouncillorPool();
		CouncilBalcony cbtemp = createBalcony(cp);
		this.coast = new Region(cbtemp, this.config.getMaxBonus());
		cbtemp = createBalcony(cp);
		this.hills = new Region(cbtemp, this.config.getMaxBonus());
		cbtemp = createBalcony(cp);
		this.mountains = new Region(cbtemp, this.config.getMaxBonus());
		loadMap();
		/* set town list in */
		this.coast.setTownList(this.towns);
		this.hills.setTownList(this.towns);
		this.mountains.setTownList(this.towns);
		/* generate business tiles */
		generateRegionTiles();
		cbtemp = createBalcony(cp);
		this.king = new King(this.towns[9], cbtemp);
		vt = new VictoryTrack(this.config.getVictoryLength());
		List<Integer> arraybonuspos = loadnobility();
		nt = new NobilityTrack(this.config.getNobilityLength(), arraybonuspos, this.config.getMaxBonus());
		this.emporiums = new ArrayList<>();
		/*
		 * set special bonus
		 */
		this.bonusgold = new Bonus(Constant.REGIONBONUS, this.config.getMaxBonus());
		this.bonussilver = new Bonus(Constant.REGIONBONUS, this.config.getMaxBonus());
		this.bonusbronze = new Bonus(Constant.REGIONBONUS, this.config.getMaxBonus());
		this.bonusiron = new Bonus(Constant.REGIONBONUS, this.config.getMaxBonus());
		countNumberBonus();
	}

	private synchronized void loadMap() throws IOException {
		/* loading towns */
		loadTowns();
		/* loading streets */
		loadStreets();
	}

	/**
	 * Carica tutte le città che vengono prese da un file scelto all'inizio
	 * nella configurazione della partita
	 * 
	 * @throws IOException
	 *             se ci sono problemi con la lettura del file della mappa
	 */
	private synchronized void loadTowns() throws IOException {
		String temp;
		String temptown = null;
		String duplicate;
		Color truecolor = null;
		Region trueregion = null;
		Scanner in;
		boolean first = true;
		in = new Scanner(
				new BufferedReader(giveMeTheFileReader(Constant.MAPFOLDER + this.config.getMap() + "mapcity.txt")));
		towns = new Town[Constant.NUMBERCITIES];
		int i = 0;
		while (i < Constant.NUMBERCITIES * 3) {
			temp = in.next();
			duplicate = temp;
			duplicate.toLowerCase();
			switch (whatIsIt(duplicate)) {
			case "color":
				truecolor = giveMeTheColorTown(duplicate);
				break;
			case "region":
				trueregion = giveMeTheRegion(duplicate);
				break;
			case "town":
				if (!first)
					i--;
				temptown = new String(temptown + " " + temp);
				first = false;
				break;
			default:
				break;
			}
			boolean check = checkWriteTown(i, temptown, truecolor, trueregion);
			if (check) {
				first = true;
				temptown = null;
			}
			i++;
		} /* end while */
		in.close();
	}

	private String whatIsIt(String duplicate) {
		if ("gold".equals(duplicate) || "silver".equals(duplicate) || "bronze".equals(duplicate)
				|| "iron".equals(duplicate)) {
			return "color";
		}
		if (duplicate.equals(Constant.COASTNAME) || duplicate.equals(Constant.HILLSNAME)
				|| duplicate.equals(Constant.MOUNTAINSNAME)) {
			return "region";
		}
		return "town";
	}

	private Region giveMeTheRegion(String duplicate) {
		switch (duplicate) {
		case Constant.COASTNAME:
			return this.coast;
		case Constant.HILLSNAME:
			return this.hills;
		case Constant.MOUNTAINSNAME:
			return this.mountains;
		default:
			return null;
		}
	}

	private Color giveMeTheColorTown(String duplicate) {
		switch (duplicate) {
		case "gold":
			return Constant.GOLD;
		case "silver":
			return Constant.SILVER;
		case "bronze":
			return Constant.BRONZE;
		case "iron":
			return Constant.IRON;
		default:
			return null;
		}
	}

	private boolean checkWriteTown(int i, String temptown, Color truecolor, Region trueregion) {
		if ((i + 1) % 3 == 0 && temptown != null) {
			towns[((i + 1) / 3) - 1] = new Town(new String(temptown.substring(5)), truecolor, trueregion,
					this.config.getMaxBonus());
			return true;
		}
		return false;
	}

	private Reader giveMeTheFileReader(String s) throws IOException {
		try {
			return new FileReader(s);
		} catch (IOException e) {
			throw new IOException(e + " error loading map towns file");
		} /* end try-catch */
	}

	/**
	 * Crea tutte le strade di collegamento tra ogni città
	 * 
	 * @throws IOException
	 *             se ci sono problemi con il file della mappa
	 */
	private synchronized void loadStreets() throws IOException {
		int temptownloaded;
		Town[] temptwn = new Town[2];
		Scanner in = null;
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + this.config.getMap() + "mapstreet.txt")) {
			in = new Scanner(new BufferedReader(fr));
			this.streets = new ArrayList<>();
			while (in.hasNextInt()) {
				for (int j = 0; j < 2; j++) {
					temptownloaded = in.nextInt();
					temptwn[j] = this.towns[temptownloaded - 1];
				}
				this.streets.add(new Street(temptwn[0], temptwn[1]));
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loading map streets file");
		} /* end try-catch */
	}

	/**
	 * genera due Business Tile di partenza per ogni regione
	 */
	private synchronized void generateRegionTiles() {
		this.coast.addTiles(new BusinessTile(this.towns, this.config.getMaxBonus()));
		this.coast.addTiles(new BusinessTile(this.towns, this.config.getMaxBonus()));
		this.hills.addTiles(new BusinessTile(this.towns, this.config.getMaxBonus()));
		this.hills.addTiles(new BusinessTile(this.towns, this.config.getMaxBonus()));
		this.mountains.addTiles(new BusinessTile(this.towns, this.config.getMaxBonus()));
		this.mountains.addTiles(new BusinessTile(this.towns, this.config.getMaxBonus()));
	}

	/**
	 * Inizializza le posizioni dei bonus nella nobility track
	 * 
	 * @return Le posizioni dei bonus nella nobility track
	 * @throws IOException
	 *             se ci sono problemi con il file della nobility track
	 */
	private synchronized List<Integer> loadnobility() throws IOException {
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + this.config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		} /* end try-catch */
		return arraypos;
	}

	private void countNumberBonus() {
		for (Town t : this.towns) {
			if (t.getType().equals(Constant.GOLD)) {
				this.bonusgoldnumber++;
			}
			if (t.getType().equals(Constant.SILVER)) {
				this.bonussilvernumber++;
			}
			if (t.getType().equals(Constant.BRONZE)) {
				this.bonusbronzenumber++;
			}
			if (t.getType().equals(Constant.IRON)) {
				this.bonusironnumber++;
			}
		}
	}

	/**
	 * Inizializza tutti i Council Balcony.
	 * 
	 * @param cp
	 *            Classe Councillor Pool che contiene inizialmente tutti i
	 *            consiglieri del gioco, verranno presi e rimossi da qui
	 * @return La Council Balcony inizializzata
	 */
	private synchronized CouncilBalcony createBalcony(CouncillorPool cp) {
		CouncilBalcony tempcb;
		Councillor[] tempcounc = new Councillor[4];
		int cplength = cp.getWholePool().size();
		int temprndm;
		Councillor tempclr;
		Random random = new Random();
		/*
		 * generate 4 random number between 0-length max of the pool, extract
		 * the ones with this index
		 */
		for (int i = 0; i < 4; i++) {
			temprndm = random.nextInt(cplength - 1);
			tempclr = cp.getCouncillor(temprndm);
			tempcounc[i] = tempclr;
			cplength--;
		}
		/*
		 * put the 4 councillor extracted in a new balcony
		 */
		tempcb = new CouncilBalcony(tempcounc);
		return tempcb;
	}

	/**
	 * Metodo che gestisce la mossa per eleggere un consigliere
	 * 
	 * @param c
	 *            Consigliere da aggiungere
	 * @param region
	 *            Regione dove si trova la Council Balcony in cui si vuole
	 *            eleggere un consigliere
	 */
	private synchronized void electCouncillor(Councillor c, String region) {
		this.cp.removeCouncillor(c);
		CouncilBalcony bal = this.getBalcony(region);
		Councillor tothepool = bal.addCouncillor(c);
		this.cp.addCouncillor(tothepool);
	}

	/**
	 * Deve essere chiamata quando un Match inizia, sceglie randomicamente il
	 * giocatore che inizia per primo
	 */
	public synchronized void start() {
		int lengthplayer = this.players.size();
		Random random = new Random();
		int starter = random.nextInt(lengthplayer);
		this.theonewhomakeactions = this.players.get(starter);
		this.theonewhomakeactions.drawCard();
		this.mainactionready = true;
		this.quickactionready = true;
		setPlayerBaseStats(starter);
		this.started = true;
		this.marketcounter = this.players.size();
		startTimerTurn();
	}

	/**
	 * Timer che gestisce ogni turno dei giocatori, se il timer scade e il
	 * giocatore non ha compiuto azioni allora il giocatore verrà flaggato afk.
	 * Se il giocatore finisce il turno prima dello scadere del timer, esso
	 * verrà cancellato e reiniziallizzato per l'inizio del prossimo turno.
	 */
	private void startTimerTurn() {
		timer = new MyTimer();
		timer.schedule(() -> {
			if (hasNextTurn()) {
				/*
				 * set afk status if player didn't make any action
				 */
				if (mainactionready && quickactionready) {
					theonewhomakeactions.setStatus(Constant.AFK);
				}
				nextTurn();
				updateAllClientsMatch();
			} else {
				endOfTheGame();
				timer.cancel();
			}
		}, this.getConfig().getTimeRound() * 1000L);
	}

	/**
	 * Timer che gestisce se impostare un giocatore offline(client che ha perso
	 * la connessione), e quindi metterlo in una lista di offliners apposita per
	 * saltare i suoi turni. Il giocatore ha la possibilità di riconnettersi
	 * entro la fine del timer altrimenti verrà considerato offline e non potrà
	 * più riconnettersi. Verrà comunque considerato nel conteggio finale.
	 * 
	 * @param offliner
	 *            Giocatore non più connesso
	 */
	private void startOfflinerFoundTimer(Player offliner) {
		timer = new MyTimer();
		this.offlinetimers.add(timer);
		timer.schedule(() -> disconnectHandle(offliner), 30000);
		return;
	}

	private void disconnectHandle(Player offliner) {
		try {
			serverstub.remotePlayerDisconnected(offliner.getName());
			offliner.setStatus(Constant.DISCONNECTED);
			boolean end = false;
			for (int i = 0; i < offliners.size() && !end; i++) {
				if (offliners.get(i).equals(offliner.getName())) {
					offliners.remove(i);
					end = true;
				}
			}
		} catch (RemoteException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
		for (int i = 0; i < this.players.size(); i++) {
			try {
				this.getClientStub(this.getPlayers().get(i).getName()).playerDisconnect(offliner.getName());
			} catch (Exception e) {
				log.log(Level.FINE, e.toString(), e);
			}
		}
	}

	/**
	 * Inizializza le variabili assistants e coins di ogni giocatore, tenendo
	 * conto del suo ordine.
	 * 
	 * @param starter
	 */
	private synchronized void setPlayerBaseStats(int starter) {
		int rich = 10;
		int ass = 1;
		for (int i = starter; i < this.players.size(); i++) {
			this.players.get(i).moveCoins(rich);
			this.players.get(i).addAssistant(ass);
			rich++;
			ass++;
		}
		for (int i = 0; i < starter; i++) {
			this.players.get(i).moveCoins(rich);
			this.players.get(i).addAssistant(ass);
			rich++;
			ass++;
		}
	}

	/*
	 * add new player to the match should only be called before the match starts
	 */
	/**
	 * Aggiunge un nuovo giocatore al match, deve essere chiamato solo prima che
	 * il match inizi. Se viene aggiunto il secondo giocatore verrà gestito in
	 * seguito il timer per l'inizio della partita
	 * 
	 * @param nome
	 *            Nome del giocatore
	 * @return True se è il secondo giocatore, altrimenti false.
	 * @throws IllegalActionException
	 *             Se il match è già iniziato
	 */
	public synchronized boolean addPlayer(String nome) throws IllegalActionException {
		if (!this.started) {
			Player p = new Player(nome, this.deck, this.nt);
			this.players.add(p);
		} else {
			throw new IllegalActionException(Constant.MATCHISSTARTED);
		}
		return this.players.size() > 1 && this.players.get(1).getName().equals(nome);
	}

	/**
	 * 
	 * @return Il giocatore che deve compiere una mossa
	 */
	public synchronized String whoIsNow() {
		return this.theonewhomakeactions.getName();
	}

	/**
	 * 
	 * @return Se è finito il gioco
	 */
	public synchronized boolean hasNextTurn() {
		return !this.hasnonext;
	}

	/*
	 * it changes the current player whit the next one
	 */
	/**
	 * Cambio del turno. Il metodo gestisce anche i turni di market chiamando
	 * checkMarketPhases() e controlla se il match sta per finire, in questo
	 * caso farà fare l'ultimo turno di ogni giocatore.
	 */
	public synchronized void nextTurn() {
		timer.cancel();
		if (this.theonewhoended == null) {
			checkMarketPhases();
		}
		Player next = this.theonewhomakeactions;
		boolean changed = false;
		int i;
		for (i = 0; i < this.players.size() && !changed; i++) {
			/*
			 * if the player is the one that was making action, set the next one
			 */
			if (this.players.get(i) == this.theonewhomakeactions) {
				if (i >= this.players.size() - 1) {
					i = 0;
					next = this.players.get(i);
				} else {
					i += 1;
					next = this.players.get(i);
				}
				changed = true;
			}
		}
		this.theonewhomakeactions = next;
		/*
		 * reset action variables
		 */
		this.mainactionready = true;
		this.quickactionready = true;
		/*
		 * the last cycle of the for moved +1 the index i, so it doesn't need to
		 * be applied when looking for the next
		 */
		if (i == this.players.size()) {
			i = 0;
		}
		if (this.players.get(i) == this.theonewhoended) {
			this.hasnonext = true;
		}
		this.startTimerTurn();
		boolean check = checkStatusNewPlayer();
		if (check) {
			// non sono rimasti giocatori in partita
			this.tavolino = true;
			this.theonewhoended = next;
			this.dontdoanythingelse = true;
			this.endOfTheGame();
		} else {
			turnDraw();
		}
	}

	/**
	 * Gestisce il controllo dei giocatori afk e offline per vedere se bisogna
	 * saltare i loro turni.
	 */
	private boolean checkStatusNewPlayer() {
		for (String e : this.offliners) {
			if (e.equals(this.theonewhomakeactions.getName())) {
				this.nextTurn();
				return true;
			}
		}
		if (!this.theonewhomakeactions.getStatus().equals(Constant.AVAILABLE)) {
			this.nextTurn();
			return true;
		}
		return false;
	}

	/**
	 * Metodo che andrà a chiamare un metodo remoto sul client RMI per avvisarlo
	 * che il gioco è finito.
	 */
	private void endOfTheGame() {
		try {
			for (Player p : this.players) {
				if (server.isRmiOrSocket(p.getName()).equals(Constant.RMI)
						&& !p.getStatus().equals(Constant.DISCONNECTED) && !isItOffliner(p.getName())) {
					RemoteClient rctemp = this.getClientStub(p.getName());
					rctemp.endingMatch();
				} else if (server.isRmiOrSocket(p.getName()).equals(Constant.SOCKET)) {
					this.getClientSocket(p.getName()).setEnd();
				}
			}
		} catch (RemoteException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
	}

	private synchronized void turnDraw() {
		this.theonewhomakeactions.drawCard();
	}

	private boolean isItOffliner(String name) {
		for (String e : this.offliners) {
			if (e.equals(name)) {
				return true;
			}
		}
		return false;
	}

	public synchronized void setEnd() {
		this.dontdoanythingelse = true;
		// add all end bonus
		addPossibleVictoryPointsToTheBestInNobility();
		int maxnumberoftiles = players.get(0).getTilesowned();
		for (Player p : this.players) {
			if (p.getTilesowned() > maxnumberoftiles) {
				maxnumberoftiles = p.getTilesowned();
			}
		}
		for (Player p : players) {
			if (p.getTilesowned() == maxnumberoftiles) {
				p.moveVictory(3);
			}
		}
	}

	private void addPossibleVictoryPointsToTheBestInNobility() {
		this.players
				.sort((Player p1, Player p2) -> Integer.compare(p1.getNobilityPosition(), p2.getNobilityPosition()));
		int i;
		boolean goon = false;
		for (i = players.size(); i >= 0 && !goon; i--) {
			if (!(players.get(i).getNobilityPosition() == players.get(i - 1).getNobilityPosition())) {
				goon = true;
			}
		}
		i++;
		boolean checkseconds = false;
		if (i == players.size()) {
			players.get(i).moveNobility(5);
			checkseconds = true;
		} else {
			for (int j = players.size(); j >= i; j--) {
				players.get(j).moveNobility(5);
			}
		}
		if (checkseconds) {
			checkSecondPossibilityPoints();
		}
	}

	private void checkSecondPossibilityPoints() {
		int i;
		boolean goon = false;
		for (i = players.size() - 1; i >= 0 && !goon; i--) {
			if (!(players.get(i).getNobilityPosition() == players.get(i - 1).getNobilityPosition())) {
				goon = true;
			}
		}
		i++;
		if (i == players.size()) {
			players.get(i).moveNobility(2);
		} else {
			for (int j = players.size() - 1; j >= i; j--) {
				players.get(j).moveNobility(2);
			}
		}
	}

	public synchronized Configuration getConfig() {
		return this.config;
	}

	public synchronized CouncilBalcony getBalcony(String s) {
		/*
		 * serve per poter farlo visualizzare al client
		 */
		s.toLowerCase();
		switch (s) {
		case Constant.COASTNAME:
			return this.coast.getCouncBalcony();
		case Constant.HILLSNAME:
			return this.hills.getCouncBalcony();
		case Constant.MOUNTAINSNAME:
			return this.mountains.getCouncBalcony();
		case Constant.KINGNAME:
			return this.king.getBalcony();
		default:
			throw new IllegalArgumentException();
		}
	}

	public synchronized Region getRegion(String s) {
		/*
		 * serve per poter farlo visualizzare al client
		 */
		s.toLowerCase();
		switch (s) {
		case Constant.COASTNAME:
			return this.coast;
		case Constant.HILLSNAME:
			return this.hills;
		case Constant.MOUNTAINSNAME:
			return this.mountains;
		default:
			throw new IllegalArgumentException();
		}
	}

	public synchronized Town[] getTowns() {
		return this.towns;
	}

	public synchronized List<Street> getStreet() {
		return this.streets;
	}

	public synchronized List<BusinessTile> getPlayersTile() {
		return this.tilesbought;
	}

	public synchronized King getKing() {
		return this.king;
	}

	public synchronized NobilityTrack getNobilityTrack() {
		return this.nt;
	}

	public synchronized List<Councillor> getCouncillorPoolList() {
		return this.cp.getWholePool();
	}

	public synchronized List<String> getPlayersName() {
		List<String> tempplayers = new ArrayList<>();
		for (Player p : this.players) {
			tempplayers.add(p.getName());
		}
		return tempplayers;
	}

	public synchronized List<Player> getPlayers() {
		return this.players;
	}

	public synchronized List<Emporium> getEmporiums() {
		return this.emporiums;
	}

	public synchronized Bonus getBonusGoldTown() {
		return this.bonusgold;
	}

	public synchronized Bonus getBonusSilverTown() {
		return this.bonussilver;
	}

	public synchronized Bonus getBonusBronzeTown() {
		return this.bonusbronze;
	}

	public synchronized Bonus getBonusIronTown() {
		return this.bonusiron;
	}

	public synchronized boolean isStarted() {
		return this.started;
	}

	/**
	 * 
	 * @return Il vincitore del gioco
	 */
	public synchronized String whoIsTheWinner() {
		return this.vt.theWinner(this.players).getName();
	}

	/*
	 * Azioni principali
	 */

	/**
	 * Azione principale per eleggere un consigliere ricevendo monete.
	 * 
	 * @param nomeplayer
	 *            Nome del client-giocatore
	 * @param c
	 *            Consigliere da eleggere
	 * @param region
	 *            Regione della Council Balcony in cui eleggere un consigliere
	 * @throws IllegalActionException
	 *             Se non si ha un'azione principale disponibile
	 */
	public synchronized void electCouncillorWithEarn(String nomeplayer, Councillor c, String region)
			throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (this.mainactionready) {
			electCouncillor(c, region);
			this.theonewhomakeactions.moveCoins(Constant.BONUSELECTCOUNCILLOR);
			this.mainactionready = false;
		} else {
			throw new IllegalActionException(Constant.MAINACTIONDONE);
		}

	}

	/**
	 * Azione principale per comprare una BusinessTile
	 * 
	 * @param nomeplayer
	 *            Nome del Client-giocatore
	 * @param region
	 *            Regione in cui si trova la business tile
	 * @param pos
	 *            posizione della business tile da comprare tra le due
	 * @param discardedcards
	 *            Carte scartate per comprarla
	 * @throws IllegalActionException
	 *             Se non si ha un'azione principale disponibile
	 */
	public synchronized void acquireBusinessTile(String nomeplayer, String region, int pos,
			PoliticCard[] discardedcards) throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (this.mainactionready) {
			boolean sat;
			sat = satisfiedBalcony(region, discardedcards);
			if (sat) {
				this.tilesbought.add(0, getRegion(region).takeTile(pos));
				this.tilesbought.get(0).setPlayer(this.theonewhomakeactions);
				this.theonewhomakeactions.addBonus(this.tilesbought.get(0).getBonus());
				this.theonewhomakeactions.increaseTilesowned();
				this.mainactionready = false;
			}
		} else {
			throw new IllegalActionException(Constant.MAINACTIONDONE);
		}

	}

	/**
	 * Azione principale per costruire un emporium con una business tile
	 * 
	 * @param nomeplayer
	 *            Nome client-giocatore
	 * @param t
	 *            Città in cui costruire l'emporio
	 * @param used
	 *            Business Tile usata
	 * @throws IllegalActionException
	 *             Se non si ha un'azione principale disponibile
	 */
	public synchronized void buildEmporiumWithTile(String nomeplayer, Town t, BusinessTile used)
			throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (!this.mainactionready) {
			throw new IllegalActionException(Constant.MAINACTIONDONE);
		}
		checkNoEmporiumInThisTown(t);
		checkIfTownIsInTheTile(t, used);
		/*
		 * pay assistant for each player who already build an emporium in this
		 * town
		 */
		int assistantcost = payAssistants(t);
		try {
			this.theonewhomakeactions.addAssistant(-assistantcost);
		} catch (IllegalArgumentException e) {
			throw new IllegalActionException(e + Constant.NOTENOUGHASSISTANTS);
		}
		/*
		 * add emporium and remove tile
		 */
		this.emporiums.add(new Emporium(this.theonewhomakeactions, t));
		boolean f = false;
		for (int i = 0; i < this.tilesbought.size() && !f; i++) {
			if (this.tilesbought.get(i) == used) {
				this.tilesbought.remove(i);
				f = true;
			}
		}
		addBuildBonus(t);
		checkEnd();
		this.theonewhomakeactions.decreaseTilesowned();
		this.mainactionready = false;
	}

	private void checkIfTownIsInTheTile(Town t, BusinessTile used) throws IllegalActionException {
		/*
		 * check if town is in the tile discarded
		 */
		Boolean found = false;
		List<Town> possibility = used.getTown();
		for (Town tw : possibility) {
			if (tw == t) {
				found = true;
			}
		}
		/*
		 * throw exception if the check is not passed
		 */
		if (!found) {
			throw new IllegalActionException(Constant.CITYTILEMATCH);
		}
	}

	private void checkNoEmporiumInThisTown(Town t) throws IllegalActionException {
		/*
		 * check if he didn't build an emporium previously in the same town
		 */
		for (Emporium e : this.emporiums) {
			if (e.getOwner() == this.theonewhomakeactions && e.getTown() == t) {
				throw new IllegalActionException(Constant.EMPORIUMBUILDED);
			}
		}
	}

	/**
	 * Azione principale per costruire un emporio con l'aiuto del re.
	 * 
	 * @param nomeplayer
	 *            Nome client-giocatore
	 * @param t
	 *            città in cui costruire
	 * @param discardedcards
	 *            indice delle carte politiche da scartare
	 * @throws IllegalActionException
	 *             Se non si hanno abbastanza monete o assistenti per pagare
	 */
	public synchronized void buildEmporiumWithKing(String nomeplayer, Town t, PoliticCard[] discardedcards)
			throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (!this.mainactionready) {
			throw new IllegalActionException(Constant.MAINACTIONDONE);
		}
		/*
		 * calcolare percorso più corto alla città di arrivo e il costo
		 */
		int cost = giveMeTheCostTo(t);
		/*
		 * find the assistant cost for each other player's emporium built in
		 * this town
		 */
		int assistantcost = payAssistants(t);
		if (this.theonewhomakeactions.getAssistant() >= assistantcost
				&& this.theonewhomakeactions.getCoinsPosition() >= cost) {
			makeHimPayForKing(cost, assistantcost, discardedcards);
		} else {
			throw new IllegalActionException(Constant.NOTENOUGHCOINSORASS);
		}
		this.emporiums.add(new Emporium(this.theonewhomakeactions, t));
		addBuildBonus(t);
		this.king.move(t);
		checkEnd();
		this.mainactionready = false;
	}

	private void makeHimPayForKing(int cost, int assistantcost, PoliticCard[] discardedcards)
			throws IllegalActionException {
		/*
		 * if the king council can be satisfied, pay all
		 */
		try {
			this.theonewhomakeactions.moveCoins(-cost);
		} catch (IllegalArgumentException e) {
			log.log(Level.SEVERE, e.toString(), e);
			throw new IllegalActionException(Constant.NOTENOUGHCOINS);
		}
		boolean sat;
		try {
			sat = satisfiedBalcony("king", discardedcards);
		} catch (IllegalArgumentException e) {
			try {
				this.theonewhomakeactions.moveCoins(cost);
			} catch (IllegalArgumentException e2) {
				log.log(Level.SEVERE, e2.toString(), e2);
				throw new IllegalActionException(Constant.NOTENOUGHCOINS);
			}
			throw e;
		}
		if (sat) {
			this.theonewhomakeactions.addAssistant(-assistantcost);
		}
	}

	private int giveMeTheCostTo(Town t) {
		if (this.king.getTown() != t) {
			return shortestWay(t) * 2;
		} else {
			return 0;
		}
	}

	/**
	 * Controlla il numero di empori di ogni giocatore per vedere se il gioco è
	 * finito
	 */
	private synchronized void checkEnd() {
		int count = 0;
		for (Emporium e : this.emporiums) {
			if (e.getOwner() == this.theonewhomakeactions) {
				count++;
			}
		}
		if (count == 10) {
			this.theonewhomakeactions.moveVictory(3);
			this.theonewhoended = this.theonewhomakeactions;
		}
	}

	/**
	 * Metodo per controllare se e quanto si è soddisfatta una CouncilBalcony
	 * con le carte scartate
	 * 
	 * @param who
	 *            Nome client-giocatore
	 * @param discardedcards
	 *            array di carte scartate per soddisfare il consiglio
	 * @return true se l'azione si è conslusa bene
	 * @throws IllegalActionException
	 *             Se non si è soddisfatto il consiglio o non si hanno
	 *             abbastanza monete
	 */
	private synchronized boolean satisfiedBalcony(String who, PoliticCard[] discardedcards)
			throws IllegalActionException {
		CouncilBalcony cb = getBalcony(who);
		Councillor[] c = cb.showCouncillors();
		/* supp represent how many councilor have been satisfied */
		int[] supp = { 0, 0, 0, 0 };
		/*
		 * nonmulti represent how many councillor have been satisfied without
		 * multicard
		 */
		int[] nonmulti = { 0, 0, 0, 0 };
		int multitimer = 0;
		/*
		 * check how many of councillors have been satisfied
		 */
		for (int i = 0; i < discardedcards.length; i++) {
			boolean stop = false;
			for (int j = 0; j < c.length && !stop; j++) {
				if (discardedcards[i].getColor().equals(c[j].getColor()) && supp[j] == 0) {
					supp[j] = 1;
					nonmulti[j] = 1;
					stop = true;
				}
			}
		}
		/*
		 * check if multicolor cards were discarded
		 */
		for (PoliticCard p : discardedcards) {
			multitimer = checkIfMulti(multitimer, supp, p);
		}
		/*
		 * count how many are satisfied
		 */
		int sud = howManyAreSatisfied(supp);
		/*
		 * make the player pay if he didn't accomplished all the balcony
		 */
		makeHimPay(sud, multitimer);
		/*
		 * remove politic cards from the player's hand
		 */
		for (int i = 0; i < nonmulti.length; i++) {
			if (nonmulti[i] == 1) {
				this.theonewhomakeactions.discardCard(c[i].getColor());
			}
		}
		/* remove the multicolor cards used */
		for (int i = 0; i < multitimer; i++) {
			this.theonewhomakeactions.discardCard(Color.cyan);
		}
		return true;
	}

	private int howManyAreSatisfied(int[] supp) {
		/*
		 * count how many are satisfied
		 */
		int sud = 0;
		for (int i = 0; i < 4; i++) {
			sud += supp[i];
		}
		return sud;
	}

	private void makeHimPay(int sud, int multitimer) throws IllegalActionException {
		int cost;
		switch (sud) {
		case 0:
			throw new IllegalActionException("non soddisfatto");
		case 1:
			cost = -10 - multitimer;
			break;
		case 2:
			cost = -7 - multitimer;
			break;
		case 3:
			cost = -4 - multitimer;
			break;
		default:
			cost = 0 - multitimer;
			break;
		}
		try {
			this.theonewhomakeactions.moveCoins(cost);
		} catch (IllegalArgumentException e) {
			throw new IllegalActionException(e + Constant.NOTENOUGHCOINS);
		}
	}

	private int checkIfMulti(int multitimer, int[] supp, PoliticCard p) {
		int multi = multitimer;
		boolean exit = false;
		if (p.getColor().equals(Color.cyan)) {
			multi++;
			for (int i = 0; i < supp.length && !exit; i++) {
				if (supp[i] == 0) {
					supp[i] = 1;
					exit = true;
				}
			}
		}
		return multi;
	}

	/**
	 * Metodo per sapere quanto si paga per costruire un emporio in una città
	 * 
	 * @param t
	 *            Citta in cui costruire
	 * @return Quanto si deve pagare
	 */
	private synchronized int payAssistants(Town t) {
		int num = 0;
		for (Emporium e : this.emporiums) {
			if (e.getOwner() != this.theonewhomakeactions && e.getTown() == t) {
				num++;
			}
		}
		return num;
	}

	/**
	 * Metodo che calcola la strada più corta da far fare al re per spostarlo.
	 * 
	 * @param to
	 *            Città di arrivo.
	 * @return Costo per spostarlo.
	 */
	private synchronized int shortestWay(Town to) {
		Town from = this.king.getTown();
		List<Way> all = new ArrayList<>();
		/*
		 * find all the possible ways
		 */
		all.add(new Way(from));
		Way best = null;
		/* ripeto il ciclo fino a che anche l'ultimo percorso */
		boolean end = false;
		int cost;
		/*
		 * il costo massimo è pari al numero di città che è 15 il che lo rende
		 * minore dell'inizializzazione
		 */
		cost = 20;
		while (!end) {
			lookForAnotherWay(all);
			/*
			 * se un percorso è gia arrivato a destinazione calcolo il costo e
			 * cancello tutti i percorsi con costo superiore
			 */
			for (Way w : all) {
				if (w.last() == to && cost > w.cost() && w != best) {
					cost = w.cost();
					best = w;
				}
			}
			removeWayWithCoastHigher(all, cost, best);
			if (all.size() == 1 && all.get(0).last() == to) {
				end = true;
			}
		} /* end while */
		return cost;
	}/* end method */

	private void lookForAnotherWay(List<Way> all) {
		/*
		 * controllo che esista almeno un percorso con ancora delle possibilità
		 * per ogni percorso
		 */
		int lunghezza = all.size();
		int i = 0;
		while (i < lunghezza) {
			/*
			 * cerco nelle strade quelle che partono dall'ultima posizione
			 */
			Way newway = new Way();
			all.get(i).copy(newway);
			for (Street s : this.streets) {
				if (s.isItIn(all.get(i).last())) {
					/*
					 * creo una copia del percorso nel caso ci siano più strada,
					 * aggiungo il nuovo percorso, e aggiungo anche la nuova
					 * città del percorso
					 */
					Way temp = new Way();
					newway.copy(temp);
					temp.add(s.destination(all.get(i).last()));
					all.add(0, temp);
					i++;
				}
			}
			/*
			 * rimuovo l'ultimo percorso aggiunto poichè tutti i possibili
			 * percorsi sono stati aggiunti
			 */
			all.remove(i);
			/* aggiorno lunghezza */
			lunghezza = all.size();
			i++;// while increment
		} /* end while ways */
	}

	private void removeWayWithCoastHigher(List<Way> all, int cost, Way best) {
		int k = 0;
		while (k < all.size()) {
			/*
			 * rimuovo anche quelli incompleti che hanno costo=costo-1 perchè al
			 * massimo possono avere costo pari a quello gia trovato
			 */
			if (all.get(k).cost() >= cost - 1 && all.get(k) != best) {
				all.remove(k);
				/* need to adapt the index to the remove */
				k--;
			}
			k++;// while increment
		}
	}

	/**
	 * Metodo che viene usato dopo la costruzione di un emporio per calcolare
	 * gli eventuali bonus delle città collegate in cui si ha un emporio
	 * 
	 * @param t
	 *            Città in cui si è costruito l'emporio
	 */
	private synchronized void addBuildBonus(Town t) {
		List<Town> withemporium = new ArrayList<>();
		withemporium.add(t);
		Town tw;
		int length = withemporium.size();
		for (int i = 0; i < length; i++) {
			/*
			 * cerco tutte le strade collegare città che viene aggiunta nella
			 * lista
			 */
			tw = withemporium.get(i);
			for (Street s : this.streets) {
				if (s.isItIn(tw) && isEmporiumBuildThere(s.destination(tw))) {
					withemporium.add(s.destination(tw));
				}
			}
			/* eliminate possible copies of cities */
			removeCopies(withemporium);
			length = withemporium.size();
		}
		int assis = 0;
		int car = 0;
		int coi = 0;
		int nob = 0;
		int vic = 0;
		for (Town temp : withemporium) {
			assis += temp.getBonus().getAssistants();
			car += temp.getBonus().getCards();
			coi += temp.getBonus().getCoins();
			nob += temp.getBonus().getNobilityMoves();
			vic += temp.getBonus().getVictoryPoints();
		}
		this.theonewhomakeactions.addBonus(new Bonus(assis, car, false, coi, nob, vic));
		checkSpecialBonus(t);
	}

	private void removeCopies(List<Town> withemporium) {
		int length = withemporium.size();
		/* eliminate possible copies of cities */
		for (int j = 0; j < length; j++) {
			int k = j + 1;
			while (k < length) {
				if (withemporium.get(j) == withemporium.get(k)) {
					withemporium.remove(k);
					k--;
				}
				length = withemporium.size();
				k++;
			}
			length = withemporium.size();
		}
	}

	private boolean isEmporiumBuildThere(Town tw) {
		for (Emporium e : this.emporiums) {
			if (e.getOwner() == this.theonewhomakeactions && e.getTown() == tw) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo per il calcolo del bonus di una tipologia di città. Controlla se
	 * si ha construito un emporio in ogni città Bronze, Silver, Gold, Iron e
	 * anche quelle delle regioni Coast, Hills, Mountains.
	 * 
	 * @param where
	 *            Città in cui si è costruito l'emporio
	 */
	private synchronized void checkSpecialBonus(Town where) {
		int[] ar = countBonus();
		int possbonuscoast = ar[0];
		int possbonushills = ar[1];
		int possbonusmountains = ar[2];
		int possbonusgold = ar[3];
		int possbonussilver = ar[4];
		int possbonusbronze = ar[5];
		int possbonusiron = ar[6];
		int numofbonus = 0;
		boolean checked = assignBonusRegion(where, possbonuscoast, possbonushills, possbonusmountains);
		if (checked) {
			numofbonus++;
		}
		checked = assignBonusType(where, possbonusgold, possbonussilver);
		if (checked) {
			numofbonus++;
		}
		checked = assignBonusType2(where, possbonusbronze, possbonusiron);
		if (checked) {
			numofbonus++;
		}
		for (int i = 0; i < numofbonus && this.kingbonus <= 5; i++) {
			Bonus b = new Bonus(Constant.REGIONBONUS, this.config.getMaxBonus());
			this.theonewhomakeactions.addBonus(b);
			this.kingbonus++;
		}
	}

	private int[] countBonus() {
		int[] ar = { 0, 0, 0, 0, 0, 0, 0 };
		for (Emporium e : this.emporiums) {
			if (e.getOwner() == this.theonewhomakeactions) {
				if (e.getTown().getRegion() == this.coast) {
					ar[0]++;
				}
				if (e.getTown().getRegion() == this.hills) {
					ar[1]++;
				}
				if (e.getTown().getRegion() == this.mountains) {
					ar[2]++;
				}
				if (e.getTown().getType() == Constant.GOLD) {
					ar[3]++;
				}
				if (e.getTown().getType() == Constant.SILVER) {
					ar[4]++;
				}
				if (e.getTown().getType() == Constant.BRONZE) {
					ar[5]++;
				}
				if (e.getTown().getType() == Constant.IRON) {
					ar[6]++;
				}
			}
		}
		return ar;
	}

	private boolean assignBonusType(Town where, int possbonusgold, int possbonussilver) {
		/*
		 * assegno bonus tipo
		 */
		if (where.getType().equals(Constant.GOLD) && possbonusgold == this.bonusgoldnumber) {
			this.theonewhomakeactions.addBonus(bonusgold);
			return true;
		}
		if (where.getType().equals(Constant.SILVER) && possbonussilver == this.bonussilvernumber) {
			this.theonewhomakeactions.addBonus(bonussilver);
			return true;
		}

		return false;
	}

	private boolean assignBonusType2(Town where, int possbonusbronze, int possbonusiron) {
		if (where.getType().equals(Constant.BRONZE) && possbonusbronze == this.bonusbronzenumber) {
			this.theonewhomakeactions.addBonus(bonusbronze);
			return true;
		}
		if (where.getType().equals(Constant.IRON) && possbonusiron == this.bonusironnumber) {
			this.theonewhomakeactions.addBonus(bonusiron);
			return true;
		}
		return false;
	}

	private boolean assignBonusRegion(Town where, int possbonuscoast, int possbonushills, int possbonusmountains) {
		/*
		 * assegno bonus regioni
		 */
		if (where.getRegion() == this.coast && possbonuscoast == Constant.NUMBEROFCITYPERREGION) {
			this.theonewhomakeactions.addBonus(this.coast.getBonus());
			return true;
		}
		if (where.getRegion() == this.hills && possbonushills == Constant.NUMBEROFCITYPERREGION) {
			this.theonewhomakeactions.addBonus(this.hills.getBonus());
			return true;
		}
		if (where.getRegion() == this.mountains && possbonusmountains == Constant.NUMBEROFCITYPERREGION) {
			this.theonewhomakeactions.addBonus(this.mountains.getBonus());
			return true;
		}
		return false;
	}

	/*
	 * Quick Action
	 */

	/**
	 * Azione veloce per ingaggiare un assistente
	 * 
	 * @param nomeplayer
	 *            Nome del client-giocatore
	 * @throws IllegalActionException
	 *             se non si ha un'azione veloce disponibile
	 */
	public synchronized void engageAssistant(String nomeplayer) throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (this.quickactionready) {
			if (this.theonewhomakeactions.getCoinsPosition() > 3) {
				this.theonewhomakeactions.moveCoins(-3);
				this.theonewhomakeactions.addAssistant();
				this.quickactionready = false;
			} else {
				throw new IllegalActionException(
						nomeplayer + "needs" + (3 - this.theonewhomakeactions.getCoinsPosition()) + "coins");
			}
		} else {
			throw new IllegalActionException(Constant.QUICKACTIONDONE);
		}

	}

	/**
	 * Azione veloce per cambiare una Business Tile in una regione
	 * 
	 * @param nomeplayer
	 *            Nome del client - giocatore
	 * @param region
	 *            Nome della regione in cui si vuole cambiare la tile
	 * @throws IllegalActionException
	 *             se non si ha un'azione veloce disponibile oppure se non si
	 *             hanno abbastanza assistenti
	 */
	public synchronized void changeBusinessTile(String nomeplayer, String region) throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (this.quickactionready) {
			try {
				this.theonewhomakeactions.addAssistant(-1);
			} catch (IllegalArgumentException e) {
				throw new IllegalActionException(e + Constant.NOTENOUGHASSISTANTS);
			}
			getRegion(region).changeTile();
			this.quickactionready = false;
		} else {
			throw new IllegalActionException(Constant.QUICKACTIONDONE);
		}
	}

	/**
	 * Azione veloce per eleggere un cosigliere con gli assistenti
	 * 
	 * @param nomeplayer
	 *            Nome client- giocatore
	 * @param c
	 *            Consigliere da eleggere
	 * @param region
	 *            Nome regione in cui eleggere il consigliere
	 * @throws IllegalActionException
	 *             se non si ha un'azione veloce disponibile oppure se non si
	 *             hanno abbastanza assistenti
	 */
	public synchronized void electCouncillorWithAssistant(String nomeplayer, Councillor c, String region)
			throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (this.quickactionready) {
			if (this.theonewhomakeactions.getAssistant() > 0) {
				this.theonewhomakeactions.addAssistant(-1);
				electCouncillor(c, region);
				this.quickactionready = false;
			} else {
				throw new IllegalActionException("Not enough assistants");
			}
		} else {
			throw new IllegalActionException(Constant.QUICKACTIONDONE);
		}
	}

	/**
	 * Azione veloce per ottenre un'ulteriore azione principare
	 * 
	 * @param nomeplayer
	 *            Nome client-giocatore
	 * @throws IllegalActionException
	 *             se non si ha un'azione veloce disponibile oppure se si ha
	 *             ancora un'azione principale
	 */
	public synchronized void additionalMainAction(String nomeplayer) throws IllegalActionException {
		if (!(this.theonewhomakeactions.getName().equals(nomeplayer) && !this.dontdoanythingelse)) {
			actionError(nomeplayer);
		}
		if (this.quickactionready) {
			if (!this.mainactionready) {
				this.mainactionready = true;
				this.quickactionready = false;
			} else {
				throw new IllegalActionException("MainAction is free to be used");
			}
		} else {
			throw new IllegalActionException(Constant.QUICKACTIONDONE);
		}
	}

	/**
	 * metodo che lancia delle IllegalArgumentException se il match è finito o
	 * se non è il turno di un giocatore
	 * 
	 * @param nameplayer
	 *            nome client - giocatore
	 */
	private synchronized void actionError(String nameplayer) {
		if (this.dontdoanythingelse) {
			throw new IllegalArgumentException(Constant.MATCHISOVER);
		} else {
			throw new IllegalArgumentException("It's not " + nameplayer + "'s turn");
		}
	}

	/*
	 * remote methods
	 */

	@Override
	public synchronized String remoteElectCouncillorWithEarn(String nomeplayer, int councillor, String region)
			throws RemoteException {
		return electCouncillorWithEarnCall(nomeplayer, councillor, region);
	}

	protected synchronized String electCouncillorWithEarnCall(String nomeplayer, int councillor, String region) {
		List<Councillor> listcounc = this.getCouncillorPoolList();
		try {
			this.electCouncillorWithEarn(nomeplayer, listcounc.get(councillor), region);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remoteAcquireBusinessTile(String nomeplayer, String region, int pos,
			int[] discardedcards) throws RemoteException {
		return acquireBusinessTileCall(nomeplayer, region, pos, discardedcards);
	}

	protected synchronized String acquireBusinessTileCall(String nomeplayer, String region, int pos,
			int[] discardedcards) {
		int numofdisc = discardedcards.length;
		PoliticCard[] disccards = new PoliticCard[numofdisc];
		int playerindex = 0;
		for (int i = 0; i < this.getPlayersName().size(); i++) {
			if (this.getPlayersName().get(i).equals(nomeplayer)) {
				playerindex = i;
			}
		}
		for (int i = 0; i < discardedcards.length; i++) {
			disccards[i] = this.getPlayers().get(playerindex).getHand().showCard().get(discardedcards[i]);
		}
		try {
			this.acquireBusinessTile(nomeplayer, region, pos, disccards);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remoteBuildEmporiumWithTile(String nomeplayer, String town, int businesstile)
			throws RemoteException {
		return buildEmporiumWithTileCall(nomeplayer, town, businesstile);
	}

	protected synchronized String buildEmporiumWithTileCall(String nomeplayer, String town, int businesstile) {
		Town t = null;
		BusinessTile used;
		for (Town e : this.getTowns()) {
			if (e.getName().equals(town)) {
				t = e;
			}
		}
		used = this.getPlayersTile().get(businesstile);
		try {
			this.buildEmporiumWithTile(nomeplayer, t, used);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remoteBuildEmporiumWithKing(String nomeplayer, String town, int[] discardedcards)
			throws RemoteException {
		return buildEmporiumWithKingCall(nomeplayer, town, discardedcards);
	}

	protected synchronized String buildEmporiumWithKingCall(String nomeplayer, String town, int[] discardedcards) {
		Town t = null;
		int numofdisc = discardedcards.length;
		PoliticCard[] disccards = new PoliticCard[numofdisc];
		int playerindex = 0;
		for (int i = 0; i < this.getPlayersName().size(); i++) {
			if (this.getPlayersName().get(i).equals(nomeplayer)) {
				playerindex = i;
			}
		}
		for (int i = 0; i < discardedcards.length; i++) {
			disccards[i] = this.getPlayers().get(playerindex).getHand().showCard().get(discardedcards[i]);
		}
		for (int i = 0; i < this.getTowns().length; i++) {
			if (this.getTowns()[i].getName().equals(town)) {
				t = this.getTowns()[i];
			}
		}
		try {
			this.buildEmporiumWithKing(nomeplayer, t, disccards);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remoteEngageAssistant(String nomeplayer) throws RemoteException {
		return engageAssistantCall(nomeplayer);
	}

	protected synchronized String engageAssistantCall(String nomeplayer) {
		try {
			this.engageAssistant(nomeplayer);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remoteChangeBusinessTile(String nomeplayer, String region) throws RemoteException {
		return changeBusinessTileCall(nomeplayer, region);
	}

	protected synchronized String changeBusinessTileCall(String nomeplayer, String region) {
		try {
			this.changeBusinessTile(nomeplayer, region);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remoteElectCouncillorWithAssistant(String nomeplayer, int councillor, String region)
			throws RemoteException {
		return electCouncillorWithAssistantCall(nomeplayer, councillor, region);
	}

	protected synchronized String electCouncillorWithAssistantCall(String nomeplayer, int councillor, String region) {
		try {
			this.electCouncillorWithAssistant(nomeplayer, this.getCouncillorPoolList().get(councillor), region);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remoteAdditionalMainAction(String nomeplayer) throws RemoteException {
		return additionalMainActionCall(nomeplayer);
	}

	protected synchronized String additionalMainActionCall(String nomeplayer) {
		try {
			this.additionalMainAction(nomeplayer);
		} catch (IllegalActionException e) {
			log.log(Level.FINE, e.toString(), e);
			return e.getMessage();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	@Override
	public synchronized String remotePassToNextRound(String nomeplayer) throws RemoteException {
		return nextTurnCall(nomeplayer);
	}

	protected synchronized String nextTurnCall(String nomeplayer) {
		if (this.dontdoanythingelse) {
			return Constant.MATCHISOVER;
		}
		if (!this.theonewhomakeactions.getName().equals(nomeplayer)) {
			return "It's not " + nomeplayer + "'s turn";
		}
		if (this.hasNextTurn()) {
			this.nextTurn();
		} else {
			endOfTheGame();
		}
		updateAllClientsMatch();
		return Constant.SUCCESS;
	}

	/* update client method */
	private void updateAllClientsMatch() {
		for (Player p : this.getPlayers()) {
			try {
				tryToUpdate(p);
			} catch (IOException e1) {
				offliners.add(p.getName());
				checkStatusNewPlayer();
				this.startOfflinerFoundTimer(p);
				log.log(Level.FINE, e1.toString(), e1);
			}
		}
	}

	private void tryToUpdate(Player p) throws IOException {
		boolean check = false;
		for (String k : offliners) {
			if (p.getName().equals(k) || p.getStatus().equals(Constant.DISCONNECTED)) {
				check = true;
			}
		}
		if (!check) {
			if (this.server.isRmiOrSocket(p.getName()).equals(Constant.RMI)) {
				RemoteClient stub = getClientStub(p.getName());
				stub.updateMatch();
			} else if (this.server.isRmiOrSocket(p.getName()).equals(Constant.SOCKET)) {
				this.getClientSocket(p.getName()).updateMatch(this);
			}
		}
	}

	/**
	 * Metodo per cercare lo stub di un client
	 * 
	 * @param clientname
	 *            Nome del client
	 * @return Lo stub del client
	 */
	private RemoteClient getClientStub(String clientname) {
		RemoteClient stub;
		try {
			Registry registry;
			registry = LocateRegistry.getRegistry(Constant.PORT);
			stub = (RemoteClient) registry.lookup(clientname);
			return stub;
		} catch (RemoteException | NotBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
			return null;
		}
	}

	private SocketListener getClientSocket(String clientname) {
		for (SocketListener e : server.getSocketList()) {
			if (e.getName().equals(clientname)) {
				return e;
			}
		}
		throw new IllegalArgumentException();
	}

	/* UI rmi method */
	@Override
	public String remoteWhoIsNow() throws RemoteException {
		return this.whoIsNow();
	}

	@Override
	public King remoteGetKing() throws RemoteException {
		return this.getKing();
	}

	@Override
	public NobilityTrack remoteGetNobilityTrack() throws RemoteException {
		return this.getNobilityTrack();
	}

	@Override
	public List<Player> remoteGetPlayers() throws RemoteException {
		return this.getPlayers();
	}

	@Override
	public Region remoteGetRegion(String r) throws RemoteException {
		return this.getRegion(r);
	}

	@Override
	public List<Emporium> remoteGetEmporium() throws RemoteException {
		return this.getEmporiums();
	}

	@Override
	public List<String> remoteGetPlayersname() throws RemoteException {
		return this.getPlayersName();
	}

	@Override
	public List<BusinessTile> remoteGetPlayersTile() throws RemoteException {
		return this.getPlayersTile();
	}

	@Override
	public Town[] remoteGetTowns() throws RemoteException {
		return this.getTowns();
	}

	@Override
	public CouncilBalcony remoteGetCouncillorBalcony(String r) throws RemoteException {
		return this.getBalcony(r);
	}

	@Override
	public List<Councillor> remoteGetCouncillorPoolList() throws RemoteException {
		return this.getCouncillorPoolList();
	}

	@Override
	public Bonus remoteGetBonusGoldTown() throws RemoteException {
		return this.getBonusGoldTown();
	}

	@Override
	public Bonus remoteGetBonusSilverTown() throws RemoteException {
		return this.getBonusSilverTown();
	}

	@Override
	public Bonus remoteGetBonusBronzeTown() throws RemoteException {
		return this.getBonusBronzeTown();
	}

	@Override
	public Bonus remoteGetBonusIronTown() throws RemoteException {
		return this.getBonusIronTown();
	}

	@Override
	public String remoteWhoIsTheWinner() throws RemoteException {
		return this.whoIsTheWinner();
	}

	@Override
	public void reconnect(String clientname) throws RemoteException {
		for (int i = 0; i < offliners.size(); i++) {
			if (offliners.get(i).equals(clientname)) {
				offliners.remove(i);
				this.offlinetimers.get(i).cancel();
			}
		}
	}

	@Override
	public synchronized void noMoreAfk(String who) throws RemoteException {
		noMoreAfkCall(who);
	}

	protected synchronized void noMoreAfkCall(String who) {
		for (Player p : this.getPlayers()) {
			if (p.getName().equals(who) && p.getStatus().equals(Constant.AFK)) {
				p.setStatus(Constant.AVAILABLE);
			}
		}
	}

	/**
	 * Metodo per la gestione delle fasi di market, andrà a cambiare le
	 * variabili boolean marketbuyingphase e marketsellingphase a seconda di
	 * quale fase del gioco si è arrivati. Viene lanciata una volta a turno ed
	 * andrà a decrementare una variabile intera che è inizialmente uguale al
	 * numero dei giovatori nella partita. Quando marketcounter arriverà a zero
	 * si cambierà di fase. Una volta arrivati alla fase in cui si deve mettere
	 * in vendita verrà creato un nuovo ordine di giocatori e scambiato
	 * temporaneamente (fino alla fine delle fasi di market) con la lista dei
	 * giocatori
	 */
	public void checkMarketPhases() {
		this.marketcounter--;
		if (this.marketcounter == 0 && !this.marketbuyingphase && !this.marketsellingphase) {
			market = new Market(this.players);
			this.marketcounter = this.players.size();
			this.marketsellingphase = true;
			return;
		} else {
			this.checkMarketOtherPhases();
		}
	}

	public void checkMarketOtherPhases() {
		if (this.marketcounter == 0 && this.marketsellingphase && !this.marketbuyingphase) {
			this.marketcounter = this.players.size();
			this.marketsellingphase = false;
			this.marketbuyingphase = true;
			this.temporaryplayers = this.players;
			this.players = market.getPlayersOrder();
			return;
		} else if (this.marketcounter == 0 && !this.marketsellingphase && this.marketbuyingphase) {
			this.marketcounter = this.players.size();
			this.marketbuyingphase = false;
			this.players = this.temporaryplayers;
			return;
		}
	}

	@Override
	public boolean remoteIsSellingPhase() throws RemoteException {
		return this.marketsellingphase;
	}

	public boolean isSellingPhase() {
		return this.marketsellingphase;
	}

	@Override
	public boolean remoteIsBuyingPhase() throws RemoteException {
		return this.marketbuyingphase;
	}

	public boolean isBuyingPhase() {
		return this.marketbuyingphase;
	}

	@Override
	public void addMarketSupply(String playername, int assistants, int[] business, int[] politics, int price)
			throws RemoteException {
		if (this.theonewhomakeactions.getName().equals(playername) && !this.dontdoanythingelse) {
			int index = 0;
			for (int i = 0; i < players.size(); i++) {
				if (playername.equals(players.get(i).getName())) {
					index = i;
				}
			}
			List<BusinessTile> businesstiles = new ArrayList<>();
			for (int i = 0; i < business.length; i++) {
				businesstiles.add(this.getPlayersTile().get(business[i]));
			}
			List<PoliticCard> politicscards = new ArrayList<>();
			for (int i = 0; i < politics.length; i++) {
				politicscards.add(players.get(index).getHand().showCard().get(politics[i]));
			}
			Supply supply = new Supply(assistants, businesstiles, politicscards, price, players.get(index));
			this.market.addSupply(supply);
		} else {
			actionError(playername);
		}
	}

	@Override
	public List<Supply> remoteShowSupplies() throws RemoteException {
		return showSupplies();
	}

	public List<Supply> showSupplies() {
		return this.market.getSuppliesList();
	}

	@Override
	public String buySupply(String playername, int supplyindex) throws RemoteException {
		return this.market.buySupply(playername, this.market.getSuppliesList().get(supplyindex));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == (Match) obj) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return players.size();
	}

	@Override
	public boolean giveMeTavolino() throws RemoteException {
		return this.tavolino;
	}

	public boolean isEnd() {
		return this.dontdoanythingelse;
	}

}