package server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import client.MyTimer;
import model.Configuration;
import model.Constant;
import model.IllegalActionException;
import model.RemoteClient;
import model.RemoteServer;

/**
 * Classe che gestisce tutte le connessioni con i client e la lista delle
 * partite.
 *
 */
public class Server extends UnicastRemoteObject implements RemoteServer {
	private static final Logger log = Logger.getLogger(Server.class.getName());
	private List<String> players;
	private List<String> avaibleplayers;
	private transient List<Match> matches;
	private transient List<RemoteClient> stubs;
	private transient ServerSocketListener serversocket;
	private transient List<SocketListener> sockets;
	private transient Registry registry;
	private boolean waiting;

	/**
	 * Costruttore che inizializza tutte le variabili del server e si mette
	 * online nel registry alla porta Constant.PORT
	 * 
	 * @throws RemoteException
	 *             se ci sono problemi di connessione, porta già in uso
	 * @throws AlreadyBoundException
	 *             Se c'è già un server online in quel registry
	 */
	public Server() throws RemoteException, AlreadyBoundException {
		waiting = false;
		stubs = new ArrayList<>();
		matches = new ArrayList<>();
		players = new ArrayList<>();
		avaibleplayers = new ArrayList<>();
		sockets = new ArrayList<>();
	}

	public void start() throws IOException {
		try {
			registry = LocateRegistry.createRegistry(Constant.PORT);
			registry.bind("Server", this);
			serversocket = new ServerSocketListener(this);
			Thread tr = new Thread(serversocket);
			tr.start();
			endMatchTimer();
		} catch (RemoteException | AlreadyBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);

		}
	}

	public String login(String clientname, String password) {
		try (FileReader fr = new FileReader(Constant.ACCOUNTFOLDER + Constant.LISTCONST)) {
			for (String e : players) {
				if (clientname.equals(e)) {
					return Constant.ALREADYCONNECTED;
				}
			}
			if (checkLoginFile(clientname, password, fr)) {
				return Constant.CONNECTSUCCESS;
			} else {
				return Constant.NOLOGINMATCH;
			}

		} catch (IOException e) {
			log.log(Level.SEVERE, e.toString(), e);
			return Constant.FILEREADPROBLEM;

		}

	}

	@Override
	public String remoteLogin(String clientname, String password) throws RemoteException {
		return this.login(clientname, password);
	}

	/**
	 * Metodo ausiliario per il controllo del file degli account durante il
	 * login o la creazione di un account
	 * 
	 * @param clientname
	 *            nome del client
	 * @param password
	 *            password dell'account
	 * @param fr
	 *            FileReader associato al file degli accounts
	 * @return true se c'è una corrispondenza e quindi l'account cercato esiste,
	 *         falso altrimenti
	 */
	public boolean checkLoginFile(String clientname, String password, FileReader fr) {
		String tempname;
		String temppass;
		Scanner in = new Scanner(new BufferedReader(fr));
		while (in.hasNext()) {
			tempname = in.next();
			temppass = in.next();
			if (tempname.equals(clientname) && temppass.equals(password)) {
				in.close();
				return true;
			}
		}
		in.close();
		return false;
	}

	public String createAccount(String clientname, String password) {
		String tempname;
		try (FileReader fr = new FileReader(Constant.ACCOUNTFOLDER + Constant.LISTCONST)) {
			Scanner in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				tempname = in.next();
				if (tempname.equals(clientname)) {
					log.log(Level.FINEST, Constant.NAMEEXISTS);
					in.close();
					return Constant.NAMEEXISTS;
				}
				in.next();
			}
			in.close();
		} catch (FileNotFoundException e) {
			log.log(Level.FINE, e.toString(), e);
		} catch (IOException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}

		try {
			FileWriter fw = new FileWriter(Constant.ACCOUNTFOLDER + "list.txt", true);
			fw.write('\n' + clientname + " " + password);
			fw.close();
			return Constant.CONNECTSUCCESS;
		} catch (IOException e) {
			log.log(Level.FINE, e.toString(), e);
			return Constant.FILEREADPROBLEM;
		}
	}

	@Override
	public String remoteCreateAccount(String clientname, String password) throws RemoteException {
		return this.createAccount(clientname, password);
	}

	@Override
	public boolean lookForStub(String clientname) throws RemoteException {
		try {
			registry = LocateRegistry.getRegistry(Constant.PORT);
			stubs.add((RemoteClient) registry.lookup(clientname));
		} catch (RemoteException | NotBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
		players.add(clientname);
		return true;
	}

	public String joinGame(String clientname) {
		if (this.matches.isEmpty() || this.matches.get(matches.size() - 1).isStarted()
				|| matches.get(matches.size() - 1).getPlayers().size() == matches.get(matches.size() - 1).getConfig()
						.getMaxNumberOfPlayers()) {
			if (waiting) {
				this.avaibleplayers.add(clientname);
				return Constant.WAITING;
			} else {
				waiting = true;
				this.avaibleplayers.add(clientname);
				// confronta tutti i players (TRAMITE LIST CONFRONTER) con
				// clientname per trovare l'indice del suo stub
				this.giveMeConfig(clientname);
				return Constant.WAITING;
			}
		} else {
			try {
				// the match method addplayer returns true if the player added
				// is the second, so the timer will begin

				if (matches.get(matches.size() - 1).addPlayer(clientname)) {
					startMatchTimer(matches.get(matches.size() - 1));
				}

			} catch (IllegalActionException e) {
				log.log(Level.SEVERE, e.toString(), e);
			}
			// the calling client will get an update with the game config and
			// the index of the match
			updateClientConfig(clientname, matches.get(matches.size() - 1).getConfig());
			return Constant.SUCCESS;
		}
	}

	public String isRmiOrSocket(String clientname) {
		try {
			for (int i = 0; i < this.registry.list().length; i++) {
				if (registry.list()[i].equals(clientname)) {
					return Constant.RMI;
				}
			}
			return Constant.SOCKET;
		} catch (RemoteException e) {
			log.log(Level.SEVERE, e.toString(), e);
			return Constant.ERROR;
		}
	}

	public void giveMeConfig(String clientname) {
		if (this.isRmiOrSocket(clientname).equals(Constant.RMI)) {
			try {
				RemoteClient client = (RemoteClient) registry.lookup(clientname);
				client.giveMeConfig();
			} catch (RemoteException | NotBoundException e) {
				log.log(Level.SEVERE, e.toString(), e);
			}
		} else {
			this.sockets.get(this.listSocketConfronter(clientname)).giveMeConfig();
		}
	}

	@Override
	public String remoteJoinGame(String clientname) throws RemoteException {
		return this.joinGame(clientname);
	}

	public void setConfig(Configuration c) {
		int index = 0;
		Match matchind = createMatch(c);
		for (int matchsize = 0; !this.avaibleplayers.isEmpty() && matchsize < c.getMaxNumberOfPlayers(); matchsize++) {
			try {
				// se è il secondo giocatore ad essere aggiungi la condizione
				// sarà true e farà partire il timer
				if (matches.get(matches.size() - 1).addPlayer(this.avaibleplayers.get(index))) {
					startMatchTimer(matchind);
				}
				this.updateClientConfig(avaibleplayers.get(index), matchind.getConfig());
			} catch (IllegalActionException e) {
				log.log(Level.FINEST, e.toString(), e);
				break;

			}
			avaibleplayers.remove(index);
		}
		waiting = false;
		// calls the first client on avaiblplayer if there are any left and
		// tells him to pass a game configuration (then he'll call a setconfig)

		for (int i = 0; i < this.players.size() && !this.avaibleplayers.isEmpty(); i++) {
			if (this.players.get(i).equals(this.avaibleplayers.get(0))) {
				// confronts every player with the first of avaiblplayer to find
				// the index of his stub
				giveMeConfig(this.avaibleplayers.get(0));
			}
		}
	}

	@Override
	public void remoteSetConfig(Configuration c) throws RemoteException {
		this.setConfig(c);
	}

	/**
	 * Metodo per inizializzare un timer per far iniziare la partita.
	 * 
	 * @param matchind
	 *            indirizzo del match, serve per far iniziare la partita e avere
	 *            la lista dei giocatori presenti per mandargli un'update allo
	 *            scadere del timer
	 */
	public void startMatchTimer(Match matchind) {

		MyTimer timer = new MyTimer();
		timer.schedule(() -> {
			matchind.start();
			for (int i = 0; i < matchind.getPlayersName().size(); i++) {
				try {
					updateMatch(matchind.getPlayersName().get(i), matchind);
				} catch (IOException e) {
					log.log(Level.FINE, e.toString(), e);
				}
			}
		}, Constant.STARTMATCHTIMER);
	}

	public void updateMatch(String clientname, Match match) throws IOException {
		String result = this.isRmiOrSocket(clientname);
		if (result.equals(Constant.RMI)) {
			RemoteClient client;
			try {
				client = (RemoteClient) registry.lookup(clientname);
				client.updateMatch();
			} catch (NotBoundException e) {
				log.log(Level.SEVERE, e.toString(), e);
			}
		} else if (result.equals(Constant.SOCKET)) {
			this.sockets.get(this.listSocketConfronter(clientname)).updateMatch(match);
		}
	}

	/**
	 * Metodo che fa un'update al client mandandogli la configurazione della
	 * partita in cui è entrato
	 * 
	 * @param clientname
	 *            nome del client
	 * @param c
	 *            configurazione della partita
	 */
	public void updateClientConfig(String clientname, Configuration c) {
		try {
			String result = this.isRmiOrSocket(clientname);
			if (result.equals(Constant.RMI)) {
				RemoteClient client = (RemoteClient) registry.lookup(clientname);
				client.updateConfig(c, matches.size() - 1);
			} else if (result.equals(Constant.SOCKET)) {
				this.sockets.get(this.listSocketConfronter(clientname)).updateConfig(c, matches.size() - 1);
			}

		} catch (IOException | NotBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
	}

	/**
	 * Confronta tutti i players con clientname per trovare l'indice del suo
	 * stub
	 * 
	 * @param clientname
	 *            nome del client
	 * @return indice nella lista
	 */
	public int listConfronter(String clientname) {
		int i = 0;
		for (; i < this.players.size(); i++) {
			if (this.players.get(i).equals(clientname)) {
				return i;
			}
		}
		return i;

	}

	public int listSocketConfronter(String clientname) {
		int i = 0;
		for (; i < this.sockets.size(); i++) {
			if (this.sockets.get(i).getName().equals(clientname)) {
				return i;
			}
		}
		return i;
	}

	/**
	 * Crea il match con la configurazione data, se ci sono problemi con la
	 * creazione del match con quella configurazione ne verrà creato uno con
	 * configurazioni di default
	 * 
	 * @param c
	 *            la configurazione
	 * @return Il match creato
	 */
	public Match createMatch(Configuration c) {
		Match match;
		try {
			match = new Match(c, this);
			matches.add(match);
			registry = LocateRegistry.getRegistry(Constant.PORT);
			registry.bind("match" + (matches.size() - 1), match);

			return match;
		} catch (IOException | AlreadyBoundException e) {
			c.setMap(Constant.DEFAULT);
			match = createMatch(c);
			log.log(Level.SEVERE, e.toString(), e);

		}
		return match;
	}

	@Override
	public boolean equals(Object obj) {
		return this == obj;
	}

	@Override
	public int hashCode() {
		return 0;
	}

	public void playerDisconnected(String clientname) {
		int index = 0;
		for (String e : this.players) {
			if (e.equals(clientname)) {
				this.players.remove(index);
				break;
			}
			index++;
		}

	}

	@Override
	public void remotePlayerDisconnected(String clientname) throws RemoteException {
		this.playerDisconnected(clientname);
	}

	public void disconnectSocketPlayer(SocketListener sock) {
		for (int i = 0; i < this.sockets.size(); i++) {
			if (this.sockets.get(i) == sock) {
				this.sockets.remove(i);
				break;
			}
		}
		this.playerDisconnected(sock.getName());
	}

	public List<SocketListener> getSocketList() {
		return this.sockets;
	}

	public void addSocketListener(SocketListener socketliste) {
		this.sockets.add(socketliste);
	}

	public void addPlayer(String playername) {
		this.players.add(playername);
	}

	public List<Match> getMatches() {
		return this.matches;
	}

	private void endMatchTimer() {
		MyTimer timer = new MyTimer();
		timer.scheduleAtFixedRate(() -> {
			for (int i = 0; i < matches.size(); i++) {
				Match m = matches.get(i);
				if (m.isEnd()) {
					sleep();// let the server update all the clients before shut
							// the match down
					m = null;
				}
			}
		}, 500, 500);
	}

	private void sleep() {
		try {
			Thread.sleep(5000);
		} catch (Exception e1) {
			log.log(Level.SEVERE, "Err", e1);
			sleep();
		}
	}

}
