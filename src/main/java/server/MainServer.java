package server;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainServer {
	private static final Logger log = Logger.getLogger(Server.class.getName());
	private Server serv;

	MainServer() throws IOException {
		try {
			serv = new Server();
		} catch (AlreadyBoundException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
	}

	private void start() throws IOException {
		serv.start();
	}

	public static void main(String[] args) throws IOException {
		try {
			MainServer main = new MainServer();
			main.start();
		} catch (RemoteException e) {
			log.log(Level.SEVERE, e.toString(), e);
		}
	}
}
