package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerSocketListener implements Runnable {
	ServerSocket serversocket;
	Server server;
	private static final Logger log = Logger.getLogger(Server.class.getName());
	
	ServerSocketListener(Server server) throws IOException{
		this.server = server;
		serversocket = new ServerSocket(2003);
	}
	
	@Override
	public void run() {
		while(true){
			try {
				SocketListener socketliste = new SocketListener(this.server, serversocket.accept());
				Thread tr = new Thread(socketliste);
				tr.start();
			} catch (IOException e) {
				log.log(Level.SEVERE, e.toString(), e);
			}
		}
		
	}

}
