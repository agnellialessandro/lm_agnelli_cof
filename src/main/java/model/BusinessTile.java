package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * 
 * Classe che gestisce le business tiles
 * 
 */
public class BusinessTile implements Serializable {
	private List<Town> towns;
	private Player player;
	private Bonus bonus;
	/** 
	 * @param t
	 * Lista di città a cui si riferirà per le costruzioni.
	 * @param maxbonus
	 * valore massimo
	 */
	public BusinessTile(Town[] t, int maxbonus) {
		towns = new ArrayList<>();
		randomtown(t);
		this.player = null;
		this.bonus = new Bonus(Constant.CLASSICBONUS, maxbonus);
	}
	/**
	 * Metodo che sceglie una delle città presenti nella lista per generarne una o più da associare a towns.
	 * @param t
	 * Lista di città da cui verranno generate le città per la costruzione.
	 */
	private void randomtown(Town[] t) {
		Random random = new Random();
		int i = random.nextInt(101);
		if (i <= 56) {
			int l = random.nextInt(Constant.NUMBERCITIES);
			towns.add(t[l]);
		} else {
			if (i <= 89) {
				int l = random.nextInt(Constant.NUMBERCITIES);
				towns.add(t[l]);
				int e = l;
				while (e == l) {
					e = random.nextInt(Constant.NUMBERCITIES);
				}
				towns.add(t[e]);
			} else {
				int l = random.nextInt(Constant.NUMBERCITIES);
				towns.add(t[l]);
				int e = l;
				while (e == l) {
					e = random.nextInt(Constant.NUMBERCITIES);
				}
				towns.add(t[e]);
				int s = l;
				while (s == l || s == e) {
					s = random.nextInt(Constant.NUMBERCITIES);
				}
				towns.add(t[s]);
			}
		}
	}

	public List<Town> getTown() {
		return this.towns;
	}

	public Bonus getBonus() {
		return this.bonus;
	}

	public Player getPlayer() {
		return this.player;
	}

	public void setPlayer(Player p) {
		this.player = p;
	}
}
