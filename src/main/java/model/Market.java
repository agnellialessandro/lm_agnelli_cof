package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * Questa classe andrà a gestire le fasi di market. Durante la prima fase verranno messe in vendita delle supply da ogni giocatore, nella seconda 
 * verranno comprate. Al termine di ogni fase di market la classe verrà reinizializzata.
 * @author Loris
 *
 */
public class Market {
	private List<Supply> supplies;
	private List<Player> players;
	/**
	 * Costruttore, serve la lista dei giocatori presenti nel gioco. Durante l'inizializzazione verrà creata una nuova lista con gli stessi giocatori
	 * ma con un ordine diverso. Questa lista sarà poi usata in match per sostituire il vecchio ordine di giocatori con il nuovo.
	 * @param listofplayers
	 * Lista dei giocatori in gioco
	 */
	public Market(List<Player> listofplayers) {
		supplies = new ArrayList<>();
		players = new ArrayList<>();
		List<Player> tempplayers = new ArrayList<>();
		for(int i=0; i<listofplayers.size(); i++){
			tempplayers.add(listofplayers.get(i));
		}
		Random rand = new Random();
		int i = rand.nextInt(tempplayers.size());
		while (!tempplayers.isEmpty()) {
			players.add(tempplayers.get(i));
			tempplayers.remove(i);
			if(!tempplayers.isEmpty())
				i = rand.nextInt(tempplayers.size());
		}
	}

	public void addSupply(Supply supply) {
		supplies.add(supply);
	}

	public List<Supply> getSuppliesList() {
		return this.supplies;
	}

	public List<Player> getPlayersOrder() {
		return this.players;
	}
	/**
	 * Metodo per comprare una supply
	 * @param playername
	 * Nome del giocatore che vuole comprare
	 * @param supply
	 * Indirizzo della supply da comprare
	 * @return
	 * Constant.SUCCESS se è stata comprata con successo
	 * @return
	 * Constant.NOTENOUGHCOINS se il giocatore non aveva abbastanza monete
	 */
	public String buySupply(String playername, Supply supply) {
		int index = 0;
		int indexofexowner = 0;
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getName().equals(playername)) {
				index = i;
			}
		}

		for (int i = 0; i < players.size(); i++) {
			if (players.get(i) == supply.getSeller()) {
				indexofexowner = i;
			}
		}

		if (supply.getPrice() <= players.get(index).getCoinsPosition()) {
			adjustPlayersCurrencies(supply, index, indexofexowner);
			return Constant.SUCCESS;
		} else {
			return Constant.NOTENOUGHCOINS;
		}
	}
	
	/**
	 * Metodo supplementare per buySupply, assicura di rimuovere gli oggetti dal vecchio proprietario e gli dà il guadagno. Gli oggetti rimossi
	 * verranno assegnati al nuovo giocatore. Il metodo viene lanciato solo se il giocatore comprante ha abbastanza soldi
	 * @param supply
	 * Indirizzo della supply da comprare
	 * @param index
	 * indice nella lista dei giocatori del giocatore comprante
	 * @param indexofexowner
	 * indice nella lista dei giocatori del vecchio proprietario
	 */
	public void adjustPlayersCurrencies(Supply supply, int index, int indexofexowner) {
		players.get(index).moveCoins(-(supply.getPrice()));
		players.get(indexofexowner).moveCoins(supply.getPrice());

		players.get(index).addAssistant(supply.getAssistant());
		players.get(indexofexowner).addAssistant(-supply.getAssistant());

		for (BusinessTile e : supply.getBusiness()) {
			e.setPlayer(players.get(index));
			players.get(indexofexowner).decreaseTilesowned();
			players.get(index).increaseTilesowned();
		}
		
		for(int k=0; k< supply.getPolitics().size(); k++){
			for (int i = 0; i < players.get(index).getHand().showCard().size(); i++) {
				
				if (players.get(indexofexowner).getHand().showCard().get(i).equals(supply.getPolitics().get(k))) {
					players.get(indexofexowner).getHand().showCard().remove(i);
					players.get(index).getHand().addCard(supply.getPolitics().get(k).getColor());			
					break;
				}
			}
		}

		for (int i = 0; i < supplies.size(); i++) {
			if (supplies.get(i) == supply) {
				supplies.remove(i);
			}
		}

	}

	public void clearTheSupplies() {
		this.players.clear();
	}

}
