package model;

import java.io.Serializable;

public class King implements Serializable {

	private Town here;
	private CouncilBalcony kingcb;

	public King(Town initTown, CouncilBalcony cb) {
		/*
		 * initTown is the capital, and the king starts always from here
		 */
		this.here = initTown;
		this.kingcb = cb;
	}

	public Town getTown() {
		return this.here;
	}

	public void move(Town t) {
		this.here = t;
	}

	public CouncilBalcony getBalcony() {
		return this.kingcb;
	}
}
