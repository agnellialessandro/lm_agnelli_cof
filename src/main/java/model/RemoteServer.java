package model;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * Metodi remoti del lato server, attraverso questi il client riesce a comunicare con il server.
 * 
 */
public interface RemoteServer extends Remote {
	/**
	 * Metodo per il login, le credenziali verranno controllate in un file con tutti gli accounts
	 * @param clientname
	 * nome del client
	 * @param password
	 * password dell'account
	 * @return
	 * Stringa di successo, account al momento già online, account non esistente, problema lettura file accounts
	 * @throws RemoteException
	 * se ci sono problemi di connessione
	 */
	String remoteLogin(String clientname, String password) throws RemoteException;
	
	/**
	 * Metodo per poter creare un account
	 * @param clientname
	 * nome dell'account da creare
	 * @param password
	 * password dell'account da creare
	 * @return
	 * Stringa di successo, nome già esistente, problemi con il file accounts
	 * @throws RemoteException
	 * se ci sono problemi di connessione
	 */
	String remoteCreateAccount(String clientname, String password) throws RemoteException;
	
	/**
	 * Metodo per avvisare il server che può andare a cercare lo stub del client, in questo modo si creerà una doppia connessione.
	 * Se ha successo il client è definitivamente online e viene aggiunto alla lista in server dei giocatori online
	 * @param clientname
	 * nome del client
	 * @return
	 * true se ha successo
	 * @throws RemoteException
	 * se ci sono problemi di connessione
	 */
	boolean lookForStub(String clientname) throws RemoteException;
	
	/**
	 * Metodo per potersi unire ad un game. Se il client è il primo ad unirsi ad un game libero inizializzerà la configurazione del gioco, se è
	 * il secondo farà partire un timer per far partire la partita
	 * @param clientname
	 * nome client
	 * @return
	 * Stringa di successo o waiting se è in corso l'inizializzazione della configurazione
	 * @throws RemoteException
	 * se ci sono problemi di connessione
	 */
	String remoteJoinGame(String clientname) throws RemoteException;
	
	/**
	 * Metodo per indicare la configurazione della partita, in questo modo si crea la partita e vengono aggiunti tutti i giocatori che erano disponibili 
	 * @param c
	 * configurazione da impostare alla partita in creazione
	 * @throws RemoteException
	 * se ci sono problemi di connessione
	 */
	void remoteSetConfig(Configuration c) throws RemoteException;
	
	/**
	 * Metodo che match chiamerà per comunicare al server che un giocatore si è disconnesso, in questo modo lo toglierà dalla lista dei client online
	 * così da renderlo disponibile per nuovi login
	 * @param clientname
	 * nome del client da disconnettere
	 * @throws RemoteException
	 * se ci sono problemi di connessione
	 */
	void remotePlayerDisconnected(String clientname) throws RemoteException;
	
}
