package model;

import java.awt.Color;

public class Constant {
	public static final String TITLE = "Council of Four";

	public static final String GUI = "GUI";
	public static final String RMI = "RMI";
	public static final String SOCKET = "socket";

	public static final int PORT = 2001;
	public static final int SOCKETPORT = 2003;

	public static final int MAXPLAYERS = 8;

	public static final int STARTINGCOUNCILLOR = 24;
	public static final int NUMBERCITIES = 15;
	public static final int NUMBEROFCITYPERREGION = 5;
	public static final int NUMBEROFCOLORCARDS = 4;

	public static final String CLASSICBONUS = "Classic bonus";
	public static final String REGIONBONUS = "Region bonus";
	public static final String NOBILITYBONUS = "Nobility bonus";
	public static final String TOWNBONUS = "Town bonus";

	public static final Color GOLD = new Color(255, 215, 0);
	public static final Color SILVER = new Color(192, 192, 192);
	public static final Color BRONZE = new Color(205, 127, 50);
	public static final Color IRON = new Color(230, 231, 232);// is a bit
																// lighter than
																// silver
	public static final int BONUSELECTCOUNCILLOR = 4;
	public static final String MAPFOLDER = "maps/";
	public static final String ACCOUNTFOLDER = "account/";

	public static final String DEFAULT = "default";

	public static final String SERVERPROBLEMTIME = "Server response is taking too long.";
	public static final String ITSRMI = "It's RMI.";
	public static final String ITSSOCKET = "It's Socket.";
	public static final String ERROR = "Error.";
	public static final String ALREADYCONNECTED = "Connection refused: Account already in use.";
	public static final String NOLOGINMATCH = "Connection refused: Account doesn't exists or the combination username/password is wrong.";
	public static final String CONNECTSUCCESS = "Connected.";
	public static final String CONNECTIONPROBLEM = "Connection problem. Retry.";
	public static final String FILEREADPROBLEM = "Problem with file reader.";
	public static final String NAMEEXISTS = "Error, name already exists";
	public static final String WAITING = "Waiting.";
	public static final String GIVECONFIG = "Givemetheconfiguration.";
	public static final String SUCCESS = "Successful.";
	public static final String LISTCONST = "list.txt";
	public static final String NOPLAYING = "noplaying";

	public static final String WAITINGFORPLAYERS = "waiting for players";
	public static final String STARTED = "started";

	public static final int STARTMATCHTIMER = 10000;
	public static final int CHECKTIMER = 100;

	/*
	 * Match Constant String
	 */
	public static final String MAINACTIONDONE = "Main action already done";
	public static final String QUICKACTIONDONE = "Quick action already done";
	public static final String NOTENOUGHCOINS = "Not enough coins";
	public static final String CITYTILEMATCH = "City and business tile are not matching";
	public static final String EMPORIUMBUILDED = "Emporium already builded in this town";
	public static final String NOTENOUGHASSISTANTS = "Not enough assistants";
	public static final String NOTENOUGHCOINSORASS = "Not enough coins or assistants for this action";
	public static final String MATCHISOVER = "Match is over";
	public static final String MATCHISSTARTED = "Match is started";
	public static final String COASTNAME = "coast";
	public static final String HILLSNAME = "hills";
	public static final String MOUNTAINSNAME = "mountains";
	public static final String KINGNAME = "king";
	public static final String CHOICENOTALLOWED = "Choice not allowed! Retry";
	public static final String DECKEMPTY = "Deck is empty.";

	public static final String AREYOUSURE = "Are you sure? [Y/N]";

	/*
	 * status player
	 */
	public static final String AVAILABLE = "Available";
	public static final String AFK = "Away from keyboard";
	public static final String DISCONNECTED = "Disconnected";

	private Constant() {
	}/* this class is not meant to be instantiated */
}
