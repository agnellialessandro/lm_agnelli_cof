package model;

import java.awt.Color;
import java.io.Serializable;

/**
 * Classe che gestisce ogni giocatore. Ogni giocatore può essere in 4 diverse
 * situazioni: attivo- il giocatore sta giocando. afk: il giocatore è connesso
 * ma salta i turni. Offline - il giocatore ha un certo lasso di tempo per
 * riconnettersi. Disconnected - Il giocatore non può più riconnettersi a questo
 * specifico gioco.*
 *
 */
public class Player implements Serializable {
	private int nobilityposition;
	private int coinsposition;
	private int victoryposition;
	private int assistant;
	private String name;
	private Hand hand;
	private NobilityTrack nobtr;
	private int tilesowned = 0;

	private String status = Constant.AVAILABLE;

	/**
	 * Inizializza un giocatore.
	 * 
	 * @param name
	 *            Nome del giocatore
	 * @param deck
	 *            Mazzo di riferimento nel gioco
	 * @param nobtr
	 *            Nobility track di riferimento nel gioco
	 */
	public Player(String name, Deck deck, NobilityTrack nobtr) {
		this.nobilityposition = 0;
		this.coinsposition = 0;
		this.victoryposition = 0;
		this.assistant = 0;
		this.name = name;
		hand = new Hand(deck);
		this.nobtr = nobtr;
	}

	public void drawCard() {
		hand.drawCard();
	}

	public void discardCard(Color color) {
		hand.discardCard(color);
	}

	public int getPolitics() {
		return hand.getNumberOfCards();
	}

	public void addAssistant() {
		this.assistant++;
	}

	public void addAssistant(int num) {
		if (this.assistant + num < 0) {
			throw new IllegalArgumentException();
		}
		this.assistant = this.assistant + num;
	}

	/**
	 * Metodo per la gestione dei bonus su un giocatore
	 * 
	 * @param bonus
	 *            Oggetto bonus da cui verranno presi i bonus e associati al
	 *            giocatore
	 */
	public void addBonus(Bonus bonus) {
		assistant = assistant + bonus.getAssistants();
		coinsposition = coinsposition + bonus.getCoins();
		int temp = this.nobilityposition;
		nobilityposition = nobilityposition + bonus.getNobilityMoves();
		victoryposition = victoryposition + bonus.getVictoryPoints();
		for (int i = 0; i < bonus.getCards(); i++) {
			hand.drawCard();
		}
		if (temp != this.nobilityposition) {
			this.nobtr.checkBonus(this);
		}
	}

	public String getName() {
		return this.name;
	}

	public int getNobilityPosition() {
		return this.nobilityposition;
	}

	public int getCoinsPosition() {
		return this.coinsposition;
	}

	public int getVictoryPosition() {
		return this.victoryposition;
	}

	public int getAssistant() {
		return assistant;
	}

	public Hand getHand() {
		return hand;
	}

	public void moveNobility(int addtopos) {
		if (this.nobilityposition + addtopos > 21)
			this.nobilityposition = 21;
		else
			this.nobilityposition = this.nobilityposition + addtopos;
	}

	public void moveCoins(int addtopos) {
		if (this.coinsposition + addtopos < 0) {
			throw new IllegalArgumentException();
		}
		this.coinsposition = this.coinsposition + addtopos;
	}

	public void moveVictory(int addtopos) {
		this.victoryposition = this.victoryposition + addtopos;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTilesowned() {
		return tilesowned;
	}

	public void increaseTilesowned() {
		this.tilesowned++;
	}

	public void decreaseTilesowned() {
		this.tilesowned--;
	}

}
