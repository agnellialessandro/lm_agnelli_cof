package model;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Metodi remoti del lato client, attraverso questi il server o match riesce a
 * comunicare con il client
 *
 */
public interface RemoteClient extends Remote {
	/**
	 * Fa un aggiornamento della configurazione della partita al client, viene
	 * lanciata all'inizio dopo che un client si è connesso alla partita
	 * 
	 * @param c
	 *            La configurazione scelta dal primo giocatore
	 * @param idmatch
	 *            posizione nel registry del match
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void updateConfig(Configuration c, int idmatch) throws RemoteException;

	/**
	 * segnala il client che è partito il timer per l'inizio del match
	 * 
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void setTimer() throws RemoteException;

	/**
	 * Chiede al primo client che si è connesso qual è la configurazione della
	 * parita
	 * 
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void giveMeConfig() throws RemoteException;

	/**
	 * Fa un aggiornamento della partita al client, ogni volta che viene
	 * compiuta un'azione che cambia le caratteristiche di match viene chiamato
	 * 
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void updateMatch() throws RemoteException;

	/**
	 * Segnala il client che il match è finito
	 * 
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void endingMatch() throws RemoteException;

	public void playerGoneAfk(String s) throws RemoteException;

	public void playerDisconnect(String s) throws RemoteException;
}
