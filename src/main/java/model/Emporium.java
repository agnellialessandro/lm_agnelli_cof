package model;

import java.io.Serializable;

public class Emporium implements Serializable {

	private Player owner;
	private Town town;

	public Emporium(Player p, Town t) {
		this.owner = p;
		this.town = t;
	}

	public Player getOwner() {
		return this.owner;
	}

	public Town getTown() {
		return this.town;
	}
}
