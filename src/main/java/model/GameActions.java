package model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Lista di azioni che il client può compiere come azioni principali, veloci o
 * solo per ottenere informazioni dal match. Sono metodi remoti del match,
 * attraverso questi il client riesce a comunicare con il match.
 */
public interface GameActions extends Remote {
	/**
	 * Azione principale per eleggere un consigliere con un guadagno
	 * 
	 * @param nomeplayer
	 *            Nome client
	 * @param councillor
	 *            indice del consigliere nella counciloorpool da eleggere
	 * @param region
	 *            regione in cui eleggerlo
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteElectCouncillorWithEarn(String nomeplayer, int councillor, String region)
			throws RemoteException;

	/**
	 * Azione principale per comprare una businesstile
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @param region
	 *            regione in cui la si vuole comprare
	 * @param pos
	 *            posizione della tile
	 * @param discardedcards
	 *            array di interi che indicano la posizione delle carte scartate
	 *            nella mano del giocatore
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteAcquireBusinessTile(String nomeplayer, String region, int pos, int[] discardedcards)
			throws RemoteException;

	/**
	 * Azione principale per costruire un emporio con una tile
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @param town
	 *            nome città in cui costruire
	 * @param businesstile
	 *            posizione della tile da usare
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteBuildEmporiumWithTile(String nomeplayer, String town, int businesstile) throws RemoteException;

	/**
	 * Azione principale per costruire un emporio con l'aiuto del re
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @param town
	 *            nome cittò in cui costruire
	 * @param discardedcards
	 *            array di interi che indicano la posizione delle carte scartate
	 *            nella mano del giocatore
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 * 
	 */
	public String remoteBuildEmporiumWithKing(String nomeplayer, String town, int[] discardedcards)
			throws RemoteException;

	/**
	 * Azione veloce per ingaggiare un assistente
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteEngageAssistant(String nomeplayer) throws RemoteException;

	/**
	 * Azione veloce per cambiare una business tile in una regione
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @param region
	 *            nome regione in cui cambiare una tile
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteChangeBusinessTile(String nomeplayer, String region) throws RemoteException;

	/**
	 * Azione veloce per eleggere un consigliere con degli assistenti
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @param councillor
	 *            posizione nel councillorpool da cui prendere il consigliere da
	 *            eleggere
	 * @param region
	 *            nome regione in cui eleggere il consigliere
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteElectCouncillorWithAssistant(String nomeplayer, int councillor, String region)
			throws RemoteException;

	/**
	 * Azione veloce per ottenere un'altra azione principale
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @return Stringa di successo o dell'eccezione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteAdditionalMainAction(String nomeplayer) throws RemoteException;

	/**
	 * Metodo per finire il turno
	 * 
	 * @param nomeplayer
	 *            nome client
	 * @return Stringa di successo
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remotePassToNextRound(String nomeplayer) throws RemoteException;

	/**
	 * Metodo per sapere di chi è il turno
	 * 
	 * @return nome giocatore che sta giocando
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteWhoIsNow() throws RemoteException;

	/**
	 * Metodo per ottenere informazioni sul re
	 * 
	 * @return Il re
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public King remoteGetKing() throws RemoteException;

	/**
	 * Metodo per ottenere informazioni sulla nobility track
	 * 
	 * @return La nobility track
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public NobilityTrack remoteGetNobilityTrack() throws RemoteException;

	/**
	 * Metodo per ottenere la lista dei giocatori presenti
	 * 
	 * @return Lista dei giocatori
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public List<Player> remoteGetPlayers() throws RemoteException;

	/**
	 * Metodo per ottenere informazioni su una regione
	 * 
	 * @param r
	 *            Nome regione
	 * @return la regione
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public Region remoteGetRegion(String r) throws RemoteException;

	/**
	 * Metodo per avere una lista di tutti gli empori prensenti nel gioco
	 * 
	 * @return la lista degli empori
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public List<Emporium> remoteGetEmporium() throws RemoteException;

	/**
	 * Metodo per sapere il nome di tutti i giocatori in gioco
	 * 
	 * @return Lista dei nomi
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public List<String> remoteGetPlayersname() throws RemoteException;

	/**
	 * Metodo per avere informazioni su tutte le businesstile dei giocatori
	 * 
	 * @return Lista delle business tiles
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public List<BusinessTile> remoteGetPlayersTile() throws RemoteException;

	/**
	 * Metodo per avere informazioni sulle città
	 * 
	 * @return La lista delle città
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public Town[] remoteGetTowns() throws RemoteException;

	/**
	 * Metodo per avere informazioni su una council balcony in particolare
	 * 
	 * @param r
	 *            nome regione della councilbalcony
	 * @return La Councilbalcony
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public CouncilBalcony remoteGetCouncillorBalcony(String r) throws RemoteException;

	/**
	 * Metodo per avere informazioni riguardo la councillorpool
	 * 
	 * @return una lista con tutti i councillors della councillorpool
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public List<Councillor> remoteGetCouncillorPoolList() throws RemoteException;

	/**
	 * Metodo per avere informazioni sul bonus delle città Gold
	 * 
	 * @return Bonus della tipologia corrispondente
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public Bonus remoteGetBonusGoldTown() throws RemoteException;

	/**
	 * Metodo per avere informazioni sul bonus delle città Silver
	 * 
	 * @return Bonus della tipologia corrispondente
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public Bonus remoteGetBonusSilverTown() throws RemoteException;

	/**
	 * Metodo per avere informazioni sul bonus delle città Bronze
	 * 
	 * @return Bonus della tipologia corrispondente
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public Bonus remoteGetBonusBronzeTown() throws RemoteException;

	/**
	 * Metodo per avere informazioni sul bonus delle città Iron
	 * 
	 * @return Bonus della tipologia corrispondente
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public Bonus remoteGetBonusIronTown() throws RemoteException;

	/**
	 * Metodo per sapere il nome del vincitore
	 * 
	 * @return nome del vincitore
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String remoteWhoIsTheWinner() throws RemoteException;

	/**
	 * Metodo per potersi riconnettere in caso di disconnessione
	 * 
	 * @param clientname
	 *            nome del client
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void reconnect(String clientname) throws RemoteException;

	/**
	 * Metodo per togliere la flag di afk, in questo modo al giocatore non
	 * verranno più saltati i turni
	 * 
	 * @param who
	 *            nome client
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void noMoreAfk(String who) throws RemoteException;

	/**
	 * Metodo per sapere se è la fase Selling del market
	 * 
	 * @return true se lo è
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public boolean remoteIsSellingPhase() throws RemoteException;

	/**
	 * Metodo per sapere se è la fase Buying del market
	 * 
	 * @return true se lo è
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public boolean remoteIsBuyingPhase() throws RemoteException;

	/**
	 * Metodo per aggiungere una supply durante la fase Selling del market
	 * 
	 * @param playername
	 *            nome client
	 * @param assistants
	 *            numero assistenti da mettere in vendita
	 * @param business
	 *            indice delle business tiles da mettere in vendita
	 * @param politics
	 *            indice delle carte politiche da mettere in vendita che si
	 *            trovano nella mano del giocatore
	 * @param price
	 *            prezzo
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public void addMarketSupply(String playername, int assistants, int[] business, int[] politics, int price)
			throws RemoteException;

	/**
	 * Metodo per avere informazioni su tutte le supply presenti nella fase si
	 * Buying
	 * 
	 * @return Lista delle supply messe in vendita
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public List<Supply> remoteShowSupplies() throws RemoteException;

	/**
	 * MEtodo per poter comprare una supply
	 * 
	 * @param playername
	 *            nome client
	 * @param supplyindex
	 *            indice della supply da comprare
	 * @return Stringa di successo o di errore se non si hanno abbastanza monete
	 * @throws RemoteException
	 *             se ci sono problemi di connessione
	 */
	public String buySupply(String playername, int supplyindex) throws RemoteException;
	
	/**
	 * Metodo per vedere se una persona ha vinto a tavolino
	 * @return
	 * vero se il giocatore ha vinto a tavolino
	 * @throws RemoteException
	 * se ci sono problemi di connessione
	 */
	public boolean giveMeTavolino() throws RemoteException;
}
