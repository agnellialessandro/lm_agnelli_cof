package model;

import java.io.Serializable;

public class CouncilBalcony implements Serializable {
	private Councillor[] councillors;

	public CouncilBalcony(Councillor[] initcounc) {
		this.councillors = initcounc;
	}

	public Councillor[] showCouncillors() {
		return this.councillors;
	}

	public Councillor addCouncillor(Councillor c) {
		Councillor tempcounc;

		tempcounc = councillors[3];
		for (int i = 2; i >= 0; i--) {
			councillors[i + 1] = councillors[i];
		}
		councillors[0] = c;
		return tempcounc;
	}
}
