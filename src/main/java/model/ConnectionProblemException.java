package model;

public class ConnectionProblemException extends Exception {

	public ConnectionProblemException() {
		super();
	}

	public ConnectionProblemException(String s) {
		super(s);
	}
}
