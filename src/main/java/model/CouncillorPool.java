package model;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * Classe che gestisce tutti i consiglieri al momento inutilizzati. Al momento della creazione della partita e all'inizializzazione sarà pieno. 
 *
 */
public class CouncillorPool implements Serializable {
	private List<Councillor> unusedcouncillors;
	/**
	 * Costruttore che riempie la lista dei councillors.
	 */
	public CouncillorPool() {
		this.unusedcouncillors = new ArrayList<>();
		fillPool();
	}

	public void addCouncillor(Councillor c) {
		this.unusedcouncillors.add(c);
	}

	/**
	 * cerca il consigliere che soddisfa il parametro lo rimuove e lo ritorna
	 * @param c
	 * colore del cosigliere da prendere
	 * @return
	 * il cosigliere voluto
	 * @throws IllegalArgumentException
	 * se non c'è il cosigliere che si cercava
	 */
	public Councillor getCouncillor(Color c) {
		Councillor tempcounc;
		for (int i = 0; i < this.unusedcouncillors.size(); i++) {
			if ((this.unusedcouncillors.get(i).getColor()).equals(c)) {
				tempcounc = this.unusedcouncillors.get(i);
				this.unusedcouncillors.remove(i);
				return tempcounc;
			}
		}
		throw new IllegalArgumentException();
	}
	/**
	 * Rimuove il cosigliere di cui si ha il suo indirizzo nella lista
	 * @param c
	 * Consigliere da rimuovere
	 * @throws IllegalArgumentException
	 * Se non c'è il consigliere da rimuovere
	 */
	public void removeCouncillor(Councillor c) {
		boolean found = false;
		for (int i = 0; i < this.unusedcouncillors.size(); i++) {
			if (this.unusedcouncillors.get(i) == c) {
				this.unusedcouncillors.remove(i);
				found = true;
			}
		}
		if (!found) {
			throw new IllegalArgumentException();
		}
	}
	/**
	 * cerca il consigliere al numero index della lista e lo rimuove e lo ritorna
	 * @param index
	 * posizione nella lista in cui si trova il consigliere da rimuovere
	 * @return
	 * il consigliere rimosso
	 * @throws IllegalArgumentException
	 * se si è messa una posizione più piccola o più grande della grandezza della lista
	 */
	public Councillor getCouncillor(int index) {
		int length = this.unusedcouncillors.size();
		if (index >= 0 && index < length) {
			Councillor temp = unusedcouncillors.get(index);
			unusedcouncillors.remove(index);
			return temp;
		} else
			throw new IllegalArgumentException();
	}

	public List<Councillor> getWholePool() {
		return this.unusedcouncillors;
	}
	
	private void fillPool() {
		Color[] list = { Color.magenta, Color.blue, Color.white, Color.black, Color.orange, Color.pink };
		for (Color c : list) {
			for (int j = 0; j < Constant.STARTINGCOUNCILLOR / 6; j++) {
				unusedcouncillors.add(new Councillor(c));
			}
		}
	}

}
