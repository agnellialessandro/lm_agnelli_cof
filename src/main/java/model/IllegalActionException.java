package model;

public class IllegalActionException extends Exception {

	public IllegalActionException() {
		super();
	}

	public IllegalActionException(String s) {
		super(s);
	}

}
