package model;

import java.awt.Color;
import java.io.Serializable;

public class PoliticCard implements Serializable {
	private Color color;

	public PoliticCard(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

}
