package model;

import java.io.Serializable;
import java.util.List;
/**
 * Classe supplementare per la gestione delle fasi di market. Rappresenta le offerte del mercato di ogni giocatore. Per ogni supply viene scelta
 * solo una variabile (assistenti, carte politiche, carte di costruzione) da mettere in vendita
 */
public class Supply implements Serializable {
	private int assistantselling;
	private List<BusinessTile> businessselling;
	private List<PoliticCard> politicsselling;
	private int price;
	private Player seller;

	public Supply(int assistantselling, List<BusinessTile> businessselling, List<PoliticCard> politicselling, int price,
			Player seller) {
		this.assistantselling = assistantselling;
		this.businessselling = businessselling;
		this.politicsselling = politicselling;
		this.price = price;
		this.seller = seller;
	}

	public int getAssistant() {
		return this.assistantselling;
	}

	public List<BusinessTile> getBusiness() {
		return this.businessselling;
	}

	public List<PoliticCard> getPolitics() {
		return this.politicsselling;
	}

	public int getPrice() {
		return this.price;
	}

	public Player getSeller() {
		return this.seller;
	}
}
