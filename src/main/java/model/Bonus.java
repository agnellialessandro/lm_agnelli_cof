package model;

import java.io.Serializable;
import java.util.Random;
/**
 * 
 * 
 * Crea un bonus da assegnare a diverse classi del gioco. Il bonus può essere di diversi tipi per adeguarsi alla classe a cui verrà assegnato.
 *
 */
public class Bonus implements Serializable {

	private int assistants;
	private int cards;
	private boolean mainaction;
	private int coins;
	private int nobilitymoves;
	private int victorypoints;
	/**
	 * Costruttore principale
	 * @param type
	 * Stringa che descrive la tipologia di bonus.
	 * @param maxbonus
	 * Parametro che solitamente si trova all'interno della classe Configurazione. Serve a limitare i bonus per non crearli troppo grandi.
	 */
	public Bonus(String type, int maxbonus) {
		if (type.equals(Constant.TOWNBONUS)) {
			generateTownBonus(maxbonus);
		} else {
			generateRandomBonus(maxbonus);
			switch (type) {
			case Constant.REGIONBONUS:
				this.setMainActionToZero();
				break;
			case Constant.NOBILITYBONUS:
				this.setNobilityMovesToZero();
				break;
			default:
				break;
			}
		}
	}
	/**
	 * Costruttore secondario
	 * @param assistants
	 * Numero di assistenti da assegnare.
	 * @param cards
	 * Numero di carte da assegnare.
	 * @param mainaction
	 * Se darà una altra azione principale
	 * @param coins
	 * Numero di monete da assegnare.
	 * @param nobilitymoves
	 * Numero di punti nobiltà da assegnare.
	 * @param victorypoints
	 * Numero di punti vittoria da assegnare.
	 */
	public Bonus(int assistants, int cards, boolean mainaction, int coins, int nobilitymoves, int victorypoints) {
		this.assistants = assistants;
		this.cards = cards;
		this.mainaction = mainaction;
		this.coins = coins;
		this.nobilitymoves = nobilitymoves;
		this.victorypoints = victorypoints;
	}
	/**
	 * Metodo che associa alle variabili intere di Bonus dei valori randomici che rappresenteranno il bonus da assegnare
	 * @param maxbonus
	 * valore massimo
	 */
	private void generateRandomBonus(int maxbonus) {
		int mb = maxbonus;
		mb++;
		Random random = new Random();
		int temp;
		temp = random.nextInt(mb);
		this.assistants = temp;
		temp = random.nextInt(mb);
		this.cards = temp;
		temp = random.nextInt(mb);
		this.coins = temp;
		temp = random.nextInt(mb);
		this.nobilitymoves = temp;
		temp = random.nextInt(mb);
		this.victorypoints = temp;
		boolean temp2;
		temp2 = random.nextBoolean();
		this.mainaction = temp2;
	}
	/**
	 * Metodo speciale per assegnare un bonus ad una città, in questo modo verrà scelta solo una delle variabili intere di Bonus.
	 * @param maxbonus
	 * valore massimo
	 */
	private void generateTownBonus(int maxbonus) {
		Random random = new Random();
		int temp = random.nextInt(5);
		int mb = maxbonus + 1;
		int intrndmbonus = random.nextInt(mb);
		intrndmbonus++;
		switch (temp) {
		case 0:
			this.assistants = intrndmbonus;
			break;
		case 1:
			this.cards = intrndmbonus;
			break;
		case 2:
			this.coins = intrndmbonus;
			break;
		case 3:
			this.nobilitymoves = intrndmbonus;
			break;
		case 4:
			this.victorypoints = intrndmbonus;
			break;
		default:
			break;
		}
	}
	/**
	 * Serve solo per creare un Bonus per la nobility track
	 */
	private void setNobilityMovesToZero() {
		this.nobilitymoves = 0;
	}
	/**
	 * Server per creare un bonus per le regioni.
	 */
	private void setMainActionToZero() {
		this.mainaction = false;
	}

	public int getAssistants() {
		/*
		 * return the number of bonus assistants
		 */
		return this.assistants;
	}

	public int getCards() {
		/*
		 * return the number of bonus cards to be draw
		 */
		return this.cards;
	}

	public boolean getMainAction() {
		/*
		 * return true if the number the bonus applies a new mainaction when
		 * aquired
		 */
		return this.mainaction;
	}

	public int getCoins() {
		/*
		 * return the number of bonus coins
		 */
		return this.coins;
	}

	public int getNobilityMoves() {
		/*
		 * return how long he moves on the nobility track
		 */
		return this.nobilitymoves;
	}

	public int getVictoryPoints() {
		/*
		 * return victory point bonus
		 */
		return this.victorypoints;
	}
}
