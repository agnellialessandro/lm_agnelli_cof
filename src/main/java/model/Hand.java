package model;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Hand implements Serializable {
	private Deck deck;
	private List<PoliticCard> cards;

	public Hand(Deck deck) {
		this.deck = deck;
		this.cards = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			drawCard();
		}
	}

	public void drawCard(){ 
		cards.add(deck.drawCard());
	}

	public void discardCard(Color color) {
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).getColor().equals(color)) {
				deck.discardCard(cards.get(i));
				cards.remove(i);
				break;
			}
		}
	}

	public List<PoliticCard> showCard() {
		return cards;
	}

	public int getNumberOfCards() {
		return cards.size();
	}
	
	public void addCard(Color color){
		cards.add(new PoliticCard(color));
	}

}
