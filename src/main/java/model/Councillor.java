package model;

import java.awt.Color;
import java.io.Serializable;

public class Councillor implements Serializable {
	private Color color;

	public Councillor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

}
