package model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
/**
 * Questa classe rappresenta la nobility track del gioco, ha una classe Map all'interno che gestisce i bonus nei punti corrispondenti agli integer
 * nella track.
 *
 */
public class NobilityTrack extends Track implements Serializable {

	private Map<Integer, Bonus> where;
	/**
	 * Costruisce una nobility track. Associa ad ogni valore intero della lista un nuovo bonus generato randomicamente
	 * @param length
	 * Massima lunghezza della track, solitamente è nella classe Configuration
	 * @param arraybonuspos
	 * Lista di interi che indicano le posizioni nella track dei bonus
	 * @param maxbonus
	 * Massimo valore dei bonus, solitamente si trova nella classe Configuration
	 */
	public NobilityTrack(int length, List<Integer> arraybonuspos, int maxbonus) {
		super(length);
		Bonus temp;
		where = new TreeMap<>();
		for (int b = 0; b < arraybonuspos.size(); b++) {
			temp = new Bonus(Constant.NOBILITYBONUS, maxbonus);
			where.put(arraybonuspos.get(b), temp);
		}
	}

	public Bonus getBonus(int pos) {
		return this.where.get(pos);
	}

	public void checkBonus(Player p) {
		if (this.where.containsKey(p.getNobilityPosition())) {
			p.addBonus(this.where.get(p.getNobilityPosition()));
		}
	}

	public Map<Integer, Bonus> getTrack() {
		return this.where;
	}
}
