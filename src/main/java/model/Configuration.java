package model;

import java.io.Serializable;

/**
 * 
 * Classe che gestisce le configurazioni della partita. Verrà inizializzato dal
 * primo giocatore che entra nella partita.
 *
 */
public class Configuration implements Serializable {

	private int maxbonus;
	private int timeround;
	private String map;
	private int maxnumberofplayers;
	private int victorylength;
	private int nobilitylength;

	public Configuration() {
		this.maxbonus = 2;
		this.timeround = 120;
		this.map = Constant.DEFAULT;
		this.maxnumberofplayers = 8;
		this.victorylength = 100;
		this.nobilitylength = 21;
	}

	public int getMaxBonus() {
		return this.maxbonus;
	}

	public void setMaxBonus(int nmb) {
		this.maxbonus = nmb;
	}

	public int getTimeRound() {
		return this.timeround;
	}

	public void setTimeRound(int time) {
		this.timeround = time;
	}

	public String getMap() {
		return this.map;
	}

	public void setMap(String newmap) {
		this.map = newmap;
	}

	public int getVictoryLength() {
		return this.victorylength;
	}

	public void setVictoryLength(int length) {
		this.victorylength = length;
	}

	public int getNobilityLength() {
		return this.nobilitylength;
	}

	public void setNobilityLength(int length) {
		this.nobilitylength = length;
	}

	public int getMaxNumberOfPlayers() {
		return maxnumberofplayers;
	}

	public void setMaxNumberOfPlayers(int maxnumberofplayer) {
		this.maxnumberofplayers = maxnumberofplayer;
	}
}
