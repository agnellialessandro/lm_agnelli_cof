package model;

import java.awt.Color;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Deck è una classe che contiene una Map di Color, rappresentante il colore di
 * una carta, e il suo corrispettivo integer, che rappresenta il numero di carte
 * di quel colore presenti nel mazzo. *
 */
public class Deck implements Serializable {
	private static Map<Color, Integer> cards;

	/**
	 * Inizializzando Deck, viene inizializzata cards e vengono messe il numero
	 * standard di carte proporzionato al numero di persone nella partita.
	 * 
	 * @param c
	 *            Parametro preso dalla configurazione del gioco.
	 */
	public Deck(Configuration c) {
		cards = new HashMap<>();
		cards.put(Color.magenta, Constant.NUMBEROFCOLORCARDS * c.getMaxBonus());
		cards.put(Color.black, Constant.NUMBEROFCOLORCARDS * c.getMaxBonus());
		cards.put(Color.white, Constant.NUMBEROFCOLORCARDS * c.getMaxBonus());
		cards.put(Color.blue, Constant.NUMBEROFCOLORCARDS * c.getMaxBonus());
		cards.put(Color.cyan, Constant.NUMBEROFCOLORCARDS * c.getMaxBonus());
		cards.put(Color.orange, Constant.NUMBEROFCOLORCARDS * c.getMaxBonus());
		cards.put(Color.pink, Constant.NUMBEROFCOLORCARDS * c.getMaxBonus());
	}

	/**
	 * Pesca a caso una carta dal mazzo diminuendo uno dei contatori in cards.
	 * Lancia IllegalArgumentException se il mazzo è vuoto.
	 * 
	 * @return Restituisce la carta pescata.
	 */
	public PoliticCard drawCard() {
		int value;
		int newvalue;
		int sumofcards = 0;

		Color[] list = { Color.magenta, Color.black, Color.white, Color.blue, Color.cyan, Color.orange, Color.pink };
		for (int i = 0; i < cards.size(); i++) {
			sumofcards = sumofcards + cards.get(list[i]);
		}
		if (sumofcards == 0) {
			throw new IllegalArgumentException(Constant.DECKEMPTY);
		}
		Random random = new Random();
		while (true) {
			int temp = random.nextInt(7);
			value = cards.get(list[temp]);
			newvalue = value - 1;
			if (value > 0) {
				cards.replace(list[temp], value, newvalue);
				return new PoliticCard(list[temp]);
			}
		}
	}

	/**
	 * Aggiunge una carta scartata da un giocatore al mazzo.
	 * 
	 * @param card
	 *            Carta scartata dal giocatore.
	 */
	public void discardCard(PoliticCard card) {
		int value;
		int newvalue;
		value = cards.get(card.getColor());
		newvalue = value + 1;
		cards.replace(card.getColor(), value, newvalue);
	}

	/**
	 * 
	 * @return Il numero di carte presenti nel mazzo.
	 */
	public int getNumberOfCards() {
		Color[] list = { Color.magenta, Color.black, Color.white, Color.blue, Color.cyan, Color.orange, Color.pink };
		int sum = 0;
		for (int i = 0; i < 7; i++) {
			sum = sum + cards.get(list[i]);
		}
		return sum;
	}

	/**
	 * 
	 * @param color
	 *            Colore delle carte di cui se ne vuole sapere il numero nel
	 *            mazzo.
	 * @return Numero di carte presenti.
	 */
	public int getNumberOfColor(Color color) {
		return cards.get(color);
	}
}
