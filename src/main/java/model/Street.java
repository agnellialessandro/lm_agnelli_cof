package model;

import java.io.Serializable;
/**
 * Classe usata per la gestione dei collegamenti tra città, viene sfruttata per il calcolo delle distanze tra più città * 
 *
 */
public class Street implements Serializable {
	private Town first;
	private Town second;

	public Street(Town one, Town two) {
		this.first = one;
		this.second = two;
	}

	public boolean isItIn(Town t) {
		return this.first == t || this.second == t;
	}

	public Town destination(Town t) {
		if (this.first == t) {
			return this.second;
		}
		if (this.second == t) {
			return this.first;
		}
		throw new IllegalArgumentException();
	}
}
