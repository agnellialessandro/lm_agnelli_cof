package model;

import java.awt.Color;
import java.io.Serializable;

public class Town implements Serializable{
	private String townname;
	private Color type;
	private Bonus bonus;
	private Region region;
	/**
	 * Iniziallizza una città
	 * @param name
	 * nome della città
	 * @param c
	 * Colore-tipo della città. Bronze, Silver, Gold e Iron.
	 * @param r
	 * Regione in cui si trova la città
	 * @param maxbonus
	 * Massimo valore del bonus. Da configuration.
	 */
	public Town(String name, Color c, Region r, int maxbonus) {
		this.townname = name;
		this.type = c;
		this.region = r;
		bonus = new Bonus(Constant.TOWNBONUS, maxbonus);
	}

	public String getName() {
		return this.townname;
	}

	public Color getType() {
		return this.type;
	}

	public Bonus getBonus() {
		return this.bonus;
	}

	public Region getRegion() {
		return this.region;
	}
}
