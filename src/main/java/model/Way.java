package model;

import java.util.ArrayList;
import java.util.List;
/**
 * Classe supplementare per la gestione delle distanze tra città
 *
 */
public class Way {
	private List<Town> fromto;

	public Way(Town from) {
		this.fromto = new ArrayList<>();
		fromto.add(from);
	}

	public Way() {
		this.fromto = new ArrayList<>();
	}

	public void add(Town t) {
		fromto.add(t);
	}
	/**
	 * Metodo per calcolare il numero di città in cui passa.
	 * @return
	 * Costo 
	 */
	public int cost() {
		return this.fromto.size() - 1;
	}

	public Town last() {
		return this.fromto.get(this.fromto.size() - 1);
	}

	public void copy(Way w) {
		for (Town t : this.fromto) {
			w.add(t);
		}
	}
}
