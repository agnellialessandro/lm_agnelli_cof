package model;

import java.io.Serializable;

public class Region implements Serializable {

	private CouncilBalcony cb;
	private Bonus bonus;
	private BusinessTile[] tiles;
	private int maxbonus;
	/* need to save a full list of towns in order to create new business tile */
	private Town[] fulllist;

	public Region(CouncilBalcony cb, int maxbonus) {
		this.cb = cb;
		this.maxbonus = maxbonus;
		this.bonus = new Bonus(Constant.REGIONBONUS, this.maxbonus);
		this.tiles = new BusinessTile[2];
		this.tiles[0] = null;
		this.tiles[1] = null;
	}

	public CouncilBalcony getCouncBalcony() {
		return this.cb;
	}

	public Bonus getBonus() {
		return this.bonus;
	}

	public void addTiles(BusinessTile bt) {
		if (this.tiles[0] == null) {
			this.tiles[0] = bt;
		} else {
			if (this.tiles[1] == null)
				this.tiles[1] = bt;
		}
	}

	public void changeTile() {
		this.tiles[0] = new BusinessTile(this.fulllist, this.maxbonus);
		this.tiles[1] = new BusinessTile(this.fulllist, this.maxbonus);
	}

	public BusinessTile takeTile(int i) {
		if (i == 0 || i == 1) {
			BusinessTile tempbt = this.tiles[i];
			this.tiles[i] = new BusinessTile(this.fulllist, this.maxbonus);
			return tempbt;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void setTownList(Town[] t) {
		/*
		 * needs to be setted in order to create business tile when one of them
		 * is taken cause it's bought
		 */
		this.fulllist = t;
	}

	public BusinessTile[] showTiles() {
		return this.tiles;
	}

}
