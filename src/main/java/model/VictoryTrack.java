package model;

import java.io.Serializable;
import java.util.List;
/**
 * 
 * Classe che gestisce le vistory track di ogni giocatore.
 *
 */
public class VictoryTrack extends Track implements Serializable {

	public VictoryTrack(int length) {
		super(length);
	}
	/**
	 * Controlla il vincitore di ogni partita.
	 * @param p
	 * Lista dei giocatori presenti nella partita
	 * @return
	 * Il vincitore
	 */
	public Player theWinner(List<Player> p) {
		Player winner = p.get(0);
		for (Player temp : p) {
			if (temp.getVictoryPosition() > winner.getVictoryPosition()) {
				winner = temp;
			}
			if (temp.getVictoryPosition() == winner.getVictoryPosition()
					&& temp.getAssistant() > winner.getAssistant()) {
				winner = temp;
			} else {
				if (temp.getVictoryPosition() == winner.getVictoryPosition()
						&& temp.getAssistant() == winner.getAssistant() && temp.getPolitics() > winner.getPolitics()) {
					winner = temp;
				}
			}
		}
		return winner;
	}
}
