package modelTest;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import model.Configuration;
import model.Constant;
import model.Deck;
import model.PoliticCard;

public class DeckTest {
	@Rule
    public ExpectedException thrown= ExpectedException.none();

	@Test
	public void testDeck() {
		Configuration c = new Configuration();
		Deck deck = new Deck(c);
		assertEquals(Constant.NUMBEROFCOLORCARDS*c.getMaxBonus()*7, deck.getNumberOfCards());
	}

	@Test
	public void testDrawCard() {
		Configuration c = new Configuration();
		Deck deck = new Deck(c);
		int sum= deck.getNumberOfCards();
		deck.drawCard();
		sum--;
		assertEquals(sum, deck.getNumberOfCards());
		deck = new Deck(c);
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage(Constant.DECKEMPTY);
		for(int i=0; i<Constant.NUMBEROFCOLORCARDS*c.getMaxBonus()*7; i++){
			deck.drawCard();
		}
		deck.drawCard();
	}

	@Test
	public void testDiscardCard() {
		Configuration c = new Configuration();
		PoliticCard pol = new PoliticCard(Color.BLACK);
		Deck deck = new Deck(c);
		int sum = deck.getNumberOfColor(Color.BLACK);
		deck.discardCard(pol);
		sum++;
		assertEquals(sum, deck.getNumberOfColor(Color.BLACK));
	}
	

}
