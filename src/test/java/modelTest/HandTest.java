package modelTest;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import model.Configuration;
import model.Deck;
import model.Hand;

public class HandTest {

	@Test
	public void testHand() {
		Deck de = new Deck(new Configuration());
		Hand hand = new Hand(de);
		assertEquals(6 ,hand.getNumberOfCards());
		
	}


	@Test
	public void testDiscardCard() {
		Deck de = new Deck(new Configuration());
		Hand hand = new Hand(de);
		int sum = 0 ;
		for(int i=0; i<hand.getNumberOfCards(); i++){
			if(hand.showCard().get(i).getColor().equals(Color.BLACK)){
				sum++;
			}
		}
		int sum2 = 0;
		hand.discardCard(Color.BLACK);
		for(int i=0; i<hand.getNumberOfCards(); i++){
			if(hand.showCard().get(i).getColor().equals(Color.BLACK)){
				sum2++;
			}
		}
		if(sum ==0){
			assertEquals(0, sum2);
		}
		else{
			assertEquals(sum-1, sum2);
		}
	}


	@Test
	public void testAddCard() {
		Deck de = new Deck(new Configuration());
		Hand hand = new Hand(de);
		int sum = 0 ;
		for(int i=0; i<hand.getNumberOfCards(); i++){
			if(hand.showCard().get(i).getColor().equals(Color.BLACK)){
				sum++;
			}
		}
		int sum2 = 0;
		hand.addCard(Color.BLACK);
		for(int i=0; i<hand.getNumberOfCards(); i++){
			if(hand.showCard().get(i).getColor().equals(Color.BLACK)){
				sum2++;
			}
		}
		assertEquals(sum+1, sum2);
		
	}

}
