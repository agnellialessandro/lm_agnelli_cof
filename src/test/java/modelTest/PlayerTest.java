package modelTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import model.Bonus;
import model.Configuration;
import model.Constant;
import model.Deck;
import model.NobilityTrack;
import model.Player;

public class PlayerTest {	
	@Rule
    public ExpectedException thrown= ExpectedException.none();

	@Test
	public void testAddBonus() throws IOException {
		Configuration config = new Configuration();
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}

		Deck deck = new Deck(config);
		NobilityTrack nob = new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		List<Player> plist = new ArrayList<>();
		plist.add(new Player("Giorgio", deck, nob));
		
		int prevassist = plist.get(0).getAssistant();
		int prevcoins = plist.get(0).getCoinsPosition();
		int prevpol = plist.get(0).getPolitics();
		int prevvicto = plist.get(0).getVictoryPosition();
		
		
		
		Bonus bonus = new Bonus(Constant.NOBILITYBONUS, config.getMaxBonus());
		plist.get(0).addBonus(bonus);
		
		assertEquals(prevassist + bonus.getAssistants() , plist.get(0).getAssistant());
		assertEquals(prevcoins + bonus.getCoins(), plist.get(0).getCoinsPosition());
		assertEquals(prevpol + bonus.getCards(), plist.get(0).getPolitics());
		assertEquals(prevvicto + bonus.getVictoryPoints(), plist.get(0).getVictoryPosition());
		
		
	}

	@Test
	public void testMoveNobility() throws IOException {
		Configuration config = new Configuration();
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}

		Deck deck = new Deck(config);
		NobilityTrack nob = new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		List<Player> plist = new ArrayList<>();
		plist.add(new Player("Giorgio", deck, nob));
		
		plist.get(0).moveNobility(40);
		assertEquals(21, plist.get(0).getNobilityPosition());
		
	}

	@Test
	public void testMoveCoins() throws IOException {
		Configuration config = new Configuration();
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}

		Deck deck = new Deck(config);
		NobilityTrack nob = new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		List<Player> plist = new ArrayList<>();
		plist.add(new Player("Giorgio", deck, nob));
		
		thrown.expect(IllegalArgumentException.class);
		plist.get(0).moveCoins(-10);
	}


}
