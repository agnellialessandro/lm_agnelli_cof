package modelTest;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import model.Constant;
import model.Councillor;
import model.CouncillorPool;

public class CouncillorPoolTest {
	@Rule
    public ExpectedException thrown= ExpectedException.none();

	@Test
	public void testCouncillorPool() {
		int sum ;
		CouncillorPool cp = new CouncillorPool();
		Color[] list = { Color.magenta, Color.blue, Color.white, Color.black, Color.orange, Color.pink };
		for(int i = 0; i<list.length; i++){
			sum =0;
			for(int k = 0; k<cp.getWholePool().size(); k++){
				if(list[i].equals(cp.getWholePool().get(k).getColor())){
					sum++;
				}
			}
			assertEquals(Constant.STARTINGCOUNCILLOR /6, sum);
		}
	}

	@Test
	public void testAddCouncillor() {
		CouncillorPool cp = new CouncillorPool();
		Councillor co = new Councillor(Color.BLACK);
		int sum = cp.getWholePool().size();
		cp.addCouncillor(co);
		sum++;
		assertEquals(sum, cp.getWholePool().size());
		
	}

	@Test
	public void testGetCouncillorColor() {
		CouncillorPool cp = new CouncillorPool();
		int sum = cp.getWholePool().size();
		cp.getCouncillor(Color.BLACK);
		sum--;
		assertEquals(sum, cp.getWholePool().size());
		thrown.expect(IllegalArgumentException.class);
		cp.getCouncillor(Color.DARK_GRAY);
		
	}

	@Test
	public void testRemoveCouncillor() {
		CouncillorPool cp = new CouncillorPool();
		int sum = cp.getWholePool().size();
		Councillor co = cp.getWholePool().get(1);
		cp.removeCouncillor(co);
		sum--;
		assertEquals(sum, cp.getWholePool().size());
		thrown.expect(IllegalArgumentException.class);
		cp.removeCouncillor(co);	
		
	}

	@Test
	public void testGetCouncillorInt() {
		CouncillorPool cp = new CouncillorPool();
		int sum = cp.getWholePool().size();
		Councillor co = cp.getCouncillor(1);
		sum--;
		assertEquals(sum, cp.getWholePool().size());
		assertNotEquals(co, cp.getCouncillor(1));
	}


}
