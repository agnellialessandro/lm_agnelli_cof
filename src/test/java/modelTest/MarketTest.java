package modelTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import model.BusinessTile;
import model.Configuration;
import model.Constant;
import model.CouncilBalcony;
import model.Councillor;
import model.Deck;
import model.Market;
import model.NobilityTrack;
import model.Player;
import model.PoliticCard;
import model.Region;
import model.Supply;
import model.Town;

public class MarketTest {

	@Test
	public void testMarket() throws IOException {

		Configuration config = new Configuration();
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}

		Deck deck = new Deck(config);
		NobilityTrack nt = new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		List<Player> plist = new ArrayList<>();
		plist.add(new Player("Giorgio", deck, nt));
		plist.add(new Player("Mario", deck, nt));
		plist.add(new Player("Ale", deck, nt));
		plist.add(new Player("Anna", deck, nt));

		Market mark = new Market(plist);
		assertEquals(4, mark.getPlayersOrder().size());

	}

	@Test
	public void testAddSupply() {

	}

	@Test
	public void testGetSuppliesList() {

	}

	@Test
	public void testGetPlayersOrder() {

	}

	@Test
	public void testBuySupply() throws IOException {
		Configuration config = new Configuration();
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}

		Deck deck = new Deck(config);
		NobilityTrack nt = new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		List<Player> plist = new ArrayList<>();
		plist.add(new Player("Giorgio", deck, nt));
		plist.add(new Player("Mario", deck, nt));

		plist.get(1).moveCoins(10);
		plist.get(0).moveCoins(10);
		plist.get(0).addAssistant(3);
		Market mark = new Market(plist);
		
		int assistants = 2;
		int price = 5;
		
		Councillor[] councils = { new Councillor(Color.BLACK), new Councillor(Color.PINK), new Councillor(Color.ORANGE),
				new Councillor(Color.WHITE) };
		CouncilBalcony cb = new CouncilBalcony(councils);
		Region reg = new Region(cb, config.getMaxBonus());
		Town[] townforbusiness = { new Town("Arkon", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Burgen", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Castrum", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Dorful", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Esti", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Framek", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Graden", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Hellar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Indur", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Juvelar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Kultos", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Lyram", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Merkatim", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Naris", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Osium", Constant.BRONZE, reg, config.getMaxBonus()),
		
		};
		
		List<BusinessTile> butiles = new ArrayList<>();
		butiles.add(new BusinessTile(townforbusiness, config.getMaxBonus()));
		
		butiles.get(0).setPlayer(plist.get(0));

		List<PoliticCard> cards = new ArrayList<>();
		cards.add(plist.get(0).getHand().showCard().get(0));
		cards.add(plist.get(0).getHand().showCard().get(1));

		Supply sup = new Supply(assistants, butiles, cards, price, plist.get(0));
		mark.addSupply(sup);
		mark.buySupply(plist.get(1).getName(), sup);

		assertEquals(10+price, plist.get(0).getCoinsPosition());
		assertEquals(10-price, plist.get(1).getCoinsPosition());
		assertEquals(plist.get(1).getName(), butiles.get(0).getPlayer().getName());
		assertEquals(plist.get(0).getHand().getNumberOfCards(), 6-2);
		assertEquals(plist.get(1).getHand().getNumberOfCards(), 6+2);
		
		assertEquals(1, plist.get(0).getAssistant());
		assertEquals(2, plist.get(1).getAssistant());
		
		
		

	}

	@Test
	public void testAdjustPlayersCurrencies() {

	}

	@Test
	public void testClearTheSupplies() {

	}

}
