package modelTest;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import model.BusinessTile;
import model.Configuration;
import model.Constant;
import model.CouncilBalcony;
import model.Councillor;
import model.Region;
import model.Town;

public class RegionTest {
	
	@Rule
    public ExpectedException thrown= ExpectedException.none();


	@Test
	public void testAddTiles() {
		Councillor[] counclist = {new Councillor(Color.BLACK), new Councillor(Color.BLUE), new Councillor(Color.ORANGE), new Councillor(Color.WHITE)};
		CouncilBalcony cb = new CouncilBalcony(counclist);
		Configuration config = new Configuration();
		Region reg = new Region(cb, config.getMaxBonus());
		
		
		Town[] townforbusiness = { new Town("Arkon", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Burgen", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Castrum", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Dorful", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Esti", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Framek", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Graden", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Hellar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Indur", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Juvelar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Kultos", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Lyram", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Merkatim", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Naris", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Osium", Constant.BRONZE, reg, config.getMaxBonus()),
		
		};
		
		
		BusinessTile bt1 = new BusinessTile(townforbusiness, config.getMaxBonus());
		reg.addTiles(bt1);
		assertTrue(bt1== reg.showTiles()[0]);
		assertTrue(null== reg.showTiles()[1]);
		
		BusinessTile bt2 = new BusinessTile(townforbusiness, config.getMaxBonus());
		reg.addTiles(bt2);
		assertTrue(bt2== reg.showTiles()[1]);
		

		BusinessTile bt3 = new BusinessTile(townforbusiness, config.getMaxBonus());
		reg.addTiles(bt3);
		assertTrue(bt1== reg.showTiles()[0]);
		assertTrue(bt2== reg.showTiles()[1]);
		
		
	}

	

	@Test
	public void testTakeTile() {
		Councillor[] counclist = {new Councillor(Color.BLACK), new Councillor(Color.BLUE), new Councillor(Color.ORANGE), new Councillor(Color.WHITE)};
		CouncilBalcony cb = new CouncilBalcony(counclist);
		Configuration config = new Configuration();
		Region reg = new Region(cb, config.getMaxBonus());
		
		
		Town[] townforbusiness = { new Town("Arkon", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Burgen", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Castrum", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Dorful", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Esti", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Framek", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Graden", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Hellar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Indur", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Juvelar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Kultos", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Lyram", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Merkatim", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Naris", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Osium", Constant.BRONZE, reg, config.getMaxBonus()),
		
		};
		
		
		BusinessTile bt1 = new BusinessTile(townforbusiness, config.getMaxBonus());
		reg.addTiles(bt1);
		
		BusinessTile bt2 = new BusinessTile(townforbusiness, config.getMaxBonus());
		reg.addTiles(bt2);
		
		reg.setTownList(townforbusiness);
		
		assertEquals(bt1 ,reg.takeTile(0));
		assertNotEquals(bt1, reg.showTiles()[0]);
		assertEquals(bt2 ,reg.takeTile(1));
		assertNotEquals(bt2, reg.showTiles()[1]);

		
	}


}
