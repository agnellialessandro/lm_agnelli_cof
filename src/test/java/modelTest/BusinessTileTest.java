package modelTest;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import model.BusinessTile;
import model.Configuration;
import model.Constant;
import model.CouncilBalcony;
import model.Councillor;
import model.Region;
import model.Town;

public class BusinessTileTest {

	@Test
	public void testBusinessTile() {
		Councillor[] counclist = {new Councillor(Color.BLACK), new Councillor(Color.BLUE), new Councillor(Color.ORANGE), new Councillor(Color.WHITE)};
		CouncilBalcony cb = new CouncilBalcony(counclist);
		Configuration config = new Configuration();
		Region reg = new Region(cb, config.getMaxBonus());
		
		Town[] townforbusiness = { new Town("Arkon", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Burgen", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Castrum", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Dorful", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Esti", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Framek", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Graden", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Hellar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Indur", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Juvelar", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Kultos", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Lyram", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Merkatim", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Naris", Constant.BRONZE, reg, config.getMaxBonus()),
				new Town("Osium", Constant.BRONZE, reg, config.getMaxBonus()),
		
		};
		
		BusinessTile bt = new BusinessTile(townforbusiness, config.getMaxBonus());
		assertTrue(bt.getTown().size()<=3);
		assertTrue(bt.getBonus()!= null);
	}

	

}
