package modelTest;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import model.Configuration;
import model.Constant;
import model.Deck;
import model.NobilityTrack;
import model.Player;

public class NobilityTrackTest {

	@Test
	public void testNobilityTrack() throws IOException {
		Configuration config = new Configuration();
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}
		
		NobilityTrack nob= new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		assertEquals(arraypos.size(), nob.getTrack().size());
		
	}

	@Test
	public void testGetBonus() {
		//fail("Not yet implemented");
	}

	@Test
	public void testCheckBonusPlayer() throws IOException {
		Configuration config = new Configuration();
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}
		
		Deck deck = new Deck(config);
		NobilityTrack nob = new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		List<Player> plist = new ArrayList<>();
		plist.add(new Player("Giorgio", deck, nob));
		
		int prevassist = plist.get(0).getAssistant();
		int prevcoins = plist.get(0).getCoinsPosition();
		int prevpol = plist.get(0).getPolitics();
		int prevvicto = plist.get(0).getVictoryPosition();
		
		plist.get(0).moveNobility(arraypos.get(0));
		nob.checkBonus(plist.get(0));		
		assertEquals(prevassist + nob.getTrack().get(arraypos.get(0)).getAssistants(), plist.get(0).getAssistant());
		assertEquals(prevcoins + nob.getTrack().get(arraypos.get(0)).getCoins(), plist.get(0).getCoinsPosition());
		assertEquals(prevpol + nob.getTrack().get(arraypos.get(0)).getCards(), plist.get(0).getPolitics());
		assertEquals(prevvicto + nob.getTrack().get(arraypos.get(0)).getVictoryPoints(), plist.get(0).getVictoryPosition());
		
		
	}


}
