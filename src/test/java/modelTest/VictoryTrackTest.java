package modelTest;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import model.Configuration;
import model.Constant;
import model.Deck;
import model.NobilityTrack;
import model.Player;
import model.VictoryTrack;

public class VictoryTrackTest {


	@Test
	public void testTheWinner() throws IOException {
		Configuration config = new Configuration();
		VictoryTrack victo = new VictoryTrack(config.getVictoryLength());
		
		
	
		Scanner in = null;
		List<Integer> arraypos = new ArrayList<>();
		try (FileReader fr = new FileReader(Constant.MAPFOLDER + config.getMap() + "nobility.txt")) {
			in = new Scanner(new BufferedReader(fr));
			while (in.hasNext()) {
				arraypos.add(in.nextInt());
			}
			in.close();
		} catch (IOException e) {
			throw new IOException(e + " error loaging nobility file");
		}

		Deck deck = new Deck(config);
		NobilityTrack nt = new NobilityTrack(config.getNobilityLength(), arraypos, config.getMaxBonus());
		List<Player> plist = new ArrayList<>();
		plist.add(new Player("wowo", deck, nt));
		plist.add(new Player("Giorgio", deck, nt));
		plist.add(new Player("Mario", deck, nt));
		
		plist.get(0).moveVictory(10);
		plist.get(1).moveVictory(11);
		
		//System.out.println(plist.get(0).getAssistant());
		//System.out.println(plist.get(1).getAssistant());
		//System.out.println(plist.get(0).getPolitics());
		//System.out.println(plist.get(1).getPolitics());
		//TODO
		
		assertEquals(plist.get(1).getName() ,victo.theWinner(plist).getName());
		plist.get(0).moveVictory(1);
		//System.out.println(victo.theWinner(plist).getName());
		//assertEquals(plist.get(1).getName() ,victo.theWinner(plist).getName());
		
	}

}
