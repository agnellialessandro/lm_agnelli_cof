package modelTest;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import model.CouncilBalcony;
import model.Councillor;

public class CouncilBalconyTest {


	@Test
	public void testAddCouncillor() {
		Councillor[] counclist = {new Councillor(Color.BLACK), new Councillor(Color.BLUE), new Councillor(Color.ORANGE), new Councillor(Color.WHITE)};
		CouncilBalcony cb = new CouncilBalcony(counclist);
		cb.addCouncillor(new Councillor(Color.PINK));
		assertTrue(cb.showCouncillors()[3].getColor().equals(Color.ORANGE));
		assertTrue(cb.showCouncillors()[0].getColor().equals(Color.PINK));
	}

}
