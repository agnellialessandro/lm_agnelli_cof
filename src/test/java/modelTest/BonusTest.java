package modelTest;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Bonus;
import model.Constant;

public class BonusTest {

	@Test
	public void testBonusStringInt() {
		Bonus bonus = new Bonus(Constant.NOBILITYBONUS, 6);
		assertEquals(0, bonus.getNobilityMoves());
		bonus = new Bonus(Constant.REGIONBONUS, 6);
		assertFalse(bonus.getMainAction());
		bonus = new Bonus(Constant.TOWNBONUS, 6);
		int sum = bonus.getAssistants() + bonus.getCards() + bonus.getCoins() + bonus.getNobilityMoves() + bonus.getVictoryPoints();
		assertEquals(6, sum, 6);
	}
	
	@Test
	public void testBonusIntIntBooleanIntIntInt() {
		Bonus bonus = new Bonus(1, 2, true, 3, 4, 5);
		assertEquals(1, bonus.getAssistants());
		assertEquals(2,bonus.getCards());
		assertTrue(bonus.getMainAction());
		assertEquals(3,bonus.getCoins());
		assertEquals(4,bonus.getNobilityMoves());
		assertEquals(5,bonus.getVictoryPoints());
	}


	


}
